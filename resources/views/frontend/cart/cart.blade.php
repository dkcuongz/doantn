@extends('frontend.master.master')
@section('title','Giỏ Hàng')
@section('content')
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Trang giỏ hàng</h2>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>                   
          <li class="active">Giỏ hàng</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

 <!-- Cart view section -->
 <section id="cart-view">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="cart-view-area">
           <div class="cart-view-table">          
               <div class="table-responsive" >
                  <table class="table">
                    <thead>
                      <tr>
                        <th></th>
                        <th></th>
                        <th>Sản phẩm</th>
                        <th>Giá</th>
                        <th>Số lượng</th>
                        <th>Tổng tiền</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($cart as $item)
                      <tr>
                        <td><a class="remove" href="/cart/del/{{$item->rowId}}"><fa class="fa fa-close"></fa></a></td>
                        <td><a href="#"><img src="/backend/img/{{$item->options->img}}" alt="img"></a></td>
                        <td><a class="aa-cart-title" href="#">{{$item->options->name_attr}}</a></td>
                        <td>{{number_format($item->price,0, ',', '.')}}VNĐ</td>
                        <td><input onchange="return update('{{$item->rowId}}',this.value)" id="quantity" class="aa-cart-quantity" type="number" value="{{$item->qty}}" ></td>
                        <td>{{number_format($item->price * $item->qty,0, ',', '.')}}VNĐ</td>
                      </tr>
                      @endforeach                   
                    <form action="{{route('postCoupon')}}" method="POST">
                      @csrf
                      <tr>
                        <td colspan="6" class="aa-cart-view-bottom">
                          <div class="aa-cart-coupon" >
                            <input class="aa-coupon-code" type="text" placeholder="Coupon" name="code"  data-role="none">
                            <input class="aa-cart-view-btn" type="submit" value="Nhập mã"  data-role="none">
                          </div>
                          @if (session('thongbao'))
                          <div class="alert alert-danger col-lg-4" role="alert">
                                  <strong>{{session('thongbao')}}</strong>
                          </div>
                          @endif
                          @if (session('thongbao1'))
                          <div class="alert alert-success col-lg-4" role="alert">
                                  <strong>{{session('thongbao1')}}</strong>
                          </div>
                          @endif
                          {{-- <input class="aa-cart-view-btn" type="submit" value="Update Cart"  data-role="none"> --}}
                        </td>
                      </tr>
                    </form>
                      </tbody>
                  </table>
                </div>           
             <!-- Cart Total view -->
             <div class="cart-view-total">
               <h4>Tổng giỏ hàng</h4>
               <table class="aa-totals-table">
                 <tbody>
                  <tr>
                    <th>Tổng tiền</th>
                    <td>{{$total}}VNĐ</td>
                  </tr>
                   <tr>
                     <th>Giảm giá</th>
                     <td>@if (Auth::user() &&Auth::user()->id == session('user_id'))
                              @if (session('discount')>100)
                              {{number_format(session('discount'),0, ',', '.')}} VNĐ
                              @else   {{number_format($total_s*session('discount')/100,0, ',', '.')}}VNĐ
                              @endif                                                    
                         @else
                          {{"0 VNĐ"}}
                     @endif
                    </td>
                   </tr>
                   <tr>
                     <th>Thành tiền</th>
                     <td>@if (Auth::user() && Auth::user()->id == session('user_id'))
                      @if (session('discount')>100)
                      {{number_format($total_s-session('discount'),0, ',', '.')}} VNĐ
                      @else   {{number_format($total_s*(100-session('discount'))/100,0, ',', '.')}}VNĐ
                      @endif                                                    
                     @else
                     {{$total}}VNĐ
                   @endif</td>
                   </tr>
                 </tbody>
               </table>
               <a href="/checkout" class="aa-cart-view-btn">Thanh toán</a>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->
  <!-- Subscribe section -->
  @include('frontend.master.subcribe')
  <!-- / Subscribe section -->
@endsection
@section('script')
    @parent

    <script>
        function update(rowId,qty){
            $.get(
                "/cart/edit/"+rowId+"/"+qty,
                function(data){
                    if(data=='success'){
                        location.reload();
                    }else{
                        alert('update false');
                    }
                }
            )
        }


    </script>
    <script>
            $(document).ready(function () {

                var quantitiy = 0;
                $('.quantity-right-plus').click(function (e) {

                    // Stop acting like a button
                    e.preventDefault();
                    // Get the field name
                    var quantity = parseInt($('#quantity').val());

                    // If is not undefined

                    $('#quantity').val(quantity + 1);


                    // Increment

                });

                $('.quantity-left-minus').click(function (e) {
                    // Stop acting like a button
                    e.preventDefault();
                    // Get the field name
                    var quantity = parseInt($('#quantity').val());

                    if (quantity > 0) {
                        $('#quantity').val(quantity - 1);
                    }
                });

            });

        </script>

@endsection