<header id="aa-header">
  <!-- start header top  -->
  <div class="aa-header-top">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-header-top-area">
            <!-- start header top left -->
            <div class="aa-header-top-left">
              <!-- start language -->
              {{-- <div class="aa-language">
                <div class="dropdown">
                  <a class="btn dropdown-toggle" href="#" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <img src="img/flag/english.jpg" alt="english flag">ENGLISH
                    <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="#"><img src="img/flag/french.jpg" alt="">FRENCH</a></li>
                    <li><a href="#"><img src="img/flag/english.jpg" alt="">ENGLISH</a></li>
                  </ul>
                </div>
              </div> --}}
              <!-- / language -->

              <!-- start currency -->
              {{-- <div class="aa-currency">
                <div class="dropdown">
                  <a class="btn dropdown-toggle" href="#" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <i class="fa fa-usd"></i>USD
                    <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="#"><i class="fa fa-euro"></i>EURO</a></li>
                    <li><a href="#"><i class="fa fa-jpy"></i>YEN</a></li>
                  </ul>
                </div>
              </div> --}}
              <!-- / currency -->
              <!-- start cellphone -->
              <div class="cellphone hidden-xs">
                <p><span class="fa fa-phone"></span>035-709-3377</p>
              </div>
              <!-- / cellphone -->
            </div>
            <!-- / header top left -->
            <div class="aa-header-top-right">
              <ul class="aa-head-top-nav-right" >
              <li class="hidden-xs"><a href="{{route('getcart')}}">Giỏ hàng</a></li>
                <li class="hidden-xs"><a href="{{route('getcheckout')}}">Thanh toán</a></li>
                @if(Auth::user())
                 <li class="hidden-xs"> <div class="dropdown">
                  <a data-toggle="dropdown" href="">{{Auth::user()->name}}</a>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('getProfile',Auth::user()->id)}}">Tài khoản</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('getUserorder',Auth::user()->id)}}">Xem hóa đơn</a></li>
                    <li role="presentation" class="divider"></li>
                     <br>                 
                    <li role="presentation"> <a role="menuitem" tabindex="-1" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                       {{ __('Logout') }}
                   </a>

                   <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                       @csrf
                   </form></li>
                  </ul>
                </div>
                  @else 
                <li><a href="/login" >Login</a></li>
            </li>
                @endif
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- / header top  -->

  <!-- start header bottom  -->
  <div class="aa-header-bottom">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-header-bottom-area">
            <!-- logo  -->
            <div class="aa-logo">
              <!-- Text based logo -->
              <a href="/">
                <span class="fa fa-shopping-cart"></span>
                <p>DK<strong>Shop</strong> <span>Your Shopping Partner</span></p>
              </a>
              <!-- img based logo -->
              <!-- <a href="index.html"><img src="img/logo.jpg" alt="logo img"></a> -->
            </div>
            <!-- / logo  -->
             <!-- cart box -->
             @if (Cart::content())
            <div class="aa-cartbox">
              <a class="aa-cart-link" href="{{route('getcart')}}">
                <span class="fa fa-shopping-basket"></span>
                <span class="aa-cart-title">GIỎ HÀNG</span>
                <span id="count" class="aa-cart-notify">{{count(Cart::content())}}</span>
              </a>
              <div class="aa-cartbox-summary">
                <ul>
                  @foreach (Cart::content() as $item)
                  <li>
                    <a class="aa-cartbox-img" href="{{route('getcart')}}"><img src="/backend/img/{{$item->options->img}}" alt="img"></a>
                    <div class="aa-cartbox-info">
                      <h4><a href="#">{{$item->name}}</a></h4>
                      <p>{{$item->qty}} x {{number_format($item->price,0, ',', '.')}}VNĐ</p>
                    </div>
                    <a class="aa-remove-product" href="/cart/del/{{$item->rowId}}"><span class="fa fa-times"></span></a>
                  </li>
                      @endforeach                                 
                  <li>
                    <span class="aa-cartbox-total-title">
                      Total
                    </span>
                    <span class="aa-cartbox-total-price">
                      {{Cart::total(0,'',',')}} VNĐ
                    </span>
                  </li>
                </ul>
                <a class="aa-cartbox-checkout aa-primary-btn" href="{{route('getcheckout')}}">Thanh toán</a>
              </div>
            </div>
            @endif
            <!-- / cart box -->
            <!-- search box -->
            <div class="aa-search-box">
            <form action="{{route('search')}}" method="get">
            <input id="search" name="search" value="{{request('search')}}" type="text" class="form-control" placeholder="Search here ex. 'man' ">
                <button type="submit"><span class="fa fa-search"></span></button>
              </form>
            </div>
            <!-- / search box -->             
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- / header bottom  -->
</header>
<!-- / header section -->
<!-- menu -->
<section id="menu">
  <div class="container">
    <div class="menu-area">
      <!-- Navbar -->
      <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>          
        </div>
        <div class="navbar-collapse collapse">
          <!-- Left nav -->
          <ul class="nav navbar-nav">
            <li><a href="/">Home</a></li>
            {{showMenu($cate,1,"","")}} 
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>       
  </div>
</section>