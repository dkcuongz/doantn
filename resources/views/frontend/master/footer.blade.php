<footer id="aa-footer">
  <!-- footer bottom -->
  <!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script>
    window.fbAsyncInit = function() {
      FB.init({
        xfbml            : true,
        version          : 'v7.0'
      });
    };

    (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <!-- Your Chat Plugin code -->
  <div class="fb-customerchat"
    attribution=setup_tool
    page_id="111399710615223"
logged_in_greeting="Xin chào,tôi có thể giúp gì cho bạn?"
logged_out_greeting="Xin chào,tôi có thể giúp gì cho bạn?">
  </div>
  <div class="aa-footer-top">
   <div class="container">
      <div class="row">
      <div class="col-md-12">
        <div class="aa-footer-top-area">
          <div class="row">
            <div class="col-md-3 col-sm-6">
              <div class="aa-footer-widget">
                <h3>Thông tin</h3>
                <ul class="aa-footer-nav">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">Các dịch vụ của chúng tôi</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="aa-footer-widget">
               
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="aa-footer-widget">
               
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="aa-footer-widget">
                <div class="aa-footer-widget">
                  <h3>Thông tin liên hệ</h3>
                  <address>
                    <p> 332b Hoàng Công Chất Nam Từ Liêm Hà Nội</p>
                    <p><span class="fa fa-phone"></span>0357093377 </p>
                    <p><span class="fa fa-envelope"></span>dkshop@gmail.com</p>
                  </address>
                  <div class="aa-footer-social">
                    <a href="#"><i class="fab fa-facebook"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-google-plus"></i></a>
                    <a href="#"><i class="fab fa-youtube"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
   </div>
  </div>
  <!-- footer-bottom -->
  <div class="aa-footer-bottom">
    <div class="container">
      <div class="row">
      <div class="col-md-12">
        <div class="aa-footer-bottom-area">
          <p>Designed by <a href="#">DkShop</a></p>
          <div class="aa-footer-payment">
            <span class="fab fa-cc-mastercard"></span>
            <span class="fab fa-cc-visa"></span>
            <span class="fab fa-paypal"></span>
            <span class="fab fa-cc-discover"></span>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</footer>