<section id="aa-subscribe">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-subscribe-area">
            <h3>Đăng ký để nhận tin tức mới </h3>
            <p>Đăng ký để nhận thông tin mới nhất về sản phẩm và khuyến mãi!</p>
            @if (session('thongbao'))
            <div class="alert alert-success" role="alert">
                    <strong>{{session('thongbao')}}</strong>
            </div>
            @endif
          <form action="{{route('getSubcribe')}}" class="aa-subscribe-form">
            @csrf
              <input type="email" name="email" id="" placeholder="Enter your Email">
              {{showErrors($errors,'email')}}
              <input type="submit" value="Đăng ký">
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>