@extends('frontend.master.master')
@section('title','Shop')
@section('body','productPage')
@section('content')
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img width="1920px" height="300px" src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Fashion</h2>
        <ol class="breadcrumb">
          <li><a href="/">Home</a></li>         
          <li class="active">Shop</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->
  <!-- product category -->
  <section id="aa-product-category">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-8 col-md-push-3">
          <div class="aa-product-catg-content">
            <div class="aa-product-catg-head">
              <div class="aa-product-catg-head-left">
              <form  
              action="{{route('search')}}" id="form1" class="aa-sort-form">
                  <label for="">Sắp xếp</label>
                  <select name="string" onchange = "submit()" id="slType">
                    <option @if ($string === "0") selected @endif value="0">Default</option>
                    <option @if ($string === "name") selected @endif value="name">Tên</option>
                    <option @if ($string === "price") selected @endif value="price">Giá</option>
                    <option @if ($string === "created_at") selected @endif value="created_at">Ngày</option>
                  </select>                     
                  <label style="margin-left:30px " for="">Xem</label>
                  <select name="number" onchange = "submit()" id="slNumberOnPage">
                    <option @if ($number == 6) selected @endif value="6" >6</option>
                    <option @if ($number == 12) selected @endif value="12">12</option>
                    <option @if ($number == 24) selected @endif value="24">24</option>
                  </select>
                  {{-- <input type="submit"> --}}
                </form>
              </div>
              <div class="aa-product-catg-head-right">
                <a id="grid-catg" href="#"><span class="fa fa-th"></span></a>
                <a id="list-catg" href="#"><span class="fa fa-list"></span></a>
              </div>
            </div>
            <div class="aa-product-catg-body">
              <ul class="aa-product-catg">
                <!-- start single product item -->
                @foreach ($products as $item)
                <!-- start single product item -->
             <li>
              <figure>
              <a class="aa-product-img" href="/product/detail/{{$item->slug}}-{{$item->id}}"><img src="/backend/img/{{$item->img}}" alt="{{$item->describe}}"></a>
                <a id ="{{$item->id}}"  data-toggle="modal" data-target="#myModal" class="aa-add-card-btn add-view" href=""><span class="fa fa-shopping-cart"></span>Thêm vào giỏ</a>
                  <figcaption>
                  <h4 class="aa-product-title"><a href="/product/detail/{{$item->slug}}-{{$item->id}}">{{$item->name}}</a></h4>
                  <span class="aa-product-price">{{number_format($item->price,0, ',', '.')}} VNĐ</span>
                  {{-- <span class="aa-product-price"><del>$65.50</del></span> --}}
                  <p class="aa-product-descrip">{{$item->info}}</p>
                </figcaption>
              </figure>                        
              <div class="aa-product-hvr-content">             
                <a class="view-data" id ="{{$item->id}}" href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal"><span class="fa fa-search"></span></a>                          
              </div>
              <!-- product badge -->
              {{-- <span class="aa-badge aa-sale" href="#">SALE!</span> --}}
              {{-- <span class="aa-badge aa-sold-out" href="#">Sold Out!</span>
              <span class="aa-badge aa-hot" href="#">HOT!</span> --}}
            </li>
              @endforeach                                         
              </ul>
                <!-- add cart modal --> 
                <div id="myModal" class="modal fade" role="dialog">
                  <div class="modal-dialog">              
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Chọn Loại</h4>
                      </div>
                      <div class="modal-body">
                        <form action="">
                          <p id="prd-nameq"> </p>
                          <img  id="prd-add" alt="">
                          <select style="height: 28px;width:250px" name="" id="option-add">
                            
                          </select>
                          <input id="quantity-add" style="text-align: center; width:70px" type="number" value="1" name="quantity-add">
                          <p id ="qty"></p>
                        </form>
                      </div>
                      <div class="modal-footer">
                        <button id = "send-add-cart" type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                        <button type="button" class="btn btn-close" data-dismiss="modal">Đóng</button>
                      </div>
                    </div>           
                  </div>
                </div>
                  <!-- add cart modal --> 
              <!-- quick view modal -->                  
              <div class="modal fade" id="quick-view-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">                      
                    <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <div class="row" id = "product-detail">
                        <!-- Modal view slider -->
                        <div class="col-md-5 col-sm-5 col-xs-12">                              
                          <div class="aa-product-view-slider">                                
                            <div id="demo-1" class="simpleLens-gallery-container">
                              <div class="simpleLens-container">
                                <div class="simpleLens-big-image-container"><a id="img-sd" data-lens-image="" class="simpleLens-lens-image">
                                  <img id="img-prd" src="" class="simpleLens-big-image"></a></div>
                              </div>                 
                            </div>
                          </div>
                        </div>
                        <!-- Modal view content -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="aa-product-view-content">
                            <h3 id ="nameprd"></h3>
                            <div class="aa-price-block">
                              <span id ="price" class="aa-product-view-price"></span>
                              <p class="aa-product-avilability">Trạng thái: <span id="state">Còn hàng</span></p>
                            </div>
                            <p id = "info"></p>
                            <div class="aa-prod-quantity">
                              <form action="">
                                <select style="height: 28px;width:250px" name="" id="option">
                                  
                                </select>
                              </form>
                              <p style="margin: 15px 0 0 0" class="aa-prod-category">
                              Danh mục: <a id="cate" href="#"></a>
                              </p>
                            </div>
                            <div class="aa-prod-view-bottom">
                              <button id="addcart-m" data-dismiss="modal"  class="aa-add-to-cart-btn"><span class="fa fa-shopping-cart"></span>Thêm vào giỏ</button>
                              <a id ="detail"  class="aa-add-to-cart-btn">Xem chi tiết</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>                        
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div>
              <!-- / quick view modal -->   
            </div>
            <div class="aa-product-catg-pagination">
              <nav>
                <ul>
                	{{$products->appends(request()->input())->links()}}
                </ul>
              </nav>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-md-pull-9">
          <aside class="aa-sidebar">
            <!-- single sidebar -->
            <div class="aa-sidebar-widget">
              <h3>Category</h3>   
              <ul class="aa-catg-nav">
              {{showCate_2($categories,1,'')}}
              </ul>           
            </div>
            <!-- single sidebar -->
            <!-- single sidebar -->
            <div class="aa-sidebar-widget">
              <h3>Sản phẩm theo giá</h3>              
              <!-- price range -->
              <div   class="aa-sidebar-price-range">
                <form action="">
                  <div id ="nou" class="noUi-target noUi-ltr noUi-horizontal noUi-background">
                  </div>
                <span id="slider-snap-value-lower" class="example-val">{{request('start')}}300.000</span>
                  <span id="slider-snap-value-upper" class="example-val">{{request('finish0')}}1000.000</span>
                   <p></p>
                   <button class="aa-filter-btn" type="submit" id="filter">Lọc</button>                
               </form>                       
              </div>                          
            </div>
            <!-- single sidebar -->
            <!-- single sidebar -->
            <div class="aa-sidebar-widget">
              <h3>Sản phẩm phổ biến</h3>
              <div class="aa-recently-views">
                <ul>
                  @foreach ($prd_popular as $item)
                  <li>
                    <a href="/product/detail/{{$item->slug}}-{{$item->id}}" class="aa-cartbox-img"><img alt="img" src="/backend/img/{{$item->img}}"></a>
                    <div class="aa-cartbox-info">
                      <h4><a href="/product/detail/{{$item->slug}}-{{$item->id}}">{{$item->name}}</a></h4>
                      <p>1 x {{number_format($item->price,0, ',', '.')}} VND</p>
                    </div>                    
                  </li>
                  @endforeach                                     
                </ul>
              </div>                            
            </div>
            <!-- single sidebar -->
          </aside>
        </div>
       
      </div>
    </div>
    {{-- action="/product/filter"  --}}
    <form 
        action="{{ route('search')}}" 
       id="frmFilter">
    </form>
  </section>
  <!-- / product category -->
@include('frontend.master.subcribe')
@endsection
@section('script')
@parent
<script type="text/javascript">
function submit(){
  document.getElementById("form1").submit();
  document.getElementById("form2").submit();
}
$("#filter").click(function (e) {
  e.preventDefault();
  const min = $("#slider-snap-value-lower").text();
  const max = $("#slider-snap-value-upper").text();
  const type = $("#slType").val();
  const number = $("#slNumberOnPage").val();
  const search = $("#search").val();
  const data = {
    min, max, type, number, search
  };
  $form = $("#frmFilter");
  $form.append(`<input type="hidden" name="start" value="${min}">`);
  $form.append(`<input type="hidden" name="finish" value="${max}">`);
  $form.append(`<input type="hidden" name="string" value="${type}">`);
  $form.append(`<input type="hidden" name="number" value="${number}">`);
  $form.append(`<input type="hidden" name="search" value="${search}">`);
  //debugger
  $form.submit();
  // console.log(data);
});
$("#slNumberOnPage").change(function (e) {
  e.preventDefault();
  const min = $("#slider-snap-value-lower").text();
  const max = $("#slider-snap-value-upper").text();
  const type = $("#slType").val();
  const number = $("#slNumberOnPage").val();
  const search = $("#search").val();
  const data = {
    min, max, type, number, search
  };
  $form = $("#frmFilter");
  $form.append(`<input type="hidden" name="start" value="${min}">`);
  $form.append(`<input type="hidden" name="finish" value="${max}">`);
  $form.append(`<input type="hidden" name="string" value="${type}">`);
  $form.append(`<input type="hidden" name="number" value="${number}">`);
  $form.append(`<input type="hidden" name="search" value="${search}">`);
  //debugger
  $form.submit();
  // console.log(data);
});
$("#slType").change(function (e) {
  e.preventDefault();
  const min = $("#slider-snap-value-lower").text();
  const max = $("#slider-snap-value-upper").text();
  const type = $("#slType").val();
  const number = $("#slNumberOnPage").val();
  const search = $("#search").val();
  const data = {
    min, max, type, number, search
  };
  $form = $("#frmFilter");
  $form.append(`<input type="hidden" name="start" value="${min}">`);
  $form.append(`<input type="hidden" name="finish" value="${max}">`);
  $form.append(`<input type="hidden" name="string" value="${type}">`);
  $form.append(`<input type="hidden" name="number" value="${number}">`);
  $form.append(`<input type="hidden" name="search" value="${search}">`);
  //debugger
  $form.submit();
  // console.log(data);
});
$('#nou').ready(function(){
var startSlider = document.getElementById('nou');
noUiSlider.create(startSlider, {
    start: [{{str_replace('.', '', request('start',0))}}, {{str_replace('.', '', request('finish',2000000))}}],
    step: 50000,
    range: {
        'min': 0,
        'max': 2000000,
    },
    format: wNumb({
        decimals: 0,
        thousand: '.',
    })
});
var snapValues = [
    document.getElementById('slider-snap-value-lower'),
    document.getElementById('slider-snap-value-upper')
];

startSlider.noUiSlider.on('update', function (values, handle) {
    snapValues[handle].innerHTML = values[handle];
});
});
//quick view modal
$(document).ready(function(){
  $('.view-data').click(function(){
    var prd_id = $(this).attr("id");
    $.ajaxSetup({
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
    });
    $.ajax({
      url:'{{route('getDataModal')}}',
      method:"post",
      data:{"prd_id":prd_id},
      success:function(data){
        $("#nameprd").text(data.prd.name);
        $("#price").text(number_format(data.prd.price) + " VNĐ");
        if(data.prd.quantity>0){
          $("#state").text("Còn hàng");
        }
        else{
          $("#state").text("Hết hàng");
        }
        $("#info").text(data.prd.info);
        $("#cate").text(data.cate.name);
        $("#img-prd").attr("src","/backend/img/"+ data.prd.img);
        $("#detail").attr("href","/product/detail/"+data.prd.slug+"-"+data.prd.id);
        $("#img-sd").attr("data-lens-image","/backend/imgslide/"+ data.prd.img);
        data.prd_attr.forEach(function(item){
          //$( "select" ).html( "<option value ="">"+item.name+"</option>" );
          //$("").append("<option value ="item.id">"+item.nam+"</option>");
          $('#option').append($('<option >', { value : item.id }).text(item.name));     
        });

      }
    });
  });
});
//quick-add-cart
$(document).ready(function(){
  $('.add-view').one("click", function(){
    var prd_id = $(this).attr("id");
    $.ajaxSetup({
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
    });
    $.ajax({
      url:'{{route('getDataAddcart')}}',
      method:"post",
      data:{"prd_id":prd_id},     
      success:function(data){
        $("#prd-nameq").text("Sản phẩm :"+data.prd_add.name);
        $("#prd-add").attr("src","/backend/img/"+ data.prd_add.img);
        $("#qty").text("Sản phẩm còn :"+data.prd_attr_add['0'].quantity);
        $('#option-add').find('option').remove().end()
        data.prd_attr_add.forEach(function(item,index){
          $('#option-add').append($('<option>', { value : item.id ,key : index }).text(item.name));     
        });
        //console.log(data);
        $('#option-add').change(function(){
         var option = $('option:selected', this).attr('key');
         $("#qty").text("Sản phẩm còn :"+data.prd_attr_add[option].quantity);
        });
      }
  });
});
});
//click ok-add-cart
$(document).ready(function(){
$("#send-add-cart").click(function(){
    var loai = $("#option-add").val();
    var quantity = $("#quantity-add").val();
    // alert(loai);
    $.ajax({
      url:'{{route('addcart')}}',
      method:"get",
      data:{"loai":loai,"quantity":quantity},
        success:function(data){    
          var count = {{count(Cart::content())}};    
          $("#count").text(count);
        }
    });
  });
});
//quick view add-cart
$(document).ready(function(){
$("#addcart-m").click(function(e){
    e.preventDefault();
    var loai = $("#option").val();
    var quantity = 1;
    // alert(loai);
    $.ajax({
      url:'{{route('addcart')}}',
      method:"get",
      data:{"loai":loai,"quantity":quantity},
        success:function(data){    
          var count = {{count(Cart::content())}};    
          $("#count").text(count);
        }
    });
  });
  
});
</script>
@endsection