@extends('frontend.master.master')
@section('content')
<div class="container">
    <div class="row my-2">
        <div class="col-lg-8 order-lg-2">
            @if (session('thongbao'))
            <div class="alert alert-success" role="alert">
            <strong>{{session('thongbao')}}</strong>
           </div>
            @endif
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Profile</a>
                </li>
                <li class="nav-item">
                    <a href="" data-target="#edit" data-toggle="tab" class="nav-link">Other info</a>
                </li>
            </ul>
            <div class="tab-content py-4">
                <div class="tab-pane active" id="profile">
                    <h5 class="mb-3">Thông tin cá nhân.</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <h6>Tên</h6>
                            <p>
                                {{$user->name}}
                            </p>
                            <h6>Email</h6>
                            <p>
                                {{$user->mail}}
                            </p>
                            <h6>Address</h6>
                            <p>
                                {{$user->address}}
                            </p>
                        </div>
                    </div>
                    <!--/row-->
                </div>
                <div class="tab-pane" id="edit">
                <form  method="POST" action="{{route('postProfile',Auth::user()->id)}}">
                    @csrf
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Name</label>
                            <div class="col-lg-9">
                            <input class="form-control" name="name" type="text" value=" {{$user->name}}">
                            {{showErrors($errors,'name')}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Email</label>
                            <div class="col-lg-9">
                                <input class="form-control" name="email" type="email" value="{{$user->email}}">
                                {{showErrors($errors,'email')}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Address</label>
                            <div class="col-lg-9">
                                <input class="form-control" name="address" type="text" value="{{$user->address}}" placeholder="">
                                {{showErrors($errors,'address')}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Phone</label>
                            <div class="col-lg-9">
                                <input class="form-control" name="phone" type="text" value="{{$user->phone}}" placeholder="">
                                {{showErrors($errors,'phone')}}                           
                             </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Edit password</label>
                            <div class="col-lg-9">
                            <input class="form-control" name="password" type="password" value="">
                            {{showErrors($errors,'password')}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Confirm password</label>
                            <div class="col-lg-9">
                            <input class="form-control" name="confirmpassword" type="password" value="">
                            {{showErrors($errors,'password')}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label"></label>
                            <div class="col-lg-9">
                                <a href="/" type="reset"  class="btn btn-danger">Cancel </a>
                                <button  type="submit" class="btn btn-primary">Save Changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection