@extends('frontend.master.master')
@section('content')
<style>
#invoice{
   padding: 30px;
}

.invoice {
   background-color: #FFF;
   min-height: 680px;
   padding: 15px
}
.invoice .company-details {
   text-align: right
}

.invoice .company-details .name {
   margin-top: 0;
   margin-bottom: 0
}

.invoice .contacts {
   margin-bottom: 20px
}

.invoice .invoice-to {
   text-align: left
}

.invoice .invoice-to .to {
   margin-top: 0;
   margin-bottom: 0
}

.invoice .invoice-details {
   text-align: right
}

.invoice .invoice-details .invoice-id {
   margin-top: 0;
   color: #3989c6
}

.invoice main {
   padding-bottom: 50px
}

.invoice main .thanks {
   margin-top: -100px;
   font-size: 2em;
   margin-bottom: 50px
}

.invoice main .notices {
   padding-left: 6px;
   border-left: 6px solid #3989c6
}

.invoice main .notices .notice {
   font-size: 1.2em
}

.invoice table {
   width: 100%;
   border-collapse: collapse;
   border-spacing: 0;
   margin-bottom: 20px
}

.invoice table td,.invoice table th {
   padding: 15px;
   background: #eee;
   border-bottom: 1px solid #fff
}

.invoice table th {
   white-space: nowrap;
   font-weight: 400;
   font-size: 16px
}

.invoice table td h3 {
   margin: 0;
   font-weight: 400;
   color: #3989c6;
   font-size: 1.2em
}

.invoice table .qty,.invoice table .total,.invoice table .unit {
   text-align: right;
   font-size: 1.2em
}

.invoice table .no {
   color: #fff;
   font-size: 1.6em;
   background: #3989c6
}

.invoice table .unit {
   background: #ddd
}

.invoice table .total {
   background: #3989c6;
   color: #fff
}

.invoice table tbody tr:last-child td {
   border: none
}

.invoice table tfoot td {
   background: 0 0;
   border-bottom: none;
   white-space: nowrap;
   text-align: right;
   padding: 10px 20px;
   font-size: 1.2em;
   border-top: 1px solid #aaa
}

.invoice table tfoot tr:first-child td {
   border-top: none
}

.invoice table tfoot tr:last-child td {
   color: #3989c6;
   font-size: 1.4em;
   border-top: 1px solid #3989c6
}

.invoice table tfoot tr td:first-child {
   border: none
}

.invoice footer {
   width: 100%;
   text-align: center;
   color: #777;
   border-top: 1px solid #aaa;
   padding: 8px 0
}

@media print {
   .invoice {
       font-size: 11px!important;
       overflow: hidden!important
   }

   .invoice footer {
       position: absolute;
       bottom: 10px;
       page-break-after: always
   }

   .invoice>div:last-child {
       page-break-before: always
   }
}</style>
<script>
    $('#printInvoice').click(function(){
           Popup($('.invoice')[0].outerHTML);
           function Popup(data) 
           {
               window.print();
               return true;
           }
});
</script>
<!--Author      : @arboshiki-->
<div id="invoice">
   <div class="invoice overflow-auto">
       <div style="min-width: 600px">
               <div class="row">
                   <div class="col company-details">
                       <h2 class="name">
                           <a target="_blank" href="">
                          DkShop
                           </a>
                       </h2>
                       <div>401 Nguyễn Khang Yên Hòa Cầu Giấy</div>
                       <div>0357093377</div>
                       <div>dkcuongz@gmail.com</div>
                   </div>
               </div>   
               <div class="row contacts">
                   <div class="col invoice-to">
                       <div class="text-gray-light">INVOICE TO:</div>
                   <h2 class="to">{{$order->name}}</h2>
                       <div class="address">{{$order->address}}</div>
                   </div>
                   <div class="col invoice-details">
                       <h1 class="invoice-id">INVOICE {{$order->id}}</h1>
                       <div class="date">Date of Invoice: {{date('Y-m-d', strtotime($order->created_at))}}</div>
                   </div>
               </div>
               <table border="0" cellspacing="0" cellpadding="0">
                   <thead>
                       <tr>
                           <th>ID</th>
                           <th class="text-left">Sản Phẩm</th>
                           <th class="text-right">Số Lượng</th>
                           <th class="text-right">Đơn Giá</th>
                           <th class="text-right">Tổng</th>
                       </tr>
                   </thead>
                   <tbody>
                       @foreach ($detail as $item)                                            
                       <tr>
                           <td class="no">{{$item->id}}</td>
                           <td class="text-left">{{$item->order_dt_prdatt->name}}</td>
                           <td class="unit">x{{$item->quantity}}</td>
                           <td class="qty">{{number_format($item->price,0, ',', '.')}}VNĐ</td>
                           <td class="total">{{number_format($item->price * $item->quantity,0, ',', '.')}}</td>
                       </tr>   
                       @endforeach               
                   </tbody>
                   <tfoot>
                       <tr>
                           <td colspan="2"></td>
                           <td colspan="2">Tổng</td>
                           <td>{{number_format($order->total,0, ',', '.')}}VNĐ</td>
                       </tr>
                       <tr>
                           <td colspan="2"></td>
                           <td colspan="2">Giảm giá</td>
                           <td>{{number_format($order->discount,0, ',', '.')}}VNĐ</td>
                       </tr>
                       <tr>
                           <td colspan="2"></td>
                           <td colspan="2">Thành tiền</td>
                           <td>{{number_format($order->total-$order->discount,0, ',', '.')}}VNĐ</td>
                       </tr>
                   </tfoot>
               </table>
            <div style="float: right"><a href="/user/order/{{Auth::user()->id}}" class="btn btn-danger"> Quay lại</a></div>
               <div class="thanks">Thank you!</div>
               <div class="notices">
                   <div>NOTICE:</div>
                   <div class="notice">Thanh toán khi nhận hàng</div>
               </div>
       </div>
       <div></div>
   </div>
</div>
@endsection