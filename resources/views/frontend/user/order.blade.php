@extends('frontend.master.master')
@section('content')
	<!--main-->
	<div class="col-sm-12  col-lg-12 col-lg-offset-0 main">
		<!--/.row-->
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">

				<div class="panel panel-primary">
					<div class="panel-heading">Danh sách đơn hàng </div>
					<div class="panel-body">
						<div class="bootstrap-table">
							<div class="table-responsive">

								<a href="/" class="btn btn-success">Trang chủ</a>
								<table class="table table-bordered" style="margin-top:20px;">
									<thead>
										<tr class="bg-primary">
											<th>ID</th>
											<th>Tên khách hàng</th>
											<th>Tổng tiền</th>
											<th>Giảm giá</th>
											<th>SĐT</th>
											<th>Địa chỉ</th>
											<th>Trạng thái</th>
											<th width = "15%">Tùy chọn</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($order as $item)
										<tr>
											<td>{{$item->id}}</td>
												<td>{{$item->name}}</td>											
												<td>{{number_format($item->total,0, ',', '.')}} VNĐ</td>
												<td>{{number_format($item->discount,0, ',', '.')}} VNĐ</td>
												<td>{{$item->phone}}</td>
												<td>{{$item->address}}</td>
												<td>@if ($item->state == 0)
													Chờ xác nhận
													@elseif($item->state == 1)
													Đã xác nhận/Đang giao
													@elseif($item->state == 2)
													Đã hủy
													@elseif($item->state == 3)
													Đã hoàn thành
													@endif
												</td>
												<td>
												@if ($item->state == 0)
												<a  class="btn btn-warning mhd" mhd="{{$item->id}}" data-href="/user/{{Auth::user()->id}}/order/refuse/{{$item->id}}" data-toggle="modal" data-target="#confirm-delete">Hủy</a>			
												@endif
												<a href="/user/{{Auth::user()->id}}/order/detail/{{$item->id}} "  class="btn btn-success"><i class="fas fa-eye"></i> Chi tiết</a>
												@if ($item->state != 2)
												<a href="{{route('getOrderPdf',[Auth::user()->id,$item->id])}}" class="btn btn-success "><i class="fas fa-file-export"></i></a>							
												@endif
												</td>
											</tr>	
										@endforeach																		
									</tbody>
								</table>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<!--/.row-->
	</div>
	<!--end main-->	
	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Bạn có chắc chắn muốn xóa đơn hàng không</h4>
                </div>
            
                <div class="modal-body">
                    <p>Đơn hàng đang chờ xác nhận.</p>
					<p>Bạn vẫn muốn hủy?</p>
					<p class="debug-url"></p>
                </div>
                
                <div class="modal-footer">
					<a class="btn btn-danger btn-ok">Hủy</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
@parent 
<script>
	$('#confirm-delete').on('show.bs.modal', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	});
	$(document).ready(function(){
	$(".mhd").click(function(){
		var mhd_id = $(this).attr("mhd");
			$('.debug-url').html('Mã hóa đơn: <strong>' + mhd_id + '</strong>');
		});	
	});	
</script>
@endsection