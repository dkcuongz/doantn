@extends('frontend.master.master')
@section('title','Thanh toán')
@section('content')   
 <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
    <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
    <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Trang thanh toán</h2>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>                   
          <li class="active">Thanh toán</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

 <!-- Cart view section -->
 <section id="checkout">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="checkout-area">
            <div class="row">
              <div class="col-md-8">
                <div class="checkout-left">
                  <div class="panel-group" id="accordion">
                    <!-- Coupon section -->
                    <form action="{{route('postCoupon')}}" method="POST">
                      @csrf
                    <div class="panel panel-default aa-checkout-coupon">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" >
                            Nhập mã khuyến mãi?
                          </a>
                        </h4>
                      </div>
                      <div id="collapseTwo" class="panel-collapse collapse ">
                        <div class="panel-body">
                          <input type="text" name = "code" placeholder="Coupon Code" class="aa-coupon-code">
                          <input type="submit" value="Apply Coupon" class="aa-browse-btn">
                        </div>
                      </div>
                    </div>
                  </form>
                  <form action="{{route('checkout')}}" method="POST">
                    @csrf
                    <!-- Billing Details -->
                    <div class="panel panel-default aa-checkout-billaddress">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" >
                            Thông tin hóa đơn
                          </a>
                        </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse in">                      
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="aa-checkout-single-bill">
                              <input type="text" name="name"  @if (Auth::user()) value="{{Auth::user()->name}}" @endif  placeholder="Name*">
                              {{showErrors($errors,'name')}}
                              </div>                             
                            </div>                         
                          </div> 
                          <div class="row">
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                <input name="email"  @if (Auth::user()) value="{{Auth::user()->email}}" @endif type="email" placeholder="Email Address*">
                                {{showErrors($errors,'email')}}
                              </div>                             
                            </div>
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                <input name="phone"  @if (Auth::user()) value="{{Auth::user()->phone}}" @endif type="tel" placeholder="Phone*">
                                {{showErrors($errors,'phone')}}
                              </div>
                            </div>
                          </div> 
                          <div class="row">
                            <div class="col-md-12">
                              <div class="aa-checkout-single-bill">
                                <textarea name="address" cols="8" rows="3"> @if (Auth::user()) {{Auth::user()->address}} @else Address* @endif</textarea>
                                {{showErrors($errors,'address')}}
                              </div>                             
                            </div>                            
                          </div>                         
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="checkout-right">
                  <h4>Tóm tắt hóa đơn</h4>
                  @if (session('thongbao'))
                  <div class="alert alert-danger col-lg-12" role="alert">
                          <strong>{{session('thongbao')}}</strong>
                  </div>
                  @endif
                  @if (session('thongbao1'))
                  <div class="alert alert-success col-lg-12" role="alert">
                          <strong>{{session('thongbao1')}}</strong>
                  </div>
                  @endif
                  <div class="aa-order-summary-area">
                    <table class="table table-responsive">
                      <thead>
                        <tr>
                          <th>Sản phẩm</th>
                          <th>Tổng</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach  ($cart as $item)
                        <tr>
                          <td>{{$item->name}} <strong> x  {{$item->qty}}</strong></td>
                          <td>{{number_format($item->price * $item->qty,0, ',', '.')}}VNĐ</td>
                        </tr>
                        @endforeach
                      <tfoot>
                        <tr>
                          <th>Tổng tiền</th>
                          <td>{{$total}}VNĐ</td>
                        </tr>
                         <tr>
                          <th>Giảm giá</th>
                          <td>@if (Auth::user() &&Auth::user()->id == session('user_id'))
                            @if (session('discount')>100)
                            {{number_format(session('discount'),0, ',', '.')}} VNĐ
                            @else   {{number_format($total_s*session('discount')/100,0, ',', '.')}}VNĐ
                            @endif                                                    
                         @else
                        {{"0 VNĐ"}}
                         @endif</td>
                        </tr>
                         <tr>
                          <th>Thành tiền</th>
                          <td>@if (Auth::user() &&Auth::user()->id == session('user_id'))
                            @if (session('discount')>100)
                            {{number_format($total_s-session('discount'),0, ',', '.')}} VNĐ
                            @else   {{number_format($total_s*(100-session('discount'))/100,0, ',', '.')}}VNĐ
                            @endif                                                    
                           @else
                           {{$total}} VNĐ
                         @endif</td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                  <h4>Phương thức thanh toán</h4>
                  <div class="aa-payment-method">                    
                    <label for="cashdelivery"><input type="radio" id="cashdelivery" name="optionsRadios" checked> Thanh toán khi nhận hàng</label>
                    <input type="submit" value="Place Order" class="aa-browse-btn">                
                  </div>
                </div>
              </div>
            </div>
          </form>
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->
@endsection