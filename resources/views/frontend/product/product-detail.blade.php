@extends('frontend.master.master')
@section('title','Product Detail')
@section('content')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=2942424942518320&autoLogAppEvents=1"></script>
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
    <img width="1920px" height="300px" src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
    <div class="aa-catg-head-banner-area">
      <div class="container">
       <div class="aa-catg-head-banner-content">
       <h2>{{$prd->category->name}}</h2>
         <ol class="breadcrumb">
           <li><a href="/">Home</a></li>         
           <li><a href="/product/shop">Shop</a></li>
         <li class="active">{{$prd->name}}</li>
         </ol>
       </div>
      </div>
    </div>
   </section>
   <!-- / catg header banner section -->
  <!-- product category -->
  <section id="aa-product-details">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-product-details-area">
            <div class="aa-product-details-content">
              <div class="row">
                <!-- Modal view slider -->
                <div class="col-md-5 col-sm-5 col-xs-12">                              
                  <div class="aa-product-view-slider">                                
                    <div id="demo-1" class="simpleLens-gallery-container">
                      <div class="simpleLens-container">
                        <div class="simpleLens-big-image-container"><a data-lens-image="/backend/imgslide/{{$prd->img}}" class="simpleLens-lens-image">
                          <img src="/backend/img/{{$prd->img}}" class="simpleLens-big-image"></a></div>
                      </div>                 
                    </div>
                  </div>
                </div>
                <!-- Modal view content -->
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class="aa-product-view-content">
                    <h3 style="margin-top: 0px">{{$prd->name}}</h3>
                    <div class="aa-price-block">
                      <span class="aa-product-view-price">{{number_format($prd->price,0, ',', '.')}} VNĐ</span>
                      <p class="aa-product-avilability">Trạng thái: <span>@if ($prd->price>0)
                        Còn hàng
                        @else
                        Hết hàng
                      @endif</span></p>
                    </div>
                    <p>{{$prd->info}}</p>
                    <h4>Loại</h4>
                    <div class="aa-prod-view-size">
                    @foreach ($prd->prd_prd_attr as $item)
                    @if ($item->quantity > 0)
                    <a>{{str_replace($prd->name,"", $item->name)}}</a>
                    @endif   
                    @endforeach
                    </div>
                    <div class="aa-prod-quantity">
                    <form action="{{route('addcart')}}" method="GET">
                      <input id="qty-max" style="text-align: center; width:70px" type="number" value="1" name="quantity">                    
                      <select style="height: 28px;width:200px" name="loai" id="option-add-cart" >
                        @foreach ($prd->prd_prd_attr as $key => $item)
                        @if ($item->quantity > 0)
                        <option key="{{$key}}"  value="{{$item ->id}}">{{str_replace($prd->name,"",$item->name)}}</option>
                        @endif                     
                        @endforeach
                      </select>
                      <p style="float: right;" id = "qty"></p>
                      <p class="aa-prod-category" style="margin-left: 0px; margin-top: 10px; font-size: 20px">
                        Danh mục: <a href="/product/{{$prd->category->slug}}.html">{{$prd->category->name}}</a>
                      </p>
                    </div>
                    <div class="aa-prod-view-bottom">
                      <button style="margin-top: 0px" type="submit" value="addcart" class="aa-add-to-cart-btn" name="addcart">Thêm vào giỏ</button>             
                    </div>
                  </form>
                  </div>
                </div>
              </div>
            </div>
            <div class="aa-product-details-bottom">
              <ul class="nav nav-tabs" id="myTab2">
                <li><a href="#description" data-toggle="tab">Mô tả</a></li>
                <li><a href="#review" data-toggle="tab">Bình luận</a></li>                
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane fade in active" id="description">
                 {{$prd->describle}}
                </div>
                <div class="tab-pane fade " id="review">
                 <div class="aa-product-review-area">
                  <div class="fb-comments" data-href="https://www.facebook.com/Dk.CuongZ" data-numposts="5" data-width="auto"></div>
                 </div>
                </div>            
              </div>
            </div>
            <!-- Related product -->
            <div class="aa-product-related-item">
              <h3>Sản phẩm liên quan</h3>
              <ul class="aa-product-catg aa-related-item-slider">
                @foreach ($prd_relate as $item)
                <!-- start single product item -->
                <li>
                  <figure>
                  <a class="aa-product-img" href="/product/detail/{{$item->slug}}-{{$item->id}}"><img src="/backend/img/{{$item->img}}" alt="{{$item->describle}}"></a>
                    <a class="aa-add-card-btn"href="/product/detail/{slug_prd}"><span class="fa fa-shopping-cart"></span>Thêm vào giỏ</a>
                      <figcaption>
                      <h4 class="aa-product-title"><a href="/product/detail/{{$item->slug}}-{{$item->id}}">{{$item->name}}</a></h4>
                      <span class="aa-product-price">{{number_format($item->price,0, ',', '.')}} VNĐ </span>
                    </figcaption>
                  </figure>                        
                  <div class="aa-product-hvr-content">
                    <a class="view-data" id ="{{$item->id}}" href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal"><span class="fa fa-search"></span></a>                          
                  </div>
                  <!-- product badge -->
                  {{-- <span class="aa-badge aa-sale" href="#">SALE!</span> --}}
                  {{-- <span class="aa-badge aa-sold-out" href="#">Sold Out!</span>
                  <span class="aa-badge aa-hot" href="#">HOT!</span> --}}
                </li>
                @endforeach
              </ul>
              <!-- quick view modal -->                  
              <div class="modal fade" id="quick-view-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">                      
                    <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <div class="row" id = "product-detail">
                        <!-- Modal view slider -->
                        <div class="col-md-5 col-sm-5 col-xs-12">                              
                          <div class="aa-product-view-slider">                                
                            <div id="demo-1" class="simpleLens-gallery-container">
                              <div class="simpleLens-container">
                                <div class="simpleLens-big-image-container"><a data-lens-image="/backend/imgslide/" class="simpleLens-lens-image">
                                  <img src="/backend/img/" class="simpleLens-big-image"></a></div>
                              </div>                 
                            </div>
                          </div>
                        </div>
                        <!-- Modal view content -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="aa-product-view-content">
                            <h3 id ="nameprd"></h3>
                            <div class="aa-price-block">
                              <span id ="price" class="aa-product-view-price"></span>
                              <p class="aa-product-avilability">Trạng thái: <span id="state">Còn hàng</span></p>
                            </div>
                            <p id = "info"></p>                           
                            <div class="aa-prod-quantity">
                              <form action="">
                                <select name="" id="">
                                  <option value="0" selected="1">1</option>
                                  <option value="1">2</option>
                                  <option value="2">3</option>
                                  <option value="3">4</option>
                                  <option value="4">5</option>
                                  <option value="5">6</option>
                                </select>
                              </form>
                              <p class="aa-prod-category">
                                Danh mục: <a id="cate" href="#"></a>
                              </p>
                            </div>
                            <div class="aa-prod-view-bottom">
                              <a href="#" class="aa-add-to-cart-btn"><span class="fa fa-shopping-cart"></span>Thêm vào giỏ</a>
                              <a href="#" class="aa-add-to-cart-btn">Xem chi tiết</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>                        
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div>
              <!-- / quick view modal -->  
            </div>  
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / product category -->
  @include('frontend.master.subcribe')
@endsection
@section('script')
    @parent
    <script type="text/javascript">
    $(document).ready(function(){
    $('.view-data').click(function(){
    var prd_id = $(this).attr("id");
    $.ajaxSetup({
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
    });
    $.ajax({
      url:'{{route('getDataModal')}}',
      method:"post",
      data:{"prd_id":prd_id},
      success:function(data){
        // $('#quick-view-modal').html(data);
        // $('#quick-view-modal').modal("Show");
        $("#nameprd").text(data.prd.name);
        $("#price").text(number_format(data.prd.price) + " VNĐ");
        if(data.prd.quantity>0){
          $("#state").text("Còn hàng");
        }
        else{
          $("#state").text("Hết hàng");
        }
        $("#info").text(data.prd.info);
        $("#cate").text(data.cate.name);
        console.log(data);
      }
    });
  });
});
$(document).ready(function(){
  var prd_id = $(".view-data").attr("id");
  $.ajaxSetup({
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
    });
    $.ajax({
      url:'{{route('getDataAddcart')}}',
      method:"post",
      data:{"prd_id":prd_id},     
      success:function(data){
        $("#qty").text("Sản phẩm còn :"+data.prd_attr_add['0'].quantity);
        $('#option-add-cart').change(function(){
         var option = $('option:selected', this).attr('key');
         $("#qty").text("Sản phẩm còn :"+data.prd_attr_add[option].quantity);
         $("#qty-max").attr("max",data.prd_attr_add[option].quantity);
        });
      }
  });
});
    </script>
@endsection