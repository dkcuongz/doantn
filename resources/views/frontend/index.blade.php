@extends('frontend.master.master')
@section('title','Home')
@section('content')
  <!-- Start slider -->
  <section id="aa-slider">
    <div class="aa-slider-area">
      <div id="sequence" class="seq">
        <div class="seq-screen">
          <ul class="seq-canvas">
            <!-- single slide item -->
            <li>
              <div class="seq-model">
                <img data-seq src="img/slider/1.jpg" alt="Men slide img" />
              </div>
              <div class="seq-title">
               <span data-seq>Save Up to 75% Off</span>                
                <h2 data-seq>Men Collection</h2>                
                <p data-seq>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, illum.</p>
                <a data-seq href="#" class="aa-shop-now-btn aa-secondary-btn">SHOP NOW</a>
              </div>
            </li>
            <!-- single slide item -->
            <li>
              <div class="seq-model">
                <img data-seq src="img/slider/2.jpg" alt="Wristwatch slide img" />
              </div>
              <div class="seq-title">
                <span data-seq>Save Up to 40% Off</span>                
                <h2 data-seq>Wristwatch Collection</h2>                
                <p data-seq>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, illum.</p>
                <a data-seq href="#" class="aa-shop-now-btn aa-secondary-btn">SHOP NOW</a>
              </div>
            </li>
            <!-- single slide item -->
            <li>
              <div class="seq-model">
                <img data-seq src="img/slider/3.jpg" alt="Women Jeans slide img" />
              </div>
              <div class="seq-title">
                <span data-seq>Save Up to 75% Off</span>                
                <h2 data-seq>Jeans Collection</h2>                
                <p data-seq>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, illum.</p>
                <a data-seq href="#" class="aa-shop-now-btn aa-secondary-btn">SHOP NOW</a>
              </div>
            </li>
            <!-- single slide item -->           
            <li>
              <div class="seq-model">
                <img data-seq src="img/slider/4.jpg" alt="Shoes slide img" />
              </div>
              <div class="seq-title">
                <span data-seq>Save Up to 75% Off</span>                
                <h2 data-seq>Exclusive Shoes</h2>                
                <p data-seq>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, illum.</p>
                <a data-seq href="#" class="aa-shop-now-btn aa-secondary-btn">SHOP NOW</a>
              </div>
            </li>
            <!-- single slide item -->  
             <li>
              <div class="seq-model">
                <img data-seq src="img/slider/5.jpg" alt="Male Female slide img" />
              </div>
              <div class="seq-title">
                <span data-seq>Save Up to 50% Off</span>                
                <h2 data-seq>Best Collection</h2>                
                <p data-seq>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, illum.</p>
                <a data-seq href="#" class="aa-shop-now-btn aa-secondary-btn">SHOP NOW</a>
              </div>
            </li>                   
          </ul>
        </div>
        <!-- slider navigation btn -->
        <fieldset class="seq-nav" aria-controls="sequence" aria-label="Slider buttons">
          <a type="button" class="seq-prev" aria-label="Previous"><span class="fa fa-angle-left"></span></a>
          <a type="button" class="seq-next" aria-label="Next"><span class="fa fa-angle-right"></span></a>
        </fieldset>
      </div>
    </div>
  </section>
  <!-- / slider -->
  <!-- Start Promo section -->
  <section id="aa-promo">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-promo-area">
            <div class="row">
              <!-- promo left -->
              <div class="col-md-5 no-padding">                
                <div class="aa-promo-left">
                  <div class="aa-promo-banner">                    
                    <img src="img/promo-banner-1.jpg" alt="img">                    
                    <div class="aa-prom-content">
                      <span>75% Off</span>
                      <h4><a href="#">For Women</a></h4>                      
                    </div>
                  </div>
                </div>
              </div>
              <!-- promo right -->
              <div class="col-md-7 no-padding">
                <div class="aa-promo-right">
                  <div class="aa-single-promo-right">
                    <div class="aa-promo-banner">                      
                      <img src="img/promo-banner-3.jpg" alt="img">                      
                      <div class="aa-prom-content">
                        <span>Exclusive Item</span>
                        <h4><a href="#">For Men</a></h4>                        
                      </div>
                    </div>
                  </div>
                  <div class="aa-single-promo-right">
                    <div class="aa-promo-banner">                      
                      <img src="img/promo-banner-2.jpg" alt="img">                      
                      <div class="aa-prom-content">
                        <span>Sale Off</span>
                        <h4><a href="#">On Shoes</a></h4>                        
                      </div>
                    </div>
                  </div>
                  <div class="aa-single-promo-right">
                    <div class="aa-promo-banner">                      
                      <img src="img/promo-banner-4.jpg" alt="img">                      
                      <div class="aa-prom-content">
                        <span>New Arrivals</span>
                        <h4><a href="#">For Kids</a></h4>                        
                      </div>
                    </div>
                  </div>
                  <div class="aa-single-promo-right">
                    <div class="aa-promo-banner">                      
                      <img src="img/promo-banner-5.jpg" alt="img">                      
                      <div class="aa-prom-content">
                        <span>25% Off</span>
                        <h4><a href="#">For Bags</a></h4>                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Promo section -->
  <!-- Products section -->
  <section id="aa-product">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="aa-product-area">
              <div class="aa-product-inner">
                <!-- start prduct navigation -->
                 <ul class="nav nav-tabs aa-products-tab">
                    <li class="active"><a href="#men" data-toggle="tab">Nam</a></li>
                    <li><a href="#women" data-toggle="tab">Nữ</a></li>
               
                  </ul>
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <!-- Start men product category -->
                    <div class="tab-pane fade in active" id="men">
                      <ul class="aa-product-catg">
                       @foreach ($prd_men as $item)
                            <!-- start single product item -->
                            <li>
                              <figure>
                              <a class="aa-product-img" href="/product/detail/{{$item->slug}}-{{$item->id}}"><img src="/backend/img/{{$item->img}}" alt="{{$item->describe}}"></a>
                                <a id ="{{$item->id}}"  data-toggle="modal" data-target="#myModal" class="aa-add-card-btn add-view" href=""><span class="fa fa-shopping-cart"></span>Thêm vào giỏ</a>
                                  <figcaption>
                                  <h4 class="aa-product-title"><a href="/product/detail/{{$item->slug}}-{{$item->id}}">{{$item->name}}</a></h4>
                                  <span class="aa-product-price">{{number_format($item->price,0, ',', '.')}} VNĐ</span>
                                  {{-- <span class="aa-product-price"><del>$65.50</del></span> --}}
                               
                                </figcaption>
                              </figure>                        
                              <div class="aa-product-hvr-content">             
                                <a class="view-data" id ="{{$item->id}}" href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal"><span class="fa fa-search"></span></a>                          
                              </div>
                              <!-- product badge -->
                              {{-- <span class="aa-badge aa-sale" href="#">SALE!</span> --}}
                              {{-- <span class="aa-badge aa-sold-out" href="#">Sold Out!</span>
                              <span class="aa-badge aa-hot" href="#">HOT!</span> --}}
                            </li>
                       @endforeach                      
                      </ul>
                      <a class="aa-browse-btn" href="/product/shop">Đến trang sản phẩm <i class="fas fa-arrow-right"></i></span></a>
                    </div>
                    <!-- / men product category -->
                    <!-- start women product category -->
                    <div class="tab-pane fade" id="women">
                      <ul class="aa-product-catg">
                        @foreach ($prd_women as $item)
                        <!-- start single product item -->
                        <li>
                          <figure>
                          <a class="aa-product-img" href="/product/detail/{{$item->slug}}-{{$item->id}}"><img src="/backend/img/{{$item->img}}" alt="{{$item->describe}}"></a>
                            <a id ="{{$item->id}}"  data-toggle="modal" data-target="#myModal" class="aa-add-card-btn add-view" href=""><span class="fa fa-shopping-cart"></span>Thêm vào giỏ</a>
                              <figcaption>
                              <h4 class="aa-product-title"><a href="/product/detail/{{$item->slug}}-{{$item->id}}">{{$item->name}}</a></h4>
                              <span class="aa-product-price">{{number_format($item->price,0, ',', '.')}} VNĐ</span>
                              {{-- <span class="aa-product-price"><del>$65.50</del></span> --}}
                           
                            </figcaption>
                          </figure>                        
                          <div class="aa-product-hvr-content">             
                            <a class="view-data" id ="{{$item->id}}" href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal"><span class="fa fa-search"></span></a>                          
                          </div>
                         
                        </li>
                   @endforeach                     
                      </ul>
                      <a class="aa-browse-btn" href="/product/shop">Đến trang sản phẩm <i class="fas fa-arrow-right"></i></a>
                    </div>
                    <!-- / women product category -->
                  </div>
                    <!-- add-cart-modal -->    
                  <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">              
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Chọn Loại</h4>
                        </div>
                        <div class="modal-body">
                          <form action="">
                            <p id="prd-nameq"> </p>
                            <img  id="prd-add" alt="">
                            <select style="height: 28px;width:250px" name="" id="option-add">
                            </select>
                            <input id="quantity-add"  style="text-align:center; width:70px;" type="number" value="1" name="quantity-add">
                            <p id ="qty"></p>
                          </form>
                        </div>
                        <div class="modal-footer">
                          <button id = "send-add-cart" type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                          <button type="button" class="btn btn-close" data-dismiss="modal">Đóng</button>
                        </div>
                      </div>           
                    </div>
                  </div>
                    <!-- add-cart-modal -->    
                  <!-- quick view modal -->                  
                  <div class="modal fade" id="quick-view-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">                      
                        <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <div class="row" id = "product-detail">
                            <!-- Modal view slider -->
                            <div class="col-md-5 col-sm-5 col-xs-12">                              
                              <div class="aa-product-view-slider">                                
                                <div id="demo-1" class="simpleLens-gallery-container">
                                  <div class="simpleLens-container">
                                    <div class="simpleLens-big-image-container"><a id="img-sd" data-lens-image="" class="simpleLens-lens-image">
                                      <img id="img-prd" src="" class="simpleLens-big-image"></a></div>
                                  </div>                 
                                </div>
                              </div>
                            </div>
                            <!-- Modal view content -->
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <div class="aa-product-view-content">
                                <h3 id ="nameprd"></h3>
                                <div class="aa-price-block">
                                  <span id ="price" class="aa-product-view-price"></span>
                                  <p class="aa-product-avilability">Trạng thái: <span id="state">Còn hàng</span></p>
                                </div>
                                <p id = "info"></p>
                                <div class="aa-prod-quantity">
                                  <form action="">
                                    <select style="height: 28px;width:250px" name="" id="option">
                                      
                                    </select>
                                  </form>
                                  <p style="margin: 15px 0 0 0" class="aa-prod-category">
                                  Danh mục: <a id="cate" href="#"></a>
                                  </p>
                                </div>
                                <div class="aa-prod-view-bottom">
                                  <button id="addcart-m" data-dismiss="modal"  class="aa-add-to-cart-btn"><span class="fa fa-shopping-cart"></span>Thêm vào giỏ</button>
                                  <a id ="detail"  class="aa-add-to-cart-btn">Xem chi tiết</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>                        
                      </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                  </div>
                  <!-- / quick view modal -->              
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Products section -->
  <!-- banner section -->
  <section id="aa-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12">        
          <div class="row">
            <div class="aa-banner-area">
            <a href="#"><img src="img/fashion-banner.jpg" alt="fashion banner img"></a>
          </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- popular section -->
  <section id="aa-popular-category">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="aa-popular-category-area">
              <!-- start prduct navigation -->
             <ul class="nav nav-tabs aa-products-tab">
                <li class="active"><a href="#popular" data-toggle="tab">Phổ biến</a></li>
                <li><a href="#featured" data-toggle="tab">Đặc sắc</a></li>
                <li><a href="#latest" data-toggle="tab">Mới nhất</a></li>                    
              </ul>
              <!-- Tab panes -->
              <div class="tab-content">
                <!-- Start men popular category -->
                <div class="tab-pane fade in active" id="popular">
                  <ul class="aa-product-catg aa-popular-slider">
                    <!-- start single product item -->
                    @foreach ($prd_popular as $item)
                    <!-- start single product item -->
                    <li>
                      <figure>
                      <a class="aa-product-img" href="/product/detail/{{$item->slug}}-{{$item->id}}"><img src="/backend/img/{{$item->img}}" alt="{{$item->describe}}"></a>
                        <a id ="{{$item->id}}"  data-toggle="modal" data-target="#myModal" class="aa-add-card-btn add-view" href=""><span class="fa fa-shopping-cart"></span>Thêm vào giỏ</a>
                          <figcaption>
                          <h4 class="aa-product-title"><a href="/product/detail/{{$item->slug}}-{{$item->id}}">{{$item->name}}</a></h4>
                          <span class="aa-product-price">{{number_format($item->price,0, ',', '.')}} VNĐ</span>
                          {{-- <span class="aa-product-price"><del>$65.50</del></span> --}}
                        </figcaption>
                      </figure>                        
                      <div class="aa-product-hvr-content">             
                        <a class="view-data" id ="{{$item->id}}" href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal"><span class="fa fa-search"></span></a>                          
                      </div>
                    </li>
               @endforeach                                                                                        
                  </ul>
                  <a class="aa-browse-btn" href="/product/shop">Đến trang sản phẩm <i class="fas fa-arrow-right"></i></a>
                </div>
                <!-- / popular product category -->
                
                <!-- start featured product category -->
                <div class="tab-pane fade" id="featured">
                 <ul class="aa-product-catg aa-featured-slider">
                    <!-- start single product item -->
                    @foreach ($prd_feature as $item)
                    <!-- start single product item -->
                    <li>
                      <figure>
                      <a class="aa-product-img" href="/product/detail/{{$item->slug}}-{{$item->id}}"><img src="/backend/img/{{$item->img}}" alt="{{$item->describe}}"></a>
                        <a id ="{{$item->id}}"  data-toggle="modal" data-target="#myModal" class="aa-add-card-btn add-view" href=""><span class="fa fa-shopping-cart"></span>Thêm vào giỏ</a>
                          <figcaption>
                          <h4 class="aa-product-title"><a href="/product/detail/{{$item->slug}}-{{$item->id}}">{{$item->name}}</a></h4>
                          <span class="aa-product-price">{{number_format($item->price,0, ',', '.')}} VNĐ</span>
                          {{-- <span class="aa-product-price"><del>$65.50</del></span> --}}
                       
                        </figcaption>
                      </figure>                        
                      <div class="aa-product-hvr-content">             
                        <a class="view-data" id ="{{$item->id}}" href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal"><span class="fa fa-search"></span></a>                          
                      </div>
                      <!-- product badge -->
                      {{-- <span class="aa-badge aa-sale" href="#">SALE!</span> --}}
                      {{-- <span class="aa-badge aa-sold-out" href="#">Sold Out!</span>
                      <span class="aa-badge aa-hot" href="#">HOT!</span> --}}
                    </li>
               @endforeach                                                                                             
                  </ul>
                  <a class="aa-browse-btn" href="/product/shop">Đến trang sản phẩm <i class="fas fa-arrow-right"></i></a>
                </div>
                <!-- / featured product category -->

                <!-- start latest product category -->
                <div class="tab-pane fade" id="latest">
                  <ul class="aa-product-catg aa-latest-slider">
                    <!-- start single product item -->
                    @foreach ($prd_new as $item)
                    <!-- start single product item -->
                    <li>
                      <figure>
                      <a class="aa-product-img" href="/product/detail/{{$item->slug}}-{{$item->id}}"><img src="/backend/img/{{$item->img}}" alt="{{$item->describe}}"></a>
                        <a id ="{{$item->id}}"  data-toggle="modal" data-target="#myModal" class="aa-add-card-btn add-view" href=""><span class="fa fa-shopping-cart"></span>Thêm vào giỏ</a>
                          <figcaption>
                          <h4 class="aa-product-title"><a href="/product/detail/{{$item->slug}}-{{$item->id}}">{{$item->name}}</a></h4>
                          <span class="aa-product-price">{{number_format($item->price,0, ',', '.')}} VNĐ</span>
                          {{-- <span class="aa-product-price"><del>$65.50</del></span> --}}
                       
                        </figcaption>
                      </figure>                        
                      <div class="aa-product-hvr-content">             
                        <a class="view-data" id ="{{$item->id}}" href="#" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" data-target="#quick-view-modal"><span class="fa fa-search"></span></a>                          
                      </div>
                      <!-- product badge -->
                      {{-- <span class="aa-badge aa-sale" href="#">SALE!</span> --}}
                      {{-- <span class="aa-badge aa-sold-out" href="#">Sold Out!</span>
                      <span class="aa-badge aa-hot" href="#">HOT!</span> --}}
                    </li>
               @endforeach                                                                                           
                  </ul>
                  <a class="aa-browse-btn" href="/product/shop">Đến trang sản phẩm <i class="fas fa-arrow-right"></i></a>
                </div>
                <!-- / latest product category -->              
              </div>
            </div>
          </div> 
        </div>
      </div>
    </div>
  </section>
  <!-- / popular section -->
  <!-- Subscribe section -->
  @include('frontend.master.subcribe')
  <!-- / Subscribe section -->
@endsection
@section('script')
@parent 
<script>
  //quick view modal
$(document).ready(function(){
  $('.view-data').click(function(){
    var prd_id = $(this).attr("id");
    $.ajaxSetup({
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
    });
    $.ajax({
      url:'{{route('getDataModal')}}',
      method:"post",
      data:{"prd_id":prd_id},
      success:function(data){
        $("#nameprd").text(data.prd.name);
        $("#price").text(number_format(data.prd.price) + " VNĐ");
        if(data.prd.quantity>0){
          $("#state").text("Còn hàng");
        }
        else{
          $("#state").text("Hết hàng");
        }
        $("#info").text(data.prd.info);
        $("#cate").text(data.cate.name);
        $("#img-prd").attr("src","/backend/img/"+ data.prd.img);
        $("#detail").attr("href","/product/detail/"+data.prd.slug+"-"+data.prd.id);
        $("#img-sd").attr("data-lens-image","/backend/imgslide/"+ data.prd.img);
        data.prd_attr.forEach(function(item){
          //$( "select" ).html( "<option value ="">"+item.name+"</option>" );
          //$("").append("<option value ="item.id">"+item.nam+"</option>");
          $('#option').append($('<option >', { value : item.id }).text(item.name));     
        });

      }
    });
  });
});
//quick-add-cart
$(document).ready(function(){
  $('.add-view').one("click", function(){
    var prd_id = $(this).attr("id");
    $.ajaxSetup({
      headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
    });
    $.ajax({
      url:'{{route('getDataAddcart')}}',
      method:"post",
      data:{"prd_id":prd_id},     
      success:function(data){
        $("#prd-nameq").text("Sản phẩm :"+data.prd_add.name);
        $("#prd-add").attr("src","/backend/img/"+ data.prd_add.img);
        $("#qty").text("Sản phẩm còn :"+data.prd_attr_add['0'].quantity);
        $('#option-add').find('option').remove().end()
        data.prd_attr_add.forEach(function(item,index){
          $('#option-add').append($('<option>', { value : item.id ,key : index }).text(item.name));     
        });
        //console.log(data);
        $('#option-add').change(function(){
         var option = $('option:selected', this).attr('key');
         $("#qty").text("Sản phẩm còn :"+data.prd_attr_add[option].quantity);
        });
      }
  });
});
});
//click ok-add-cart
$(document).ready(function(){
$("#send-add-cart").click(function(){
    var loai = $("#option-add").val();
    var quantity = $("#quantity-add").val();
    // alert(loai);
    $.ajax({
      url:'{{route('addcart')}}',
      method:"get",
      data:{"loai":loai,"quantity":quantity},
        success:function(data){    
          var count = {{count(Cart::content())}};    
          $("#count").text(count);
        }
    });
  });
});
//quick view add-cart
$(document).ready(function(){
$("#addcart-m").click(function(e){
    e.preventDefault();
    var loai = $("#option").val();
    var quantity = 1;
    // alert(loai);
    $.ajax({
      url:'{{route('addcart')}}',
      method:"get",
      data:{"loai":loai,"quantity":quantity},
        success:function(data){    
          var count = {{count(Cart::content())}};    
          $("#count").text(count);
        }
    });
  });
  
});
</script>
@endsection