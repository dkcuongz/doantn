@extends('backend.master.master')
@section('content')
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <!--/.row-->
    <div class="row">
        <div class="col-xs-6 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Thêm thuộc tính sản phẩm</div>
                <div class="panel-body">
                    @if (session('thongbao'))
                    <div class="alert alert-danger" role="alert">
                    <strong>{{session('thongbao')}}</strong>
                        </div>
                    @endif
                    <form method="post" enctype="multipart/form-data">
                        @csrf
                    <div class="row" style="margin-bottom:40px">
						<div class="col-md-12">
                                    <div class="form-group">
                                        <label>Size</label>
									<input type="text" name="size" value="{{$attr->size}}" class="form-control">
                                        {{showErrors($errors,'size')}}
                                    </div>
                                    <div class="form-group">
                                        <label>Màu</label>
                                        <input type="text" name="color" value="{{$attr->color}}" class="form-control">
                                        {{showErrors($errors,'color')}}
                                    </div>
                                    <div class="form-group">
                                        <label>Desciption(Tên màu bằng tiếng anh)</label>
                                        <input type="text" name="description" value="{{$attr->description}}" class="form-control">
                                        {{showErrors($errors,'description')}}
                                    </div>                  
						</div>          
                    </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success" name="add-product" type="submit">Sửa</button>
                               
                                <a class="btn btn-warning"
                            href="/admin/attribute" role="button">Huỷ bỏ</a></div>
                                
                            </div>
                        </div>
                    <div class="clearfix"></div>
                </form>
                </div>
            </div>

        </div>
    </div>

    <!--/.row-->
</div>
<!--/.main-->
@endsection