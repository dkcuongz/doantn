@extends('backend.master.master')
@section('title','Nhà sản xuất')
@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/admin"><span><i class="fas fa-home"></i></span> Tổng quan</a></li>
            <li class="active">Nhà sản xuất</li>
        </ol>
    </div>
    <!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Quản lý nhà sản xuất</h1>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form method="post">
                        @csrf
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="">Thêm nhà sản xuất</label>
                                <input type="text" class="form-control" name="name" id="" placeholder="Tên nhà sản xuất mới">
                                {{showErrors($errors,'name')}}
                            </div>
                            <button type="submit" class="btn btn-primary">Thêm nhà sản xuất</button>
                        </div>
                        <div class="col-md-7">
                            @if (session('thongbao'))
                            <div class="alert alert-success" role="alert">
                                    <strong>{{session('thongbao')}}</strong>
                            </div>
                            @endif

                            <h3 style="margin: 0;"><strong>Danh sách nhà sản xuất</strong></h3>
                            <div class="vertical-menu">
                                <div class="item-menu active">Nhà sản xuất </div>

                               @foreach ($auth as $value)
                                    <div class="item-menu"><span>{{$value['name']}}</span>
                                        <div class="category-fix">
                                        <a class="btn-category btn-primary" href="/admin/auth/edit/{{$value['id']}}"><i class="fa fa-edit"></i></a>
                                        <a onclick="return del($value['name'])" class="btn-category btn-danger" href="/admin/auth/del/{{$value['id']}}"><i class="fas fa-times"></i></i></a>
                                        </div>
                                        </div>
                              @endforeach
    
                        </div>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!--/.col-->


    </div>
    <!--/.row-->
</div>
@endsection
@section('script')
    @parent
    <script>
        function del(name){
            return confirm('Bạn muốn xóa nhà sản xuất' + name + 'này không?');
        }
    </script>

    @endsection
