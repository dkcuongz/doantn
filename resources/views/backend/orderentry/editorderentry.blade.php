@extends('backend.master.master')
@section('content')
	<!--main-->
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="/admin"><span><i class="fas fa-home"></i></span> Tổng quan</a></li>
				<li class="active">Đơn hàng / Tạo hoá đơn nhập</li>
			</ol>
		</div>
		<!--/.row-->
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Tạo hoá đơn nhập</div>
                    <form method="post" enctype="multipart/form-data">
                        @csrf
                    @foreach ($order_dt as $item)                                             
                    <div class="panel-body">
                    <div class="col-md-8">
                        <div class="col-md-8">
                        <label>Sản phẩm</label>
                        <select  class="form-control"  name="product[{{$item->id}}]">
                            <option>Sản phẩm</option>  
                            @foreach($product as $value){
                                <option  @if ($item->entry_dt_prdatt->product_id == $value['id'])
                                    {{"selected"}}
                                  @endif  value="{{$value['id']}}">{{$value['code'].'-'.$value['name']}}</option>';
                         }
                         @endforeach
                        </select> 
                    <label>Biến thể</label>
                    <select class="form-control" name="attribute[{{$item->id}}]"> 
                        <option>Biến thể</option>  
                        @foreach($attr as $value){
                            <option @if ($item->entry_dt_prdatt->prd_attr->id == $value['id'])
                                {{"selected"}}
                              @endif value="{{$value['id']}}">{{$value['color'].'-'.$value['size']}}</option>';
                     }
                     @endforeach
                    </select>
                   </div>
                    <div class="col-md-4">
                        <br>
                        {{-- style="visibility: hidden" --}}
                        <a style="margin-top: 5px" href="/admin/product/add" class="btn btn-primary">Thêm sản phẩm</a>
                        <a style="margin-top: 25px" href="/admin/attribute/add" class="btn btn-primary">Thêm biến thể</a>
                    </div>
                    <div class="col-md-12">
                    <div class="form-group">
                    <label>Đơn giá</label> <input  class="form-control" name="price[{{$item->id}}]" value="{{$item->price}}" placeholder="Đơn giá" />
                    </div>
                    <div class="form-group">
                        <label>Số lượng</label> <input  class="form-control" name="quantity[{{$item->id}}]" value="{{$item->quantity}}" placeholder="Số lượng" />
                    </div>
                    </div>
                </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Ảnh sản phẩm</label>
                            <input id="img" type="file" name="img[{{$item->id}}]" class="form-control ">
                                {{showErrors($errors,'img')}}
                        </div>
                    </div>
                  
                </div>
                @endforeach
                    <input  class="btn btn-success" type="submit" value="Xác nhận" />
                  
                </form> 
                    </div>
                </div>
            </div>
			</div>
		</div>
		<!--/.row-->
	</div>
	<!--end main-->
@endsection