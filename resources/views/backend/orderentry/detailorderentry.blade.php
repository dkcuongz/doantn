@extends('backend.master.master')
@section('content')
<style>
	.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

.body-1 {
  position: relative;
  width: 21cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  color: #001028;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
  font-family: Arial;
}

..header-1 {
  padding: 10px 0;
  margin-bottom: 30px;
}

#logo {
  text-align: center;
  margin-bottom: 10px;
}

#logo img {
  width: 90px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  background: url(img/logo/dimension.png);
}

#project {
  float: left;
}

#project span {
  color: #5D6975;
  text-align: right;
  width: 52px;
  margin-right: 10px;
  display: inline-block;
  font-size: 0.8em;
}

#company {
  float: right;
  text-align: right;
}

#project div,
#company div {
  white-space: nowrap;        
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table tr:nth-child(2n-1) td {
  background: #F5F5F5;
}

table th,
table td {
  text-align: center;
}

table th {
  padding: 5px 20px;
  color: #5D6975;
  border-bottom: 1px solid #C1CED9;
  white-space: nowrap;        
  font-weight: normal;
}

table .service,
table .desc {
  text-align: left;
}

table td {
  padding: 20px;
  text-align: right;
}

table td.service,
table td.desc {
  vertical-align: top;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table td.grand {
  border-top: 1px solid #5D6975;;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

.footer-1 {
  color: #5D6975;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}
</style>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="/admin"><span><i class="fas fa-home"></i></span> Tổng quan</a></li>
			<li class="active">Đơn hàng / Chi tiết đơn hàng nhập</li>
		</ol>
	</div>
	<!--/.row-->
	<div class="row">
		<div class="col-xs-12 col-md-12 col-lg-12">

			<div class="panel panel-primary">
				<div class="panel-heading">Chi tiết đơn hàng nhập</div>
				<div class="panel-body">
    <header class="clearfix">
      <div id="logo">
        <img src="img/logo/logo.png">
      </div>
	<h1>INVOICE {{$order->id}}</h1>
      <div id="company" class="clearfix">
        <div>Hãng</div>
        <div>{{$order->entry_auth->name}}<br /> Địa chỉ</div>
        <div>Số điện thọa</div>
        <div><a href="mailto:company@example.com">company@example.com</a></div>
      </div>
      <div id="project">
        <div><span>Shop</span> DkShop</div>
        <div><span>Nhân viên</span>{{$order->entry_user->name}} </div>
        <div><span>Địa chỉ</span>332/11b-Hoàng Công Chất-Cầu Diễn-Từ Liêm-Hà Nội</div>
        <div><span>EMAIL</span> <a href="mailto:dkcuongz@gmail.com">dkcuongz@gmail.com</a></div>
        <div><span>Ngày nhập</span> {{$order->created_at}}</div>
      </div>
    </header>
    <main>
      <table>
        <thead>
          <tr>
            <th class="service">Sản phẩm</th>
            <th class="desc">Chi tiết</th>
            <th>Giá</th>
            <th>Số lượng</th>
            <th>Tổng</th>
          </tr>
        </thead>
        <tbody>
			@php
			$total = 0;
			@endphp
			@foreach ($order_entry as $item)	
			@php
			$total += $item->quantity*$item->price;
			@endphp
          <tr>
		  <td class="service">{{$item->entry_dt_prdatt->prd_attr_prd->name}}</td>
            <td class="desc">{{$item->entry_dt_prdatt->name}}</td>
            <td class="unit">{{number_format($item->price,0,"",".")}} VNĐ</td>
            <td class="qty">{{$item->quantity}}</td>
            <td class="total">{{number_format($item->quantity*$item->price,0,"",".")}} VNĐ</td>
		  </tr>	  
		  @endforeach
          <tr>
            <td colspan="4">Tổng tiền</td>
            <td class="total">@php echo number_format($total,0,"","."); @endphp VNĐ</td>
          </tr>
          {{-- <tr>
            <td colspan="4">TAX 25%</td>
            <td class="total">$1,300.00</td>
          </tr>
          <tr>
            <td colspan="4" class="grand total">GRAND TOTAL</td>
            <td class="grand total">$6,500.00</td>
          </tr> --}}
        </tbody>
      </table>
      <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">Hóa đơn có hiệu lực từ ngày tạo.</div>
      </div>
    </main>
    <footer>
     Hóa đơn được tạo bởi máy tính, không bao gồm chữ ký và xác nhận.
    </footer>
</div>
</div>
</div>
</div>
<!--/.row-->
</div>
  @endsection