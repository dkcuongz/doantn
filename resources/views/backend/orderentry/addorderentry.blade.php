@extends('backend.master.master')
@section('content')
	<!--main-->
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="/admin"><span><i class="fas fa-home"></i></span> Tổng quan</a></li>
				<li class="active">Đơn hàng / Tạo hoá đơn nhập</li>
			</ol>
		</div>
		<!--/.row-->
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Tạo hoá đơn nhập</div>
                    <div class="panel-body">
                <div id="readroot" style="display: none">
                    <input class="btn btn-danger" type="button" value="Xoá"
                    onclick="this.parentNode.parentNode.removeChild(this.parentNode);" /><br /><br />
                    <div class="col-md-8">
                        <div class="col-md-8">
                        <label>Sản phẩm</label>
                        <select  class="form-control"  name="product[]">
                            <option>Sản phẩm</option>  
                            @foreach($product as $value){
                                <option  value="{{$value['id']}}">{{$value['code'].'-'.$value['name']}}</option>';
                         }
                         @endforeach
                        </select> 
                    <label>Biến thể</label>
                    <select class="form-control" name="attribute[]"> 
                        <option>Biến thể</option>  
                        @foreach($attr as $value){
                            <option  value="{{$value['id']}}">{{$value['color'].'-'.$value['size']}}</option>';
                     }
                     @endforeach
                    </select>
                   </div>
                    <div class="col-md-4">
                        <br>
                        {{-- style="visibility: hidden" --}}
                        <a style="margin-top: 5px" href="/admin/product/add" class="btn btn-primary">Thêm sản phẩm</a>
                        <a style="margin-top: 25px" href="/admin/attribute/add" class="btn btn-primary">Thêm biến thể</a>
                    </div>
                    <div class="col-md-12">
                    <div class="form-group">
                        <label>Đơn giá</label> <input  class="form-control" name="price[]" value="" placeholder="Đơn giá" />
                    </div>
                    <div class="form-group">
                        <label>Số lượng</label> <input  class="form-control" name="quantity[]" value="" placeholder="Số lượng" />
                    </div>
                    </div>
                </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Ảnh sản phẩm</label>
                            <input id="img" type="file" name="img" class="form-control" >
                                {{showErrors($errors,'img')}}
                            <img id="avatar" class="thumbnail" width="100%" height="350px" src="img/import-img.png">
                        </div>
                    </div>
                </div>
                
                <form method="post" enctype="multipart/form-data">
                    <label>Nhà sản xuất</label>
                    <select id="Ultra" onchange="run()" class="form-control" name="auth"  style="margin-bottom: 30px"> 
                        <option>Nhà sản xuất</option>                      
                        @foreach($auth as $value){
                            <option  value="{{$value['id']}}">{{$value['name']}}</option>';
                     }
                     @endforeach
                    </select>
                    @csrf
                    <span id="writeroot"></span>
                    <input  class="btn btn-success" type="button" value="Thêm sản phẩm" onclick="moreFields()"/>
                    <input  class="btn btn-success" type="submit" value="Xác nhận" />
                
                </form> 
                    </div>
                </div>
            </div>
			</div>
		</div>
		<!--/.row-->
	</div>
	<!--end main-->
@endsection
@section('script')
    @parent
    <script type="text/javascript">
        var counter = 0;
        function moreFields() {
            counter++;
            var newFields = document.getElementById('readroot').cloneNode(true);
            newFields.id = '';
            newFields.style.display = 'block';
            var newField = newFields.childNodes;
            for (var i=0;i<newField.length;i++) {
                var theName = newField[i].name
                if (theName)
                    newField[i].name = theName + counter;
            }
            var insertHere = document.getElementById('writeroot');
                                                    insertHere.parentNode.insertBefore(newFields,insertHere);
        }

        window.onload = moreFields;

    function changeImg(input){
           //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
           if(input.files && input.files[0]){
               var reader = new FileReader();
               //Sự kiện file đã được load vào website
               reader.onload = function(e){
                   //Thay đổi đường dẫn ảnh
                   $('#avatar').attr('src',e.target.result);
               }
               reader.readAsDataURL(input.files[0]);
           }
       }
       $(document).ready(function() {
           $('#avatar').click(function(){
               $('#img').click();
           });
       });
    </script>
@endsection
