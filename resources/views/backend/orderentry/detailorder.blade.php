@extends('backend.master.master')
@section('content')
	<!--main-->
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="/admin"><span><i class="fas fa-home"></i></span> Tổng quan</a></li>
				<li class="active">Đơn hàng / Chi tiết đơn hàng</li>
			</ol>
		</div>
		<!--/.row-->
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">

				<div class="panel panel-primary">
					<div class="panel-heading">Chi tiết đơn hàng</div>
					<div class="panel-body">
						<div class="bootstrap-table">
							<div class="table-responsive">
								<div class="form-group">
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="panel panel-blue">
											<div class="panel-heading dark-overlay">Thông đơn hàng</div>
											<div class="panel-body">
											<strong><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Nhân viên lập : {{$order->entry_user->name}}</strong> <br>
												<strong><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>  Nhà sản xuất : {{$order->entry_auth->name}}</strong><br>
												<strong><span class="glyphicon glyphicon-home" aria-hidden="true"></span>  Ngày lập : {{$order->created_at}}</strong>
												<br>	
											</div>
										</div>
									</div>
								</div>
								<table class="table table-bordered" style="margin-top:20px;">
									<thead>
										<tr class="bg-primary">
											<th>ID</th>
											<th>Thông tin Sản phẩm</th>
											<th>Số lượng</th>
											<th>Giá sản phẩm</th>
											<th>Thành tiền</th>
											<th>Tùy chọn</th>
										</tr>
									</thead>
									<tbody>
										@php
											$total = 0;
										@endphp
										@foreach ($order_entry as $item)
										@php
											$total += $item->quantity*$item->price;
										@endphp
										<tr>
											<td>1</td>
											<td>
												<div class="row">
													<div class="col-md-4">
													<img width="100px" src="{{$item->img}}" class="thumbnail">
													</div>
													<div class="col-md-8">
														<p>Mã sản phẩm:{{$item->entry_dt_prdatt->prd_attr_prd->code}}</p>
														<p>Tên Sản phẩm: <strong>{{$item->entry_dt_prdatt->prd_attr_prd->name}}</strong></p>
														<div class="group-color">Color:	<span class="product-color" style="background-color: {{$item->entry_dt_prdatt->prd_attr->description}}; transform:translate(10px,-5px)"></span>
														</div>
														<p>Size: {{$item->entry_dt_prdatt->prd_attr->size}}</p>
													</div>
												</div>
											</td>
										    <td>{{$item->quantity}}</td>
											<td>{{number_format($item->price,0,"",".")}} VNĐ</td>
											<td>{{number_format($item->quantity*$item->price,0,"",".")}} VNĐ</td>
											<td><a href="/admin/order_et/{{$order->id}}/edit/{{$item->id}}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a></td>
										</tr>
										@endforeach
									</tbody>

								</table>
								<table class="table">
									<thead>
										<tr>
											<th width='70%'>
												<h4 align='right'>Tổng Tiền :</h4>
											</th>
											<th>
												<h4 align='right' style="color:blue;">@php echo number_format($total,0,"","."); @endphp VNĐ</h4>
											</th>

										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
								<div class="alert alert-primary" role="alert" align='right'>
									<a name="" id="" class="btn btn-success" href="/admin/order_et" role="button">Trở lại</a>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<!--/.row-->


	</div>
	<!--end main-->
@endsection