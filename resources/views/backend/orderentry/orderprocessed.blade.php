@extends('backend.master.master')
@section('content')
	<!--main-->
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="/admin"><span><i class="fas fa-home"></i></span> Tổng quan</a></li>
				<li class="active">Đơn hàng</li>
			</ol>
		</div>
		<!--/.row-->
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">

				<div class="panel panel-primary">
					<div class="panel-heading">Danh sách đơn nhập hàng </div>
					<div class="panel-body">
						<div class="bootstrap-table">
							<div class="table-responsive">
								@if (session('thongbao'))
								<div class="alert bg-success" role="alert">
										<svg class="glyph stroked checkmark">
											<use xlink:href="#stroked-checkmark"></use>
										</svg>{{session('thongbao')}}
									</div>
								@endif
                                <a href="/admin/order_et/add" class="btn btn-primary">Tạo hoá đơn nhập</a>
								<table class="table table-bordered" style="margin-top:20px;">				
                                    <thead>
                                        <tr class="bg-primary">
                                            <th>ID</th>
                                            <th>Nhân viên</th>
                                            <th>Nhà sản xuất</th>
                                            <th>Tổng tiền</th>
                                            <th>Thời gian</th>
                                            <th width="20%">Tuỳ chọn</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($order_entry as $item)
                                        <tr>
                                            <td>{{$item->id}}</td>
                                            <td>{{$item->entry_user->name}}</td>
                                            <td>{{$item->entry_auth->name}}</td>
                                            <td>{{number_format($item->total,0,"",".")}}</td>
                                            <td>{{$item->created_at}}</td>
                                            <td>
											<a href="/admin/order_et/detail/{{$item->id}}" class="btn btn-success"><i class="fa fa-pencil" aria-hidden="true"></i> Chi tiết</a>
											<a href="/admin/order_et/{{$item->id}}/edit" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
											<a onclick="return del()" href="/admin/order_et/del/{{$item->id}}" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a></td>
											</tr>
                                        @endforeach 
                                    </tbody>
                                </table>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<!--/.row-->


	</div>
	<!--end main-->
@endsection
@section('script')
@parent
<script>
	function del(name){
		return confirm('Bạn muốn xóa hóa đơn này không?');
	}
</script>

@endsection