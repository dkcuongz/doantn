@extends('backend.master.master')
@section('title','Sửa Thành Viên')
@section('content')
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/admin"><span><i class="fas fa-home"></i></span> Tổng quan</a></li>
            <li class="active">Sửa thành viên</li>
        </ol>
    </div>
    <!--/.row-->

    <!--/.row-->
    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="fas fa-user"></i> Sửa khách hàng - {{$customers->email}}</div>
                <div class="panel-body">
                    <form method="post">
                        @csrf
                    <div class="row justify-content-center" style="margin-bottom:40px">

                        <div class="col-md-8 col-lg-8 col-lg-offset-2">

                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="email" class="form-control" value="{{$customers->email}}">
                                {{showErrors($errors,'email')}}
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="text" name="password" class="form-control" value="{{$customers->password}}">
                                {{showErrors($errors,'password')}}
                            </div>
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" name="name" class="form-control" value="{{$customers->name}}">
                                {{showErrors($errors,'name')}}
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <input type="address" name="address" class="form-control" value="{{$customers->address}}">
                                {{showErrors($errors,'address')}}
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="phone" name="phone" class="form-control" value="{{$customers->phone}}">
                                {{showErrors($errors,'phone')}}
                            </div>

                            <div class="form-group">
                                <label>Số đơn hàng đã mua</label>
                                <input type="quantity" name="quantity" class="form-control" value="{{$customers->quantity}}">
                                {{showErrors($errors,'quantity')}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right">

                                <button class="btn btn-success" type="submit">Sửa thành viên</button>
                                <a class="btn btn-danger"
                                href="/admin/user" role="button">Bỏ qua</a>
                            </div>
                        </div>


                    </div>

                    <div class="clearfix"></div>
                </form>
                </div>
            </div>

        </div>
    </div>

    <!--/.row-->
</div>
@endsection
