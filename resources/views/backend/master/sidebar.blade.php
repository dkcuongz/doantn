<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <form role="search">
    </form>
    <ul class="nav menu">
        <li><a href="/admin"><span><i class="fas fa-home"></i></span> Tổng quan</a></li>
        <li><a href="/admin/category"><span><i class="fas fa-clipboard-list"></i></span> Danh Mục</a></li>
        <li><a href="/admin/product"><span><i class="fab fa-phoenix-squadron"></i></span> Sản phẩm</a></li>
        <li><a href="/admin/attribute"><span><i class="fas fa-adjust"></i></span> Thuộc tính</a></li>
        <li><a href="/admin/order_et"><span><i class="fab fa-first-order-alt"></i></span> Đơn hàng nhập</a></li>
        <li><a href="/admin/order"><span><i class="fab fa-accusoft"></i></span> Đơn hàng bán</a></li>
        <li><a href="/admin/auth"><span><i class="fas fa-house-damage"></i></span> Nhà sản xuất</a></li>
        <li><a href="/admin/user"><span><i class="fas fa-users"></i></span> Khách hàng</a></li>
        <li><a href="/admin/coupon"><span><i class="fas fa-crown"></i></i></span> Coupon</a></li>
        <li><a href="/admin/subcribe"><span><i class="fas fa-envelope-open-text"></i></span> Thông báo người dùng</a></li>
        <li class="dropdown">
            <a  class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-flag"></i> Báo cáo <span class="caret"></span></a>
            <ul class="dropdown-menu " role="menu"><li><a href="/admin/report"> Báo cáo doanh thu</a></li>
            {{-- <li><a href="/admin/report/product"> Báo cáo sản phẩm bán chạy</a></li> --}}
            </ul>
        </li>
        <li role="presentation" class="divider"></li>
        @if (Auth::user()->level==1)
        <li><a href="/admin/adminuser"><span><i class="fas fa-chalkboard-teacher"></i></span> Quản lý thành viên</a></li>
        @endif
    </ul>

</div>
