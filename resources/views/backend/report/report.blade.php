@extends('backend.master.master')
@section('title','Báo cáo')
@section('content')
<style>
* {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
*:before, *:after {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}

/* body {
  font-family: "Helvetica Neue", "Helvetica", "Roboto", "Arial", sans-serif;
  color: #5e5d52;
} */

/* a {
  color: #337aa8;
}
a:hover, a:focus {
  color: #4b8ab2;
} */

/* .container {
  margin: 5% 3%;
} */
@media (min-width: 48em) {
  .container {
    margin: 2%;
  }
}
@media (min-width: 75em) {
  .container {
    margin: 2em auto;
    max-width: 75em;
  }
}

.responsive-table {
  width: 100%;
  margin-bottom: 1.5em;
  border-spacing: 0;
}
@media (min-width: 48em) {
  .responsive-table {
    font-size: 0.9em;
  }
}
@media (min-width: 62em) {
  .responsive-table {
    font-size: 1em;
  }
}
.responsive-table thead {
  position: absolute;
  clip: rect(1px 1px 1px 1px);
  /* IE6, IE7 */
  padding: 0;
  border: 0;
  height: 1px;
  width: 1px;
  overflow: hidden;
}
@media (min-width: 48em) {
  .responsive-table thead {
    position: relative;
    clip: auto;
    height: auto;
    width: auto;
    overflow: auto;
  }
}
.responsive-table thead th {
  background-color: #1d96b2;
  border: 1px solid #1d96b2;
  font-weight: normal;
  text-align: center;
  color: white;
}
.responsive-table thead th:first-of-type {
  text-align: left;
}
.responsive-table tbody,
.responsive-table tr,
.responsive-table th,
.responsive-table td {
  display: block;
  padding: 0;
  text-align: left;
  white-space: normal;
}
@media (min-width: 48em) {
  .responsive-table tr {
    display: table-row;
  }
}
.responsive-table th,
.responsive-table td {
  padding: 0.5em;
  vertical-align: middle;
}
@media (min-width: 30em) {
  .responsive-table th,
  .responsive-table td {
    padding: 0.75em 0.5em;
  }
}
@media (min-width: 48em) {
  .responsive-table th,
  .responsive-table td {
    display: table-cell;
    padding: 0.5em;
  }
}
@media (min-width: 62em) {
  .responsive-table th,
  .responsive-table td {
    padding: 0.75em 0.5em;
  }
}
@media (min-width: 75em) {
  .responsive-table th,
  .responsive-table td {
    padding: 0.75em;
  }
}
.responsive-table caption {
  margin-bottom: 1em;
  font-size: 1em;
  font-weight: bold;
  text-align: center;
}
@media (min-width: 48em) {
  .responsive-table caption {
    font-size: 1.5em;
  }
}
.responsive-table tfoot {
  font-size: 0.8em;
  font-style: italic;
}
@media (min-width: 62em) {
  .responsive-table tfoot {
    font-size: 0.9em;
  }
}
@media (min-width: 48em) {
  .responsive-table tbody {
    display: table-row-group;
  }
}
.responsive-table tbody tr {
  margin-bottom: 1em;
}
@media (min-width: 48em) {
  .responsive-table tbody tr {
    display: table-row;
    border-width: 1px;
  }
}
.responsive-table tbody tr:last-of-type {
  margin-bottom: 0;
}
@media (min-width: 48em) {
  .responsive-table tbody tr:nth-of-type(even) {
    background-color: rgba(94, 93, 82, 0.1);
  }
}
.responsive-table tbody th[scope="row"] {
  background-color: #1d96b2;
  color: white;
}
@media (min-width: 30em) {
  .responsive-table tbody th[scope="row"] {
    border-left: 1px solid #1d96b2;
    border-bottom: 1px solid #1d96b2;
  }
}
@media (min-width: 48em) {
  .responsive-table tbody th[scope="row"] {
    background-color: transparent;
    color: #5e5d52;
    text-align: left;
  }
}
.responsive-table tbody td {
  text-align: right;
}
@media (min-width: 48em) {
  .responsive-table tbody td {
    border-left: 1px solid #1d96b2;
    border-bottom: 1px solid #1d96b2;
    text-align: center;
  }
}
@media (min-width: 48em) {
  .responsive-table tbody td:last-of-type {
    border-right: 1px solid #1d96b2;
  }
}
.responsive-table tbody td[data-type="currency"] {
  text-align: right;
}
.responsive-table tbody td[data-title]:before {
  content: attr(data-title);
  float: left;
  font-size: 0.8em;
  color: rgba(94, 93, 82, 0.75);
}
@media (min-width: 30em) {
  .responsive-table tbody td[data-title]:before {
    font-size: 0.9em;
  }
}
@media (min-width: 48em) {
  .responsive-table tbody td[data-title]:before {
    content: none;
  }
}

</style>
<div class=" col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Báo cáo doanh thu</h1>
    <form action="{{route('postReport')}}" method="POST">
      @csrf
    <Label>Ngày</Label>
    <input style="margin-right:50px " type="number" name="day" max="31" >
    <Label>Tháng</Label>
    <input type="number" max="12" name="month" style="margin-right:50px " required >
    <button type="submit" class="btn btn-success">Lọc</button>
  </form>
  </div>
  </div>
    <table class="responsive-table">
    <caption>Báo cáo doanh thu trong tháng {{$month}}</caption>
      <thead>
        <tr>
          <th scope="col">Mã đơn hàng</th>         
          <th scope="col">Khách hàng</th>
          <th scope="col">Tổng tiền </th>
          <th scope="col">Giảm giá</th>
          <th scope="col">Thành tiền</th>
          {{-- <th scope="col">Foreign Gross</th>
          <th scope="col">Budget</th> --}}
        </tr>
      </thead>
      <tfoot>
        <tr>
          <td colspan="4">Nguồn: <a href="" rel="external">DkShop</a>.Ngày tạo @php echo date("d-m-Y H:i:s"); @endphp.</td>
        <td> 
          @if (isset($day))
          <a href="/admin/report/pdf/{{$day}}/{{$month}}/{{$year}}" class="btn btn-success">
            @else
          <a href="/admin/report/pdf/{{$month}}/{{$year}}" class="btn btn-success">
            @endif
            <i class="fa fa-pencil" aria-hidden="true"></i>In báo cáo</a></td>
        </tr>
      </tfoot>
      <tbody>
        @foreach ($order as $item)
        <tr>
        <th scope="row">{{$item->id}}</th>
          <td data-title="Released">{{$item->name}}</td>
          <td data-title="Studio">{{number_format($item->total + $item->discount,0,",",".")}} VNĐ</td>
          <td data-title="Worldwide Gross" data-type="currency">{{number_format($item->discount,0,",",".")}} VNĐ</td>
          <td data-title="Domestic Gross" data-type="currency">{{number_format($item->total,0,",",".")}} VNĐ</td>
        </tr>
        @endforeach
        <tr>
          <td colspan="4" >Tổng tiền bán :</td>
        <td>{{number_format($total_order,0,",",".")}} VNĐ</td>
        </tr>
      </tbody>
      <thead>
        <tr>
          <th scope="col" colspan="1">Mã đơn hàng</th>         
          <th scope="col"  colspan="1">Nhà cung cấp</th>
          <th scope="col"  colspan="2">Nhân viên</th>
          <th scope="col"  colspan="1">Tổng tiền </th>
          {{-- <th scope="col">Foreign Gross</th>
          <th scope="col">Budget</th> --}}
        </tr>
      </thead>
      <tbody>
        @foreach ($order_entry as $item)
        <tr>
        <th scope="row">{{$item->id}}</th>
          <td data-title="Released">{{$item->entry_auth->name}}</td>
          <td data-title="Studio"  colspan="2">{{$item->entry_user->name}}</td>
          <td data-title="Worldwide Gross" data-type="currency">{{number_format($item->total,0,",",".")}} VNĐ</td>
        @endforeach
        <tr>
          <td colspan="4" >Tổng tiền nhập :</td>
        <td>{{number_format($total_order_entry,0,",",".")}} VNĐ</td>
        </tr>
      </tbody>
    </table>
  </div>
  @endsection