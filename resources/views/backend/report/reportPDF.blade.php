<html lang="{{ app()->getLocale() }}">
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
        <link href="{{public_path('backdend/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> 
<title>DkShop</title>
<style>
@font-face {
            font-family: 'OpenSans';
            src: url("{{storage_path().'/fonts/OpenSans.ttf'}}") format('truetype');
        }
        @font-face {
            font-family: 'OpenSansItalic';
            src: url("{{storage_path().'/fonts/OpenSans-Italic.ttf'}}") format('truetype');
        }
        @font-face {
            font-family: 'OpenSansBold';
            src: url("{{storage_path().'/fonts/OpenSans-Bold.ttf'}}") format('truetype');
        }
        @font-face {
            font-family: 'OpenSansBoldItalic';
            src: url("{{storage_path().'/fonts/OpenSans-BoldItalic.ttf'}}") format('truetype');
        }
        body {
            font-family: Open Sans, sans-serif;
        }
html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Open Sans', sans-serif ;
                font-weight: 400;
                height: 100vh;
                margin: 0;
            }
table {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    margin-bottom: 20px;
    height: 300px;
}

table td, table th {
    padding: 15px;
    background: #eee;
    border-bottom: 1px solid #fff
}

 table th {
   text-align: left;
    white-space: nowrap;
    font-weight: 400;
    font-size: 16px
}

table td h3 {
    margin: 10px;
    font-weight: 300;
    color: #3989c6;
    font-size: 1.2em
}

table .qty, table .total, table .unit {
    text-align: right;
    font-size: 1.2em
}

 table .no {
    color: #fff;
    font-size: 1.6em;
    background: #3989c6
}

 table .unit {
    background: #ddd
}

 table .total {
    background: #3989c6;
    color: #fff
}

table tbody tr:last-child td {
    border: none
}

table tfoot td {
    background: 0 0;
    border-bottom: none;
    white-space: nowrap;
    text-align: center;
    padding: 10px 20px;
    font-size: 1.2em;
    border-top: 1px solid #aaa
}

table tfoot tr:first-child td {
    border-top: none
}

 table tfoot tr:last-child td {
    color: #3989c6;
    font-size: 1.4em;
    border-top: 1px solid #3989c6
}

 table tfoot tr td:first-child {
    border: none
}

 footer {
    width: 100%;
    text-align: center;
    color: #777;
    border-top: 1px solid #aaa;
    padding: 8px 0
}
 .company-details {
    margin-left: 30px;
    
    text-align: left
}

 .company-details .name {
    margin-top: 0;
    margin-bottom: 0
}

 .contacts {
    margin-bottom: 20px
}

</style>
</head>
<body>
<div class=" container">
  <div class="col company-details">
    <h2 class="name">
        <a target="_blank" href="">
       DkShop
        </a>
    </h2>
    <div>401 Nguyễn Khang Yên Hòa Cầu Giấy</div>
    <div>0357093377</div>
    <div>dkcuongz@gmail.com</div>
</div>
    <table class="responsive-table">
    <caption style="font-size:40px; text-align: center;margin-bottom: 50px;margin-top: 50px">Báo cáo doanh thu trong tháng {{$month}}</caption>
    
    <thead>
        <tr>
          <th scope="col">Mã đơn hàng</th>         
          <th scope="col">Khách hàng</th>
          <th scope="col">Tổng tiền </th>
          <th scope="col">Giảm giá</th>
          <th scope="col">Thành tiền</th>
          {{-- <th scope="col">Foreign Gross</th>
          <th scope="col">Budget</th> --}}
        </tr>
      </thead>
     
      <tbody>
        @foreach ($order as $item)
        <tr>
          <td scope="row">{{$item->id}}</td>
          <td >{{$item->name}}</td>
          <td >{{number_format($item->total + $item->discount,0,",",".")}} VNĐ</td>
          <td >{{number_format($item->discount,0,",",".")}} VNĐ</td>
          <td >{{number_format($item->total,0,",",".")}} VNĐ</td>
        </tr>
        @endforeach
        <tr>
          <td colspan="4" >Tổng tiền bán :</td>
        <td>{{number_format($total_order,0,",",".")}} VNĐ</td>
        </tr>
      </tbody>
      <thead>
        <tr>
          <th scope="col" colspan="1">Mã đơn hàng</th>         
          <th scope="col"  colspan="1">Nhà cung cấp</th>
          <th scope="col"  colspan="2">Nhân viên</th>
          <th scope="col"  colspan="1">Tổng tiền </th>
          {{-- <th scope="col">Foreign Gross</th>
          <th scope="col">Budget</th> --}}
        </tr>
      </thead>
      <tbody>
        @foreach ($order_entry as $item)
        <tr>
          <td scope="row">{{$item->id}}</td>
          <td data-title="Released">{{$item->entry_auth->name}}</td>
          <td data-title="Studio"  colspan="2">{{$item->entry_user->name}}</td>
          <td data-title="Worldwide Gross" data-type="currency">{{number_format($item->total,0,",",".")}} VNĐ</td>
        @endforeach
        <tr>
          <td colspan="4" >Tổng tiền nhập :</td>
        <td>{{number_format($total_order_entry,0,",",".")}} VNĐ</td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="5">Nguồn: <a href="" rel="external">DkShop</a>.Ngày tạo @php echo date("d-m-Y H:i:s"); @endphp.</td>
        </tr>
      </tfoot>
    </table>
  </div>
</body>
</html>