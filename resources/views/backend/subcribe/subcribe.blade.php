@extends('backend.master.master')
@section('title','Thông báo người dùng')
@section('content')
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/admin"><span><i class="fas fa-home"></i></span> Tổng quan</a></li>
            <li class="active">Thông báo người đăng kí</li>
        </ol>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="fas fa-user"></i> Gửi thông báo cho người đăng kí</div>
                <div class="panel-body">
                <form method="post" action="{{route('sendNotiMail')}}">
                        @csrf
                    <div class="row justify-content-center" style="margin-bottom:40px">
                        <div class="col-md-12 col-lg-12">
                            @if (session('thongbao'))
                            <div class="alert alert-success" role="alert">
                                    <strong>{{session('thongbao')}}</strong>
                            </div>
                            @endif
                            <textarea name="data" id="area1" style="width:100%;height:400px;">
                                Nhập nội dung
                            </textarea>
                        </div>
                        <div class="row">
                            <div style="margin-top: 30px" class="col-md-4 col-lg-4 col-lg-offset-1 text-right">
                                <input class="form-control" type="text" name="url" placeholder="Đường dẫn">
                            </div>
                            <div style="margin-top: 30px" class="col-md-4 col-lg-4 col-lg-offset-1 text-right">
                                <button class="btn btn-success" type="submit">Gửi</button>
                                <a class="btn btn-danger"
                                href="/admin/coupon" role="button">Bỏ qua</a>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
                </div>
            </div>

        </div>
    </div>
    <!--/.row-->
</div>
@endsection
@section('script')
@parent
<script>
</script>
@endsection