@extends('backend.master.master')
@section('title','Chi tiết biến thể sản phẩm')
@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/admin"><span><i class="fas fa-home"></i></span> Tổng quan</a></li>
            <li class="active">Danh sách biến thể sản phẩm</li>
        </ol>
    </div>
    <!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> Danh sách biến thể</h1>
        </div>
    </div>
    <!--/.row-->
    <div class="col-md-12">
        @if (session('thongbao'))
								<div class="alert bg-success" role="alert">
										<svg class="glyph stroked checkmark">
											<use xlink:href="#stroked-checkmark"></use>
										</svg>{{session('thongbao')}}
									</div>
                                @endif
                   
					<a href="/admin/attribute/add" class="btn btn-primary">Thêm thuộc tính</a>
        <div class="panel panel-default">
            <form method="post">
                <div class="panel-heading" align='center'>
                   Thông tin biến thể sản phẩm : 
                    {{$product->name}} ({{$product->code}})
                  
                </div>
                <div class="panel-body" align='center'>
                    <table class="table table-bordered" style="margin-top:20px;">

                        <thead>
                            <tr class="bg-primary">
                                <th>ID</th>
                                <th>Biến thể</th>
                                <th>Tên</th>
                                <th width = '25%'>Ảnh sản phẩm</th>
                                <th >Số lượng sản phẩm</th>
                                <th width='20%'>Tùy chọn</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($prd_attr as $row)
                            <tr> 
                                <td>{{$row->id}}</td>
                                <td>{{'Size: '.$row->prd_attr->size.'--------Màu: '.$row->prd_attr->color}}</td>
                                <td>{{$row->name}}</td>
                                <td><img src="img/{{$row->img}}" alt="" style="transform: translate(15px,0px);"  height="250px"></td>
                                <td>{{$row->quantity}}</td>
                                <td>
                                <a href="/admin/prd_attribute/{{$product->id}}/edit/{{$row->id}}" class="btn btn-warning"><i class="fa fa-pencil"
                                            aria-hidden="true"></i> Sửa</a>
                                <a onclick="return del('{{$row->name}}')" href="/admin/prd_attribute/{{$product->id}}/del/{{$row->id}}" class="btn btn-danger"><i class="fa fa-trash"
                                            aria-hidden="true"></i> Xóa</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div align='center'>
                        <ul class="pagination">
                            <ul class="pagination">
                                {{$prd_attr->links()}}
                            </ul>
                        </ul>
                    </div>
                    <div align='right' style="margin-right:50px"> <a class="btn btn-primary"
                        href="/admin/product" role="button">Bỏ qua</a></div>
                </div>
                <div class="clearfix"></div>  
            </form>
        </div>
    </div>
</div>
<!--/.main-->
@endsection
@section('script')
@parent
<script>
    function del(name){
        return confirm('Bạn muốn xóa sản phẩm thuộc tính' + name + 'này không?');
    }
</script>

@endsection
