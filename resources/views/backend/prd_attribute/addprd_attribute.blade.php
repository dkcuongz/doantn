@extends('backend.master.master')
@section('title','Thêm Sản Phẩm')
@section('content')
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/admin"><span><i class="fas fa-home"></i></span> Tổng quan</a></li>
            <li class="active">Thêm biến thể sản phẩm</li>
        </ol>
    </div>
    <!--/.row-->

    <div class="row">
        <div class="col-xs-6 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Thêm biến thể sản phẩm</div>
                <div class="panel-body">
                    <form method="post" enctype="multipart/form-data">
                        @csrf
                    <div class="row" style="margin-bottom:40px">

                                <div class="col-md-8">
                                   
                                    <div class="form-group">
                                    <label>Sản phẩm: {{$product->name.' ('.$product->code.')'}}</label>
                                    <img src="img/{{$product->img}}" alt=""  >
                                    </div>
                                  
                                    <div class="form-group">
                                        <label>Size</label>
                                        <input type="text" name="size" class="form-control">
                                        {{showErrors($errors,'size')}}
                                    </div>
                                    <div class="form-group">
                                        <label>Màu</label>
                                        <input type="text" name="color" class="form-control">
                                        {{showErrors($errors,'color')}}
                                    </div>
                                    <div class="form-group">
                                        <label>Color (Tên màu bằng tiếng anh)</label>
                                        <input type="text" name="description" class="form-control">
                                        {{showErrors($errors,'description')}}
                                    </div>
                                    <div class="form-group">
                                        <label>Số lượng sản phẩm</label>
                                        <input type="number" name="quantity" class="form-control">
                                        {{showErrors($errors,'quantity')}}
                                    </div>
                                   
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Ảnh biến thể sản phẩm</label>
                                        <input id="img" type="file" name="img" class="form-control hidden"
                                            onchange="changeImg(this)">
                                            {{showErrors($errors,'img')}}
                                        <img id="avatar" class="thumbnail" width="100%" height="350px" src="img/import-img.png">
                                    </div>
                                </div>
                    </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success" name="add-product" type="submit">Thêm sản phẩm</button>
                               
                                <a class="btn btn-warning"
                            href="/admin/prd_attribute/{{$product->id}}" role="button">Huỷ bỏ</a></div>
                                
                            </div>
                        </div>
                    <div class="clearfix"></div>
                </form>
                </div>
            </div>

        </div>
    </div>

    <!--/.row-->
</div>
@endsection

@section('script')
    @parent
    <script>
        function changeImg(input){
               //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
               if(input.files && input.files[0]){
                   var reader = new FileReader();
                   //Sự kiện file đã được load vào website
                   reader.onload = function(e){
                       //Thay đổi đường dẫn ảnh
                       $('#avatar').attr('src',e.target.result);
                   }
                   reader.readAsDataURL(input.files[0]);
               }
           }
           $(document).ready(function() {
               $('#avatar').click(function(){
                   $('#img').click();
               });
           });
       </script>

@endsection