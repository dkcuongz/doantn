@extends('backend.master.master')
@section('content')
	<!--main-->
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="/admin"><span><i class="fas fa-home"></i></span> Tổng quan</a></li>
				<li class="active">Đơn hàng</li>
			</ol>
		</div>
		<!--/.row-->
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">

				<div class="panel panel-primary">
					<div class="panel-heading">Danh sách đơn đặt hàng đã xử lý</div>
					<div class="panel-body">
						<div class="bootstrap-table">
							<div class="table-responsive">
								<a href="/admin/order" class="btn btn-warning"><span class="glyphicon glyphicon-gift"></span>Đơn chưa chấp nhận</a>
								<a href="/admin/order/refuse" class="btn btn-danger">Đơn đã hủy</a>
								<a href="/admin/order/success" class="btn btn-success">Đơn đã hoàn thành</a>
								<table class="table table-bordered" style="margin-top:20px;">				
                                    <thead>
                                        <tr class="bg-primary">
                                            <th>ID</th>
											<th>Tên khách hàng</th>								
											<th>Email</th>
											<th>Tổng tiền</th>
											<th>Giảm giá</th>
                                            <th>Sđt</th>
                                            <th>Địa chỉ</th>
											<th>Thời gian</th>
											<th width="15%">Tùy chọn</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($order as $item)
										<tr>
											<td>{{$item->id}}</td>
                                                <td>{{$item->name}}</td>
												<td>{{$item->order_customer->email}}</td>
												<td>{{number_format($item->total,0, ',', '.')}} VNĐ</td>
												<td>{{number_format($item->discount,0, ',', '.')}} VNĐ</td>
												<td>{{$item->order_customer->phone}}</td>
												<td>{{$item->order_customer->address}}</td>
												<td>{{$item->created_at}}</td>
												<td>
												<a href="/admin/order/detail/{{$item->id}}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i>Chi tiết</a>
												<a href="{{route('getPdf',$item->id)}}" class="btn btn-warning"><i class="fas fa-file-export"></i></a>
												</td>
											</tr>	
										@endforeach
                                    </tbody>
                                </table>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<!--/.row-->


	</div>
	<!--end main-->
@endsection