@extends('backend.master.master')
@section('content')
	<!--main-->
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="/admin"><span><i class="fas fa-home"></i></span> Tổng quan</a></li>
				<li class="active">Đơn hàng / Chi tiết đặt hàng</li>
			</ol>
		</div>
		<!--/.row-->
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">

				<div class="panel panel-primary">
					<div class="panel-heading">Chi tiết đặt hàng</div>
					<div class="panel-body">
						<div class="bootstrap-table">
							<div class="table-responsive">
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<div class="panel panel-blue">
												<div class="panel-heading dark-overlay">Thông tin khách hàng</div>
												<div class="panel-body">
												<strong><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Họ Tên :{{$order->order_customer->name}}</strong> <br>
													<strong><span class="glyphicon glyphicon-phone" aria-hidden="true"></span> Số điện thoại: {{$order->order_customer->phone}}</strong>
													<br>
													<strong><span class="glyphicon glyphicon-send" aria-hidden="true"></span> Địa chỉ : {{$order->order_customer->address}}</strong>
												</div>
											</div>
										</div>
									</div>


								</div>
								<table class="table table-bordered" style="margin-top:20px;">
									<thead>
										<tr class="bg-primary">
											<th>ID</th>
											<th>Thông tin Sản phẩm</th>
											<th>Số lượng</th>
											<th>Giá sản phẩm</th>
											<th>Thành tiền</th>

										</tr>
									</thead>
									<tbody>
										@php
										$total = 0;	
										@endphp
										<tr>
											@foreach ($order->order_dt as $key=> $item)	
											@php
											 $total += $item->price * $item->quantity;				
											@endphp																									
										<td>{{$key+1}}</td>
											<td>
												<div class="row">
													<div class="col-md-4">
														<img width="100px" src="img/ao-khoac.jpg" class="thumbnail">
													</div>
													<div class="col-md-8">
														<p>Mã sản phẩm:{{$item->order_dt_prdatt->prd_attr_prd->code}} </p>
														<p>Tên Sản phẩm: <strong>{{$item->order_dt_prdatt->prd_attr_prd->name}}</strong></p>
														<p>Loại: <strong>{{$item->order_dt_prdatt->name}}</strong></p>
													</div>
												</div>
											</td>
											<td>{{$item->quantity}}</td>
											<td>{{number_format($item->order_dt_prdatt->prd_attr_prd->price,0, ',', '.')}} VNĐ</td>
											<td>{{number_format($item->price * $item->quantity,0, ',', '.')}} VNĐ</td>

										</tr>
										@endforeach									
									</tbody>

								</table>
								<table class="table">
									<thead>
										<tr>
											<th width='70%'>
												<h4 align='right'>Tổng Tiền :</h4>
											</th>
											<th>
												<h4 align='right' style="color: brown;">{{number_format($total,0, ',', '.')}} VNĐ</h4>
											</th>

										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
								<div class="alert alert-primary" role="alert" align='right'>
								<a name="" id="" class="btn btn-success" href="{{route('back')}}" role="button">Quay lại</a>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<!--/.row-->


	</div>
	<!--end main-->
@endsection