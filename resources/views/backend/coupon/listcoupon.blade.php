@extends('backend.master.master')
@section('title','Quản Lý Coupon')
@section('content')
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/admin"><span><i class="fas fa-home"></i></span> Tổng quan</a></li>
            <li class="active">Danh sách Coupon</li>
        </ol>
    </div>
    <!--/.row-->


    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Danh sách Coupon</h1>
        </div>
    </div>
    <!--/.row-->

    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12">

            <div class="panel panel-primary">

                <div class="panel-body">
                    <div class="bootstrap-table">
                        <div class="table-responsive">
                            @if (session('thongbao'))
                            <div class="alert alert-success" role="alert">
                            <strong>{{session('thongbao')}}</strong>
                                </div>
                            @endif

                            <a href="/admin/coupon/add" class="btn btn-primary">Thêm Coupon</a>
                            <table class="table table-bordered" style="margin-top:20px;">

                                <thead>
                                    <tr class="bg-primary">
                                        <th>ID</th>
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Uses</th>
                                        <th>Max Uses</th>
                                        <th>Type</th>
                                        <th>Discount Amount</th>
                                        <th>Starts At</th>
                                        <th>Expires At</th>
                                        <th>Tùy chọn</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($coupons as $row)
                                    <tr>
                                        <td>{{$row->id}}</td>
                                        <td>{{$row->code}}</td>
                                        <td>{{$row->name}}</td>
                                        <td>{{$row->description}}</td>
                                        <td>{{$row->available}}</td>
                                        <td>{{$row->max_uses}}</td>
                                        <td>{{$row->type}}</td>
                                        <td>{{number_format($row->discount_amount,0,"",".")}} @if ($row->discount_amount>100)
                                                  {{"VNĐ"}}
                                            @else {{"%"}}
                                        @endif</td>
                                        <td>{{date('H:i d-m-Y', strtotime($row->starts_at))}}</td>
                                        <td>{{date('H:i d-m-Y', strtotime($row->expires_at))}}</td>
                                        <td>
                                        <a href="/admin/coupon/edit/{{$row->id}}" class="btn btn-warning"><i class="fa fa-pencil"
                                                    aria-hidden="true"></i> Sửa</a>
                                        <a onclick="return del('{{$row->full}}')" href="/admin/coupon/del/{{$row->id}}" class="btn btn-danger"><i class="fa fa-trash"
                                                    aria-hidden="true"></i> Xóa</a>
                                        </td>
                                    </tr>

                                    @endforeach



                                </tbody>
                            </table>
                            <div align='right'>
                                <ul class="pagination">
                                    {{$coupons->links()}}
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
            <!--/.row-->


        </div>
        <!--end main-->

        <!-- javascript -->
    @section('script')
    @parent
    <script>
        function del(name){
            return confirm('Bạn muốn xóa coupon' + name + 'này không?');
        }
    </script>

    @endsection





    </div>
</div>
@endsection
