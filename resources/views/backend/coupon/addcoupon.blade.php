@extends('backend.master.master')
@section('title','Thêm Coupon')
@section('content')
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/admin"><span><i class="fas fa-home"></i></span> Tổng quan</a></li>
            <li class="active">Thêm Coupon</li>
        </ol>
    </div>
    <!--/.row-->

    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="fas fa-user"></i> Thêm Coupon</div>
                <div class="panel-body">
                    <form method="post">
                        @csrf
                    <div class="row justify-content-center" style="margin-bottom:40px">
                        <div class="col-md-4 col-lg-4 col-lg-offset-2">
                            <div class="form-group">
                                <label>Code</label>
                                <input type="text" name="code" value="{{old('code')}}" class="form-control">
                                {{showErrors($errors,'code')}}
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" value="{{old('description')}}" name="description" class="form-control">
                                {{showErrors($errors,'description')}}
                            </div>      
                            <div class="form-group">
                                <label>Type</label>
                                <select id="type" value="{{old('type')}}" name="type" class="form-control">
                                    <option value="1">Tiền cố định</option>
                                    <option value="2">Phần trăm</option>
                                </select>
                            </div>                       
                            <div class="form-group">
                                <label>Starts At</label>
                            <input  type="date" name="starts_at" value="{{old('starts_at')}}" class="form-control">
                                {{showErrors($errors,'starts_at')}}
                            </div>                   
                        </div>
                        <div class="col-md-4 col-lg-4 col-lg-offset-2" style="margin-left: 0px">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" value="{{old('name')}}" name="name" class="form-control">
                                {{showErrors($errors,'name')}}
                            </div>
                            <div class="form-group">
                                <label>Max Uses</label>
                                <input type="text" value="{{old('max_uses')}}" name="max_uses" class="form-control">
                                {{showErrors($errors,'max_uses')}}
                            </div>
                            <div class="form-group">
                                <label>Discount Amount</label>
                                <input id="discount_amount" type="text" value="{{old('discount_amount')}}" name="discount_amount" class="form-control">
                                {{showErrors($errors,'discount_amount')}}
                            </div>                           
                            <div class="form-group">
                                <label>Expires At</label>
                                <input type="date" value="{{old('expires_at')}}" name="expires_at" class="form-control">
                                {{showErrors($errors,'expires_at')}}
                            </div>                                                 
                        </div>
                        <div class="row">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right">

                                <button class="btn btn-success" type="submit">Thêm Coupon</button>
                                <a class="btn btn-danger"
                                href="/admin/coupon" role="button">Bỏ qua</a>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
                </div>
            </div>

        </div>
    </div>
    <!--/.row-->
</div>
@endsection
@section('script')
@parent
<script>
  $(document).ready(function(){
    $("#type").change(function(){
        var type = $("#type").val();
        $("#discount_amount"). removeAttr("placeholder");
        if(type = 1){  
        $("#discount_amount").attr("placeholder","Vui lòng nhập giá trị là số tiền");
        }
        else if(type = 2){
        $("#discount_amount").attr("placeholder","Vui lòng nhập giá trị nhỏ hơn 99%");
        }     
    });
  });
</script>
@endsection