@extends('backend.master.master')
@section('title','Thêm Sản Phẩm')
@section('content')
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/admin"><span><i class="fas fa-home"></i></span> Tổng quan</a></li>
            <li class="active">Thêm sản phẩm</li>
        </ol>
    </div>
    <!--/.row-->

    <div class="row">
        <div class="col-xs-6 col-md-12 col-lg-12">
            @if (session('thongbao'))
            <div class="alert alert-danger" role="alert">
                    <svg class="glyph stroked checkmark">
                        <use xlink:href="#stroked-checkmark"></use>
                    </svg>{{session('thongbao')}}
                </div>
            @endif
            <div class="panel panel-primary">          
                <div class="panel-heading">Thêm sản phẩm</div>
                <div class="panel-body">
                    <form method="post" enctype="multipart/form-data">
                        @csrf
                          <div class="row" style="margin-bottom:40px">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Danh mục</label>
                                        <select name="category" class="form-control">
                                                {{getCate($categories,1,"",0)}}
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Nhà sản xuất</label>
                                        <select name="auth" class="form-control">
                                            @foreach($auth as $value){
                                                   <option selected  value="{{$value['id']}}">{{$value['name']}}</option>';
                                            }
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Mã sản phẩm</label>
                                        <input value="{{old('code')}}" type="text" name="code" class="form-control">
                                        {{showErrors($errors,'code')}}
                                    </div>
                                    <div class="form-group">
                                        <label>Tên sản phẩm</label>
                                        <input value="{{old('name')}}" type="text" name="name" class="form-control">
                                        {{showErrors($errors,'name')}}
                                    </div>
                                    <div class="form-group">
                                        <label>Giá sản phẩm (Giá chung)</label>
                                        <input value="{{old('price')}}" type="number" name="price" class="form-control">
                                        {{showErrors($errors,'price')}}
                                    </div>
                                    <div class="form-group">
                                        <label>Trạng thái</label>
                                        <select name="state" class="form-control">
                                            <option value="1">Còn hàng</option>
                                            <option value="0">Hết hàng</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Ảnh sản phẩm</label>
                                        <input id="img" type="file" name="img" class="form-control hidden"
                                            onchange="changeImg(this)">
                                            {{showErrors($errors,'img')}}
                                        <img id="avatar" class="thumbnail" width="100%" height="350px" src="img/import-img.png">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Thông tin</label>
                                        <textarea name="info"  style="width: 100%;height: 100px;">{{old('describe')}}</textarea>
                                    </div>
                                 </div>
                    </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Miêu tả</label>
                                    <textarea id="editor"  name="describe" style="width: 100%;height: 100px;">{{old('describe')}}</textarea>
                                </div>
                                <button class="btn btn-success" name="add-product" type="submit">Thêm sản phẩm</button>
                               <a class="btn btn-danger"
                                    href="/admin/product" role="button">Bỏ qua</a>
                            </div>
                        </div>
                    <div class="clearfix"></div>
                </form>
                </div>
            </div>

        </div>
    </div>
    <!--/.row-->
</div>
@endsection

@section('script')
    @parent
    <script>
        function changeImg(input){
               //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
               if(input.files && input.files[0]){
                   var reader = new FileReader();
                   //Sự kiện file đã được load vào website
                   reader.onload = function(e){
                       //Thay đổi đường dẫn ảnh
                       $('#avatar').attr('src',e.target.result);
                   }
                   reader.readAsDataURL(input.files[0]);
               }
           }
           $(document).ready(function() {
               $('#avatar').click(function(){
                   $('#img').click();
               });
           });
       </script>

@endsection