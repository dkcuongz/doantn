@component('mail::message')
# Thông báo mới DkShop
<div>{{$data}}</div>
Cảm ơn bạn đã đăng kí theo dõi tin tức.

@component('mail::button', ['url' => $url])
Xem ngay
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
