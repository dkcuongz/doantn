<?php

use Illuminate\Database\Seeder;

class AuthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('auth')->delete();
        DB::table('auth')->insert([
            ['id'=>1,'name'=>'Adidas'],
            ['id'=>2,'name'=>'Puma'],
            ['id'=>3,'name'=>'Nike'],
            ['id'=>4,'name'=>'Kappa']
        ]);
    }
}
