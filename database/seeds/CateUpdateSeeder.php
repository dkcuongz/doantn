<?php

use Illuminate\Database\Seeder;

class CateUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->where('id', 2)->update(['category_parent'=>'1']);
        DB::table('category')->where('id', 3)->update(['category_parent'=>'1']);
        DB::table('category')->where('id', 4)->update(['category_parent'=>'2']);
        DB::table('category')->where('id', 5)->update(['category_parent'=>'6']);
        DB::table('category')->where('id', 6)->update(['category_parent'=>'2']);
        DB::table('category')->where('id', 7)->update(['category_parent'=>'3']);
        DB::table('category')->where('id', 8)->update(['category_parent'=>'3']);          
    }
}
