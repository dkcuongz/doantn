<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product')->delete();
        DB::table('product')->insert([
            ['id'=>'1','category_id'=>'6','auth_id'=>'1','code'=>'SP01','name'=>'Áo nam da thật MX105','slug'=>'ao-nam-da-that-mx105','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'no-img.jpg'],
            ['id'=>'2','category_id'=>'6','auth_id'=>'2','code'=>'SP02','name'=>' Áo Thun Có Cổ','slug'=>'ao-thun-co-co','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>0,'info'=>'Một sản phẩm mới','img'=>'no-img.jpg'],
            ['id'=>'3','category_id'=>'4','auth_id'=>'3','code'=>'SP03','name'=>'Quần âu nam Prazenta I-QAM61','slug'=>'quan-au-nam-prazenta-i-qam61','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'no-img.jpg'],
            ['id'=>'4','category_id'=>'7','auth_id'=>'1','code'=>'SP04','name'=>'Áo nữ cổ V viền tay xinh xắn','slug'=>'ao-nu-co-v-vien-tay-xinh-xan','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'no-img.jpg'],
            ['id'=>'5','category_id'=>'8','auth_id'=>'2','code'=>'SP05','name'=>'Quần Nữ Suông Ống Rộng','slug'=>'quan-nu-suong-ong-rong','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'no-img.jpg'],
            ['id'=>'6','category_id'=>'6','auth_id'=>'1','code'=>'SP06','name'=>'Áo Adj kẻ chéo','slug'=>'ao-adj-ke-cheo','price'=>250000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'ao-adj-ke-cheo.jpg'],
            ['id'=>'7','category_id'=>'6','auth_id'=>'1','code'=>'SP07','name'=>' Áo body nam Adj','slug'=>'ao-body-nam-adj','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>0,'info'=>'Một sản phẩm mới','img'=>'ao-body-nam-adj.jpg'],
            ['id'=>'8','category_id'=>'6','auth_id'=>'1','code'=>'SP08','name'=>'Áo đá bóng Adj','slug'=>'ao-da-bong-adj','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'ao-da-bong-adj.jpg'],
            ['id'=>'9','category_id'=>'6','auth_id'=>'1','code'=>'SP09','name'=>'Áo khoác nam Adj','slug'=>'ao-khoac-nam-adj','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'ao-khoac-nam-adj.jpg'],
            ['id'=>'10','category_id'=>'7','auth_id'=>'1','code'=>'SP10','name'=>'Áo khoác nữ Adj','slug'=>'ao-khoac-nu-adj','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'ao-khoac-nu-adj.jpg'],
            ['id'=>'11','category_id'=>'7','auth_id'=>'2','code'=>'SP11','name'=>'Áo khoác thể thao nữ Puma','slug'=>'ao-khoac-the-thao-nu-puma','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'ao-khoac-the-thao-nu-puma.jpg'],
            ['id'=>'12','category_id'=>'6','auth_id'=>'1','code'=>'SP12','name'=>' Áo nam Adj bó sát','slug'=>'ao-nam-adj-bo-sat','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>0,'info'=>'Một sản phẩm mới','img'=>'ao-nam-adj-bo-sat.jpg'],
            ['id'=>'13','category_id'=>'6','auth_id'=>'1','code'=>'SP13','name'=>'Áo nam bó Adj','slug'=>'ao-nam-bo-adj','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'ao-nam-bo-adj.jpg'],
            ['id'=>'14','category_id'=>'6','auth_id'=>'2','code'=>'SP14','name'=>'Áo nam Puma trơn','slug'=>'ao-nam-puma-tron','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'ao-nam-puma-tron.jpg'],
            ['id'=>'15','category_id'=>'6','auth_id'=>'2','code'=>'SP15','name'=>'Áo nam Puma xanh','slug'=>'ao-nam-puma-xanh','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'ao-nam-puma-xanh.jpg'],
            ['id'=>'16','category_id'=>'7','auth_id'=>'1','code'=>'SP16','name'=>'Áo nữ Adj','slug'=>'ao-nu-adj','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'ao-nu-adj.jpg'],
            ['id'=>'17','category_id'=>'7','auth_id'=>'4','code'=>'SP17','name'=>' Áo nữ Kappa cổ trơn','slug'=>'ao-nu-kappa-co-tron','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>0,'info'=>'Một sản phẩm mới','img'=>'ao-nu-kappa-co-tron.jpg'],
            ['id'=>'18','category_id'=>'7','auth_id'=>'2','code'=>'SP18','name'=>'Áo nữ Puma rộng','slug'=>'ao-nu-puma-rong','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'ao-nu-puma-rong.jpg'],
            ['id'=>'19','category_id'=>'7','auth_id'=>'2','code'=>'SP19','name'=>'Áo Puma nữ','slug'=>'ao-puma-nu','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'ao-puma-nu.jpg'],
            ['id'=>'20','category_id'=>'7','auth_id'=>'1','code'=>'SP20','name'=>'Áo thể thao Adj ôm','slug'=>'ao-the-thao-nam-adj-om','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'ao-the-thao-nam-adj-om.jpg'],
            ['id'=>'21','category_id'=>'6','auth_id'=>'3','code'=>'SP21','name'=>'Áo thể thao nam dunlop','slug'=>'ao-the-thao-nam-dunlop','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'ao-the-thao-nam-dunlop.jpg'],
            ['id'=>'22','category_id'=>'7','auth_id'=>'3','code'=>'SP22','name'=>' Áo thể thao nữ bó','slug'=>'ao-the-thao-nu-bo','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>0,'info'=>'Một sản phẩm mới','img'=>'ao-the-thao-nu-bo.jpg'],
            ['id'=>'23','category_id'=>'4','auth_id'=>'1','code'=>'SP23','name'=>'Quần chạy bộ nam Adj','slug'=>'quan-chay-bo-nam-adj','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-chay-bo-nam-adj.jpg'],
            ['id'=>'24','category_id'=>'4','auth_id'=>'1','code'=>'SP24','name'=>'Quần dài Adj nam','slug'=>'quan-dai-adj-nam','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-dai-adj-nam.jpg'],
            ['id'=>'25','category_id'=>'8','auth_id'=>'2','code'=>'SP25','name'=>'Quần dài chạy bộ nữ Puma','slug'=>'quan-dai-chay-bo-nu-puma','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-dai-chay-bo-nu-puma.jpg'],
            ['id'=>'26','category_id'=>'4','auth_id'=>'1','code'=>'SP26','name'=>'Quần dài nam Adj','slug'=>'quan-dai-nam-adj','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-dai-nam-adj.jpg'],
            ['id'=>'27','category_id'=>'4','auth_id'=>'4','code'=>'SP27','name'=>'Quần dài nam Kappa','slug'=>'quan-dai-nam-kappa','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>0,'info'=>'Một sản phẩm mới','img'=>'quan-dai-nam-kappa.jpg'],
            ['id'=>'28','category_id'=>'4','auth_id'=>'2','code'=>'SP28','name'=>'Quần dài nam Puma','slug'=>'quan-dai-nam-puma','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-dai-nam-puma.jpg'],
            ['id'=>'29','category_id'=>'4','auth_id'=>'3','code'=>'SP29','name'=>'Quần dài nam sọc trắng Nike','slug'=>'quan-dai-nam-soc-trang-nike','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-dai-nam-soc-trang-nike.jpg'],
            ['id'=>'30','category_id'=>'4','auth_id'=>'2','code'=>'SP30','name'=>'Quần da trơn nam Puma','slug'=>'quan-da-tron-nam-puma','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-da-tron-nam-puma.jpg'],
            ['id'=>'31','category_id'=>'4','auth_id'=>'2','code'=>'SP31','name'=>'Quần đùi chạy bộ Puma','slug'=>'quan-dui-chay-bo-puma','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-dui-chay-bo-puma.jpg'],
            ['id'=>'32','category_id'=>'4','auth_id'=>'2','code'=>'SP32','name'=>'Quần đùi donpa nam Puma','slug'=>'quan-dui-donpo-nam-puma','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>0,'info'=>'Một sản phẩm mới','img'=>'quan-dui-donpo-nam-puma.jpg'],
            ['id'=>'33','category_id'=>'4','auth_id'=>'2','code'=>'SP33','name'=>'Quần đùi nam Puma','slug'=>'quan-dui-nam-puma','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-dui-nam-puma.jpg'],
            ['id'=>'34','category_id'=>'4','auth_id'=>'2','code'=>'SP34','name'=>'Quần đùi ngố nam Puma','slug'=>'quan-dui-ngo-nam-puma','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-dui-ngo-nam-puma.jpg'],
            ['id'=>'35','category_id'=>'8','auth_id'=>'3','code'=>'SP35','name'=>'Quần đùi nữ Nike','slug'=>'quan-dui-nu-nike','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-dui-nu-nike.jpg'],
            ['id'=>'36','category_id'=>'8','auth_id'=>'3','code'=>'SP36','name'=>'Quần gym nữ Nike','slug'=>'quan-gym-nu-nike','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-gym-nu-nike.jpg'],
            ['id'=>'37','category_id'=>'4','auth_id'=>'1','code'=>'SP37','name'=>'Quần nam Adj thời trang','slug'=>'quan-nam-adj-thoi-trang','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>0,'info'=>'Một sản phẩm mới','img'=>'quan-nam-adj-thoi-trang.jpg'],
            ['id'=>'38','category_id'=>'4','auth_id'=>'2','code'=>'SP38','name'=>'Quần nam cổ chân Puma','slug'=>'quan-nam-co-chan-puma','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-nam-co-chan-puma.jpg'],
            ['id'=>'39','category_id'=>'4','auth_id'=>'3','code'=>'SP39','name'=>'Quần nam donpo','slug'=>'quan-nam-donpo','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-nam-donpo.jpg'],
            ['id'=>'40','category_id'=>'4','auth_id'=>'4','code'=>'SP40','name'=>'Quần nam Kappa','slug'=>'quan-nam-kappa','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-nam-kappa.jpg'],
            ['id'=>'41','category_id'=>'4','auth_id'=>'3','code'=>'SP41','name'=>'Quần nam Nike','slug'=>'quan-nam-nike','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-nam-nike.jpg'],
            ['id'=>'42','category_id'=>'8','auth_id'=>'1','code'=>'SP42','name'=>'Quần nữ Adj bó sát','slug'=>'quan-nu-adj-bo-sat','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>0,'info'=>'Một sản phẩm mới','img'=>'quan-nu-adj-bo-sat.jpg'],
            ['id'=>'43','category_id'=>'8','auth_id'=>'4','code'=>'SP43','name'=>'Quần nữ Kappa','slug'=>'quan-nu-kappa','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-nu-kappa.jpg'],
            ['id'=>'44','category_id'=>'8','auth_id'=>'3','code'=>'SP44','name'=>'Quần nữ leg','slug'=>'quan-nu-leg','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-nu-leg.jpg'],
            ['id'=>'45','category_id'=>'8','auth_id'=>'3','code'=>'SP45','name'=>'Quần nữ ống rộng Nike','slug'=>'quan-nu-ong-rong-nike','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-nu-ong-rong-nike.jpg'],
            ['id'=>'46','category_id'=>'8','auth_id'=>'2','code'=>'SP46','name'=>'Quần nữ Puma kẻ sọc','slug'=>'quan-nu-puma-ke-soc','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-nu-puma-ke-soc.jpg'],
            ['id'=>'47','category_id'=>'8','auth_id'=>'2','code'=>'SP47','name'=>'Quần nữ Puma kẻ xám','slug'=>'quan-nu-puma-ke-xam','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>0,'info'=>'Một sản phẩm mới','img'=>'quan-nu-puma-ke-xam.jpg'],
            ['id'=>'48','category_id'=>'8','auth_id'=>'2','code'=>'SP48','name'=>'Quần Puma kẻ sọc','slug'=>'quan-puma-ke-soc','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-puma-ke-soc.jpg'],
            ['id'=>'49','category_id'=>'4','auth_id'=>'2','code'=>'SP49','name'=>'Quần Puma kẻ sọc nam','slug'=>'quan-puma-ke-soc-nam','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-puma-ke-soc-nam.jpg'],
            ['id'=>'50','category_id'=>'8','auth_id'=>'2','code'=>'SP50','name'=>'Quần Puma kẻ sọc nữ','slug'=>'quan-puma-ke-soc-nu','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-puma-ke-soc-nu.jpg'],
            ['id'=>'51','category_id'=>'8','auth_id'=>'2','code'=>'SP51','name'=>'Quần thể thao phong cách nữ Puma','slug'=>'quan-the-thao-phong-cach-nu-puma','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-the-thao-phong-cach-nu-puma.jpg'],
            ['id'=>'52','category_id'=>'8','auth_id'=>'3','code'=>'SP52','name'=>'Quần leeging nữ Nike','slug'=>'quan-legging-nu-nike','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>0,'info'=>'Một sản phẩm mới','img'=>'quan-legging-nu-nike.jpg'],
            ['id'=>'53','category_id'=>'4','auth_id'=>'4','code'=>'SP53','name'=>'Quần boxer nam Kappa','slug'=>'quan-boxer-nam-kappa','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-boxer-nam-kappa.jpg'],
            ['id'=>'54','category_id'=>'6','auth_id'=>'4','code'=>'SP54','name'=>'Áo nam Kappa','slug'=>'ao-nam-kappa','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'ao-nam-kappa.jpg'],
            ['id'=>'55','category_id'=>'5','auth_id'=>'3','code'=>'SP55','name'=>'Quần đùi nam Nike 2020','slug'=>'quan-dui-nam-nike-2020','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-dui-nam-nike-2020.jpg'],
            ['id'=>'56','category_id'=>'4','auth_id'=>'3','code'=>'SP56','name'=>'Quần ngố nam Nike','slug'=>'quan-ngo-nam-nike','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'quan-ngo-nam-nike.jpg'],
            ['id'=>'57','category_id'=>'6','auth_id'=>'1','code'=>'SP57','name'=>'Áo Adj nam','slug'=>'ao-adj-nam','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>0,'info'=>'Một sản phẩm mới','img'=>'ao-adj-nam.jpg'],
            ['id'=>'58','category_id'=>'6','auth_id'=>'1','code'=>'SP58','name'=>'Áo Nike Nam cổ tròn','slug'=>'ao-nike-nam-co-tron','price'=>500000,'quantity'=>1000,'quantity_sold'=>0,'state'=>1,'info'=>'Một sản phẩm mới','img'=>'ao-nike-nam-co-tron.jpg'],
            ]);
    }
}
