<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->delete();
        DB::table('category')->insert([
            ['id'=>1,'name'=>'Danh mục','slug'=>'danh-muc'],
            ['id'=>2,'name'=>'Nam','slug'=>'nam'],
            ['id'=>3,'name'=>'Nữ','slug'=>'nu'],
            ['id'=>4,'name'=>'Quần Nam','slug'=>'quan-nam'],
            ['id'=>5,'name'=>'Áo Nam 2020','slug'=>'ao-nam-20'],
            ['id'=>6,'name'=>'Áo Nam','slug'=>'ao-nam'],
            ['id'=>7,'name'=>'Áo Nữ','slug'=>'ao-nu'],
            ['id'=>8,'name'=>'Quần Nữ','slug'=>'quan-nu']
        ]);
    }
}
