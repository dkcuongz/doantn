<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->delete();
        DB::table('admins')->insert([
            ['id'=>1,'name'=>'Đinh Văn Cường','email'=>'admin@gmail.com','password'=>bcrypt('123456'),'address'=>'Thường tín','phone'=>'0356653301','level'=>1],
            ['id'=>2,'name'=>'Nguyễn thế vũ','email'=>'zimpro@gmail.com','password'=>bcrypt('123456'),'address'=>'Bắc giang','phone'=>'0356654487','level'=>1],
            ['id'=>3,'name'=>'Nguyễn thế phúc','email'=>'phucnguyenthe0809@gmail.com','password'=>bcrypt('123456'),'address'=>'Huế','phone'=>'0352264487','level'=>1],
            ['id'=>4,'name'=>'Nguyễn Văn Công','email'=>'zimpro9x@gmail.com','password'=>bcrypt('123456'),'address'=>'Nghệ An','phone'=>'0357846659','level'=>1],
        ]);
    }
}
