<?php

use Illuminate\Database\Seeder;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     
        DB::table('attribute')->delete();
        DB::table('attribute')->insert([
            ['id'=>1,'color'=>'Đỏ','size'=>'M','description'=>'red'],
            ['id'=>2,'color'=>'Xanh','size'=>'M','description'=>'green'],
            ['id'=>3,'color'=>'Trắng','size'=>'M','description'=>'white'],
            ['id'=>4,'color'=>'Vàng','size'=>'M','description'=>'yellow'],
            ['id'=>5,'color'=>'Nâu','size'=>'XL','description'=>'brown'],
            ['id'=>6,'color'=>'Xám','size'=>'XXL','description'=>'grey']
        ]);
    }
}
