<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(AuthSeeder::class);
        $this->call(AttributeSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(Product_AttributeSeeder::class);
        $this->call(OrderSeeder::class);
        $this->call(Order_DetailSeeder::class);
        $this->call(Order_EntrySeeder::class);
        $this->call(Order_DetailSeeder::class);
        $this->call(CateUpdateSeeder::class);
    }
}
