<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon', function (Blueprint $table) {
            $table->bigIncrements( 'id' );
    
            // The voucher code
            $table->string( 'code' )->nullable( );
        
            // The human readable voucher code name
            $table->string( 'name' );
        
            // The description of the voucher - Not necessary 
            $table->text( 'description' )->nullable( );
        
            // The number of available
            $table->integer( 'available' )->unsigned( )->nullable( );
        
            // The max uses this voucher has
            $table->integer( 'max_uses' )->unsigned()->nullable( );
        
            // The type can be: voucher, discount, sale. What ever you want.
            $table->tinyInteger( 'type' )->unsigned( );
        
            // The amount to discount by (in pennies) in this example.
            $table->integer( 'discount_amount' );
            
            // When the voucher begins
            $table->timestamp( 'starts_at' )->default(\DB::raw('CURRENT_TIMESTAMP'));
        
            // When the voucher ends
            $table->timestamp( 'expires_at' )->default(\DB::raw('CURRENT_TIMESTAMP'));
        
            // You know what this is...
            $table->timestamps( );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon');
    }
}
