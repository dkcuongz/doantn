<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderEntryDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_entry_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_entry_id')->unsigned();
            $table->integer('product_attribute_id')->unsigned();  
            $table->integer('price')->unsigned();
            $table->integer('quantity')->unsigned();
            $table->string('img');
            $table->foreign('order_entry_id')->references('id')->on('order_entry')->onDelete('cascade');
            $table->foreign('product_attribute_id')->references('id')->on('product_attribute')->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_entry_detail');
    }
}
