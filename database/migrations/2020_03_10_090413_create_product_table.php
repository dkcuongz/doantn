<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('auth_id')->unsigned();
            $table->string('code',50)->unique(); //không cho phép cột mã sản phẩm trùng
            $table->string('name');
            $table->string('slug');
            $table->decimal('price', 18, 0)->default(0); //mặc định là 0 nếu không có giá trị
            $table->integer('quantity');
            $table->integer('quantity_sold')->default(0);
            $table->string('state',10);
            $table->string('info')->nullable();
            $table->string('describle')->nullable();
            $table->string('img');
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade');
            $table->foreign('auth_id')->references('id')->on('auth')->onDelete('cascade');
            //$table->foreign('product_parent')->references('id')->on('product')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
