<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvoiceMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $data,$url;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order,$detail)
    {
        $this->order = $order;
        $this->detail = $detail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.user.invoice')->subject('Đặt hàng thành công')
        ->with(['order'=>$this->order,
                'detail' => $this->detail
                 ]);;
    }
}
