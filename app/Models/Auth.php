<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Auth extends Model
{
    protected $table = 'auth';
    public $timestamps = false;
    public function auth_entry()
    {
        return $this->hasMany('App\Models\Order_Entry', 'auth_id', 'id');
    }
}
