<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_Detail extends Model
{
    protected $table = 'order_detail';
    public $timestamps = false;
    public function order_dt_prdatt(){
        return $this->belongsTo('App\Models\Product_Attribute', 'product_attribute_id', 'id');
    }
    public function order(){
        return $this->belongsTo('App\Models\Order', 'order_id', 'id');
    }
}
