<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table='order';

    public function order_user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function order_customer()
    {
        return $this->belongsTo('App\User','user_id', 'id');
    }
    public function order_dt()
    {
        return $this->hasMany('App\Models\Order_Detail', 'order_id', 'id');
    }
}
