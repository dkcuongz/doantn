<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $table = 'attribute';
    public $timestamps = false;
    public function attribute_prd()
    {
        return $this->belongsToMany('App\Models\Product', 'product_attribute', 'product_id', 'attribute_id ');
    }
    public function prd_attribute()
    {
        return $this->hasMany('App\Models\Product_Attribute', 'attribute_id', 'id');
    }
}
