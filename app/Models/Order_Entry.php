<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_Entry extends Model
{
    protected $table = 'order_entry';
    public function entry_auth()
    {
        return $this->belongsTo('App\Models\Auth', 'auth_id', 'id');
    }
    public function entry_user()
    {
        return $this->belongsTo('App\Admin', 'admin_id', 'id');
    }
    public function entry_dt()
    {
        return $this->hasMany('App\Models\Order_Entry_Detail', 'order_entry_id', 'id');
    }
    
}
