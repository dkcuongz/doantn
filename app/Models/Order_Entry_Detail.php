<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_Entry_Detail extends Model
{
    protected $table = 'order_entry_detail';
    public $timestamps = false;
    public function entry_dt_prdatt(){
        return $this->belongsTo('App\Models\Product_Attribute', 'product_attribute_id', 'id');
    }
    public function entry_order(){
        return $this->belongsTo('App\Models\Order_Entry', 'order_entry_id', 'id');
    }
}
