<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_Attribute extends Model
{
    protected $table = 'product_attribute';
    public $timestamps = false;
    public function prd_attr()
    {
        return $this->belongsTo('App\Models\Attribute', 'attribute_id', 'id');
    }
    public function prd_attr_entry_dt()
    {
        return $this->hasMany('App\Models\Order_Entry_Detail', 'product_attribute_id', 'id');
    }
    public function prd_attr_order_dt()
    {
        return $this->hasMany('App\Models\Order_Detail', 'product_attribute_id', 'id');
    }
    public function prd_attr_prd()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }
}
