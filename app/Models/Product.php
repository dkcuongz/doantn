<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id', 'id');
    }
    public function prd_attribute()
    {
        return $this->belongsToMany('App\Models\Attribute', 'product_attribute', 'product_id', 'attribute_id');
    }
    public function prd_prd_attr()
    {
        return $this->hasMany('App\Models\Product_Attribute', 'product_id', 'id');
    }
}
