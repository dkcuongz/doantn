<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AttributeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'size'=>'required',
            'color'=>'required',
            'description'=>'required',

        ];
    }
    public function messages()
    {
        return [
            'size.required'=>'Size không được để trống',
            'color.required'=>'Color không được để trống',
            'description.required'=>'Description không được để trống',
 
        ];
    }
}
