<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'=>'required|unique:coupon,code',
            'name'=>'required|min:5',
            'max_uses'=>'required|numeric',
            'description'=>'required|min:20',
            'discount_amount'=>'required|numeric',
            'starts_at'  => 'required|date|after:today',
            'expires_at'=>'required|date|after:start_date',
        ];
    }
    public function messages()
    {
        return [
            'code.required'=>'Không được để trống Code',
            'code.unique'=>'Code đã tồn tại',
            'name.required'=>'Không được để trống Tên khuyến mãi',
            'name.min'=>'Tên khuyến mãi không được nhỏ hơn 5 ký tự',
            'max_uses.required'=>'Số lượng code',
            'max_uses.numeric'=>'Số lượng code phải là số',
            'description.required'=>'Mô tả không được để trống',
            'description.min'=>'Mô tả được không nhỏ hơn 20 ký tự',
            'discount_amount.required'=>'Lượng khuyến mãi được để trống',
            'discount_amount.min'=>'Lượng khuyến mãi phải là số',
            'starts_at.required'=>'Không được để trống Ngày bắt đầu khuyến mãi',
            'starts_at.date_format'=>'Ngày bắt đầu khuyến mãi không đúng định dạng',
            'starts_at.after'=>'Ngày bắt đầu khuyến mãi không đúng ',
            'expires_at.required'=>'Không được để trống  Ngày kết thúc khuyến mãi',
            'expires_at.date_format'=>'Ngày kết thúc khuyến mãi không đúng định dạng',
            'expires_at.after'=>'Ngày kết thúc khuyến mãi không đúng ',
        ];
    }
}
