<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddProductAttributeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'size'=>'required',
            'color'=>'required',
            'description'=>'required',
            'quantity'=>'numeric|required',
            'img'=>'image', // file phải là định dạng ảnh
        ];
    }
    public function messages()
    {
        return [
            'size.required'=>'Không được để trống Size',
            'color.required'=>'Không được để trống Màu',
            'description.required'=>'Mô tả không được để trống',
            'quantity.required'=>'Số lượng không được để trống',
            'quantity.numeric'=>'Số lượng không đúng định dạng',
            'img.image'=>'File Ảnh không đúng định dạng!',
        ];
    }
}
