<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth'=>'verified']);
    }
    function getLogin(){
        return view('frontend.login.account');
    }
}
