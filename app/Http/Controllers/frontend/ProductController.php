<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Product_Attribute;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class ProductController extends Controller
{
    function getShop(request $r) {       
        if($r->has('start')){
        $data['start'] = str_replace('.', '', $r->start);
        $data['finish'] = str_replace('.', '',  $r->finish);
        $data['string'] = $r->string;
        $data['number'] = $r->number;
        if ($data['string']!= "0") {                         
                    $data['products'] = Product::where('img','<>','no-img.jpg')
                    ->whereBetween('price',[$data['start'],$data['finish']])
                    ->orderBy($data['string'], 'ASC')
                    ->paginate($data['number']);              
        } 
        else {             
                $data['string'] = 0;
                $data['number'] = 6;
                $data['products'] = Product::where('img','<>','no-img.jpg')
                ->whereBetween('price',[$data['start'],$data['finish']])
                ->paginate($data['number']);
            }     
        }
    else {
            $data['string'] = 0;
            $data['number'] = 6;
            $data['start'] = 0;
            $data['finish'] = 2000000;
            $data['products'] = Product::where('img','<>','no-img.jpg')->paginate(6);
        } 
        $data['categories'] = Category::all();
        $data['prd_popular']= Product::where('img','<>','no-img.jpg')->orderBy("quantity_sold", 'DESC')->take(3)->get();
        return view('frontend.product.product',$data);
    }
    function getPrdCate($slug_cate,request $r){
        $data['cate_slug'] = Category::where('slug',$slug_cate)
        ->first();
        $data['cate'] = Category::where('id',$data['cate_slug']->id)
        ->orWhere('category_parent',$data['cate_slug']->id)
        ->get();
        if($r->has('start')){
            $data['start'] = str_replace('.', '', $r->start);
            $data['finish'] = str_replace('.', '',  $r->finish);
            $data['string'] = $r->string;
            $data['number'] = $r->number;
        if ($data['string']!= "0") {
            $limit = $data['number'];        
            foreach ($data['cate'] as $value) {    
                if(!isset($data['products'])){
                  $data['products'] = $value->product()->where('img','<>','no-img.jpg')
                  ->whereBetween('price',[$data['start'],$data['finish']])->orderBy($data['string'], 'ASC')->get();
                }
                else {
                $products = $value->product()->where('img','<>','no-img.jpg')
                ->whereBetween('price',[$data['start'],$data['finish']])->orderBy($data['string'], 'ASC')->get();
                foreach ( $products as $item){  
                    $data['products']->push($item); 
                }             
             }                    
        }
        $data['products'] = $data['products']->sortBy($data['string']);
       }
       else {     
        $limit = 6;        
        foreach ($data['cate'] as $value) {    
            if(!isset($data['products'])){
              $data['products'] = $value->product()->where('img','<>','no-img.jpg')
              ->whereBetween('price',[$data['start'],$data['finish']])->get();
            }
            else {
            $products = $value->product()->where('img','<>','no-img.jpg')
            ->whereBetween('price',[$data['start'],$data['finish']])->get();
            foreach ( $products as $item){  
                $data['products']->push($item); 
            }             
         }                 
        }
        $data['product'] = $data['products']->sortBy($data['string']);
    }    
    }else {
            $limit = 6;          
            $data['string'] = 0;
            $data['number'] = 6;
            $data['start'] = 0;
            $data['finish'] = 2000000;
            foreach ($data['cate'] as $value) {    
                if(!isset($data['products'])){
                  $data['products'] = $value->product()->where('img','<>','no-img.jpg')->get();
                }
                else {
                $products = $value->product()->where('img','<>','no-img.jpg')->get();
                foreach ( $products as $item){  
                    $data['products']->push($item); 
                }             
             }
            }
        }
        $page = $r->page;
        $count = count($data['products']);
        $data['slug_cate'] = $slug_cate;
        $data['prd_popular']= Product::where('img','<>','no-img.jpg')->orderBy("quantity_sold", 'DESC')->take(3)->get();
        $data['products'] = new Paginator( $data['products']
        ->forPage($page, $limit), $count, $limit, $page, ['path'  => Paginator::resolveCurrentPath()]);
        $data['categories'] = Category::all();
        return view('frontend.product.product',$data);
    }
    function getDataModal(Request $r){
     $data['prd'] = Product::find($r->prd_id);
     $data['cate'] = $data['prd']->category;
     $data['prd_attr'] = Product_Attribute::where('product_id',$r->prd_id)->where('quantity','>',0)->get();
     return $data;
    }
    function getDataAddcart(Request $r){
        $data['prd_add'] = Product::find($r->prd_id);
        $data['prd_attr_add'] = Product_Attribute::where('product_id',$r->prd_id)->where('quantity','>',0)->get();
        return $data;
    }    
    function getDetail($slug_prd) {
        $arr = explode('-',$slug_prd);
        $id = array_pop($arr);
        $data['prd'] = Product::find($id);
        $data['prd_new'] = Product::where('img','<>','no-img.jpg')->orderBy('updated_at','desc')->take(7)->get();
        $data['prd_relate'] = Product::where('img','<>','no-img.jpg')->where('category_id',$data['prd']->category_id)->orderBy('updated_at','desc')->take(4)->get();
        return view('frontend.product.product-detail',$data);
    }
    function getDetailPrdAttr($slug_prd,$idPrd){
        $data['prd'] = Product_Attribute::find($idPrd);
        return view('frontend.product.product-detail',$data);
    }
}