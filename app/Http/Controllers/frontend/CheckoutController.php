<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CheckoutRequest;
use App\Mail\InvoiceMail;
use App\Models\Order;
use App\Models\Order_Detail;
use App\Models\Product;
use App\Models\Product_Attribute;
use App\Models\UserCoupon;
use Illuminate\Http\Request;
use Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class CheckoutController extends Controller
{
    function getCheckout() {
        $data['cart'] = Cart::content();
        $data['total'] = Cart::total(0,"",".");
        $data['total_s'] =Cart::total(0,"","");
        return view('frontend.checkout.checkout',$data);
    }

    function postCheckout(CheckoutRequest $r) {
        $order = new Order();
        if(Auth::user()){
        $order->user_id = Auth::user()->id;
        }
        else{
            $order->user_id = 1;
        }
        if(Session::has('user_id') && Auth::user()->id == session('user_id')){
             if(session('discount')>100){
                $order->total = Cart::total(0,"","")-session('discount');
                $order->discount = session('discount');
             }
             else{
                $order->total = Cart::total(0,"","")*(100-session('discount'));
                $order->discount =  Cart::total(0,"","")*session('discount')/100;
             }
             $used = new UserCoupon();
             $used->user_id = session('user_id');
             $used->coupon_id = session('coupon_id');
             session()->forget('user_id','coupon_id','discount');
             $used->save();
        }
        else{
            $order->total = Cart::total(0,"","");
            $order->discount = 0;
        }
        $order->admin_id = 1;
        $order->name = $r->name;
        $order->address = $r->address;
        $order->phone = $r->phone;
        $order->email = $r->email;     
        $order->state = 0;
        $order->save();
        foreach (Cart::content() as $row) {
            //dd($row->options->id_attr);
            $order_dt = new Order_Detail();
            $prd_att = Product_Attribute::find($row->options->id_attr);
            Product_Attribute::where('id',$row->options->id_attr)->update(['quantity'=>$prd_att->quantity - $row->qty]);
            Product::where('id',$prd_att->prd_attr_prd->id)->update(['quantity'=>$prd_att->prd_attr_prd->quantity - $row->qty,
            'quantity_sold'=>$prd_att->prd_attr_prd->quantity_sold + $row->qty]);
            $order_dt->order_id = $order->id;
            $order_dt->product_attribute_id	 = $row->options->id_attr;
            $order_dt->price = $row->price;
            $order_dt->quantity = $row->qty;
            $order_dt->img = $row->options->img;
            $order_dt->save();
        }
        Cart::destroy();
        return redirect('/checkout/complete/'.$order->id);
    }

    function getComplete($idOrder) {
        $data['order'] = Order::find($idOrder);
        return view('frontend.checkout.complete',$data);
    }
}
