<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use App\Models\Product;
use App\Models\Product_Attribute;
use App\Models\UserCoupon;
use Illuminate\Http\Request;
use Cart;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
class CartController extends Controller
{
    public function GetCart()
    {
        $data['cart'] = Cart::Content();
        $data['total'] = Cart::total(0,'','.');
        $data['total_s'] =Cart::total(0,"","");
        return view('frontend.cart.cart',$data);
    }
    public function AddCart(Request $request)
    {
        $product = Product_Attribute::find($request->loai);
        $prd = $product->prd_attr_prd;
        if ($request->quantity!='') {
            $qty=$request->quantity;
        } else {
            $qty=1;
        }
        Cart::add(['id' => $prd->code, 
        'name' => $prd->name, 
        'qty' => $qty, 
        'price' =>$prd->price,
        'weight' => 0,
        'options' => ['img' =>$prd->img,'attr'=>$product->name,'id_attr'=>$product->id,'name_attr'=>$product->name]]);
        return redirect()->back();
        
    }
    public function postCoupon(Request $r)
    {
        if (Auth::user()) {
           $user_login = Auth::user()->id;
           $code = trim(strtoupper($r->code));
           $date = Carbon::now()->format('Y-m-d');
           $km = Coupon::where('code',$code)->whereDate('starts_at', '<=',$date  )->whereDate('expires_at', '>=', $date )
           ->where('available','>',0)->first();
           if($km != ''){
                $user = UserCoupon::where('user_id',$user_login)->where('coupon_id',$km->id)->get();
                if($user = ''){
                    session(['user_id' => $user_login,'coupon_id'=>$km->id,'discount'=>$km->discount_amount]);
                    // dd(session('coupon_id'),session('user_id'));
                    return redirect()->back()->with('thongbao1','Mã được áp dụng!');
                }
                else{
                    return redirect()->back()->with('thongbao','Tài khoản này đã dùng mã!');
                }
           }
           else{
                return redirect()->back()->with('thongbao','Chương trình đã kết thúc!');
            }
        }
        else{
            return redirect()->back()->with('thongbao','Vui lòng đăng nhập!');
        }
    }
    function delCart($rowId){
        Cart::remove($rowId);
        return redirect()->back();
    }
    function updateCart($rowId,$qty){
        Cart::update($rowId, $qty);
        return 'success';
    }
}
