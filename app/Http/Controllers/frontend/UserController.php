<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Order_Detail;
use App\Models\Product;
use App\Models\Product_Attribute;
use App\User;
use PDF;
class UserController extends Controller
{
    function getProfile($idUser){   
        $data['user'] = User::where('id',$idUser)->first();    
        return view('frontend.user.userprofile',$data);
    }
    function getUserorder($idUser){   
        $data['order'] = Order::where('user_id',$idUser)->orderBy('updated_at','desc')->get();
        return view('frontend.user.order',$data);
    }
    function getOrderDetail($idUser,$idOrder){   
        $data['order'] = Order::where('user_id',$idUser)->where('id',$idOrder)->first();
        $data['detail'] = Order_Detail::where('order_id',$idOrder)->get();
        return view('frontend.user.detailorder',$data);
    }
    function refuseOrder($idUser,$idOrder){
        $order = Order::where('user_id',$idUser)->where('id',$idOrder)->first();
        $order->state = 2;
        $order->save();
        $detail = Order_Detail::where('order_id',$idOrder)->get();
        foreach ($detail as $value) {
           $prd_att = Product_Attribute::where('id',$value->product_attribute_id)->first();
           Product::where('id',$prd_att->prd_attr_prd->id)->update(['quantity'=>$prd_att->prd_attr_prd->quantity + $value->qty,
           'quantity_sold'=>$prd_att->prd_attr_prd->quantity_sold - $value->qty]);
           $prd_att->quantity = $prd_att->quantity + $value->quantity;
           $prd_att->save();
        }
        return redirect(route('getUserorder',[$idUser,$idOrder]))->with('thongbao',"Đã hủy thành công!");
    }
    public function getOrderPdf($idUser,$idOrder)
    {   $order = Order::where('user_id',$idUser)->where('id',$idOrder)->first();
        $detail = Order_Detail::where('order_id',$order->id)->get();
    	$pdf = PDF::loadView('emails.user.invoice',  compact('order','detail'));
    	return $pdf->download('Invoice'.$order->name.'.pdf');
    }
    function postProfile(EditUserRequest $r,$idUser){   
        $user =  User::find($idUser);
        $user->name = $r->name;
        $user->address = $r->address;
        $user->email = $r->email;  
        $user->phone = $r->phone;
        if( $r->password !='' && $r->password === $r->confirmpassword){
        $user->password = $r->password;
        }
        elseif($r->password !='' && $r->password != $r->confirmpassword){ 
             return redirect()->back()->with('thongbao','Mật khẩu không khớp!');
            }
        $user->save();
        return redirect()->back()->with('thongbao','Đã sửa thành công!');
    }
}
