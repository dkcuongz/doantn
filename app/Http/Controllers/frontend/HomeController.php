<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\EditUserRequest;
use App\Http\Requests\SubRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\Subcribe;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    function getIndex() {
        $data['prd_men'] = Product::where('img','<>','no-img.jpg')
        ->join('category', 'product.category_id', '=', 'category.id')
        ->select('product.*', 'category.category_parent')
        ->where('category.category_parent',2)
        ->orderBy('updated_at','desc')->take(8)->get();
        $data['prd_women'] = Product::where('img','<>','no-img.jpg')
        ->join('category', 'product.category_id', '=', 'category.id')
        ->select('product.*', 'category.category_parent')
        ->where('category.category_parent',3)
        ->orderBy('updated_at','desc')->take(8)->get();
        $data['prd_popular'] = Product::where('img','<>','no-img.jpg')
        ->orderBy('quantity_sold','desc')->take(4)->get();
        $data['prd_feature'] = Product::where('img','<>','no-img.jpg')
        ->where('state',1)
        ->orderBy('updated_at','desc')->take(8)->get();
        $data['prd_new'] = Product::where('img','<>','no-img.jpg')
        ->orderBy('updated_at','desc')->take(4)->get();
        return view('frontend.index',$data);
    }
    function search(Request $request)
    {
          $search = $request->get('term');
      
          $result = Product::where('name', 'LIKE', '%'. $search. '%')->get();
      
          return response()->json($result);
            
    }
    public function searchFullText( Request $r)   
    {  
        $search = $r->search;
        if($r->has('start')){
            $data['start'] = str_replace('.', '', $r->start);
            $data['finish'] = str_replace('.', '',  $r->finish);
            $data['string'] = $r->string;
            $data['number'] = $r->number;
            if ($data['string']!= "0") {                         
                        $data['products'] = Product::where('name', 'LIKE', '%'. $search. '%')
                        ->whereBetween('price',[$data['start'],$data['finish']])
                        ->orderBy($data['string'], 'ASC')
                        ->paginate($data['number']);              
            } 
            else {             
                    $data['string'] = 0;
                    $data['number'] = 6;
                    $data['products'] = Product::where('name', 'LIKE', '%'. $search. '%')
                    ->whereBetween('price',[$data['start'],$data['finish']])
                    ->paginate($data['number']);
                }     
            }
        else {
                $data['string'] = 0;
                $data['number'] = 6;
                $data['start'] = 0;
                $data['finish'] = 2000000;
                $data['products'] =  Product::where('name', 'LIKE', '%'. $search. '%')->paginate(6);
            } 
        $data['prd_popular']= Product::where('img','<>','no-img.jpg')->orderBy("quantity_sold", 'DESC')->take(3)->get();
        $data['categories'] = Category::all();
        return view('frontend.search',$data);
    }
    function getCate() {
        $cate = Category::all();
        return view('frontend.master.master',compact('cate'));
    } 
    function getContact() {
        return view('frontend.contact');
    }

    function getAbout() {
        return view('frontend.about');
    }
    function getSubcribe(SubRequest $r){       
        $sub = new Subcribe();
        $sub->email = $r->email;
        $sub->save();
        return redirect()->back()->with('thongbao','Cảm ơn bạn đã đăng kí');
    }
}
