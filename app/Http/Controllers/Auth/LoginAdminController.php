<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginAdminController extends Controller
{
    public function __construct()
    {   $this->middleware('guest',['except' => ['login', 'showLoginForm', 'logout']]);
        $this->middleware('guest:admin');
    }
    public function showLoginForm(){
        return view('auth.admin-login');
    }
    public function login(LoginRequest $r){
      if(Auth::guard('admin')->attempt(['email' => $r->email, 'password' =>$r->password],$r->remember)){
          return redirect()->intended(route('admin.dashboard'));
      }
      return redirect()->back()->withInput($r->only('email','remember'));
    }
}
