<?php

namespace App\Http\Controllers\backend;

use App\Customer;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddCustomerRequest;
use App\Http\Requests\EditCustomerRequest;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    function getCustomer() {
        $data['customers']= Customer::paginate(4);
        return view('backend.customer.listcustomer',$data);
    }

    function getAddCustomer() {
        return view('backend.customer.addcustomer');
    }

    function postAddCustomer(AddCustomerRequest $r) {
        $customer = new Customer;
        $customer->email = $r->email;
        $customer->password = bcrypt($r->password);
        $customer->name = $r->name;
        $customer->address = $r->address;
        $customer->phone = $r->phone;
        $customer->quantity = $r->quantity;
        $customer->save();
        return redirect('/admin/customer')->with('thongbao','Đã thêm thành công');
    }

    function getEditCustomer($idCustomer) {
        $data['customers'] = Customer::find($idCustomer);
        return view('backend.customer.editcustomer',$data);
    }

    function postEditCustomer($idCustomer,EditCustomerRequest $r) {
        $customer = Customer::find($idCustomer);
        $customer->email = $r->email;
        $customer->password = bcrypt($r->password);
        $customer->name = $r->name;
        $customer->address = $r->address;
        $customer->phone = $r->phone;
        $customer->quantity = $r->quantity;
        $customer->save();
        return redirect('/admin/customer')->with('thongbao','Đã sửa thành công');

    }

    function DelCustomer($idCustomer){
        Customer::find($idCustomer)->delete();
        return redirect()->back()->with('thongbao','Đã xóa thành công');
    }
}
