<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddProductRequest;
use App\Http\Requests\EditProductRequest;
use App\Models\Attribute;
use App\Models\Auth;
use App\Models\Category;
use App\Models\Product;
use App\Models\Product_Attribute;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    function getProduct() {
        $data['products'] = Product::paginate(5);
        return view('backend.product.listproduct',$data);
    }
    function getAddProduct() {
        $data['categories'] = Category::all();
        $data['auth'] = Auth::all(); 
        return view('backend.product.addproduct',$data);
    }

     function postAddProduct(AddProductRequest $r) {
     try {
        $prd = new Product();
        $prd->code = strtoupper($r->code);
        $prd->name = $r->name;
        $prd->slug = Str::slug($r->name,'-');
        $prd->price = $r->price;
        $prd->quantity = 0;
        $prd->quantity_sold = 0;
        $prd->auth_id = $r->auth;
        $prd->state = $r->state;
        $prd->info = $r->info;
        $prd->describle = $r->describle;
        if ($r->hasFile('img')) {
            $file = $r->img;
            $fileName = Str::slug($r->name, '-').'.'.$file->getClientOriginalExtension();
            $file->move('backend/img',$fileName);
            $prd->img = $fileName;
        }else{
            $prd->img = 'no-img.jpg';
        }
        $prd->category_id = $r->category;
        $prd->save();
        $attr = Attribute::all();
        foreach ($attr as $value) {
            $nprd = new Product_Attribute();
            $nprd->product_id = $prd->id;
            $nprd->attribute_id = $value->id;
            $nprd->name = $prd->name.' '.$value->size.' '.$value->color;
            $nprd->quantity = 0;
            $nprd->img = 'no-img.jpg';
            $nprd->save();
           }
        return redirect('/admin/product')->with('thongbao','Đã thêm thành công');
       } catch (\Throwable $e) {
        return redirect()->back()->withInput()->with('thongbao','Có lỗi xảy ra vui lòng thử lại!');
       }      
     }

    function getEditProduct($idPrd) {
        $data['prd'] = Product::find($idPrd);
        $data['categories'] = Category::all();
        $data['auth'] = Auth::all(); 
        return view('backend.product.editproduct',$data);
    }

    function postEditProduct($idPrd, EditProductRequest $r){
        try {
        $prd = Product::find($idPrd);
        $prd->code = strtoupper( $r->code);
        $prd->name = $r->name;
        $prd->slug = Str::slug($r->name, '-');
        $prd->price = $r->price;
        $prd->quantity = $r->quantity;
        $prd->quantity_sold = $r->quantity_sold;
        $prd->state = $r->state;
        $prd->info = $r->info;
        $prd->describle = $r->describle;
        $prd->auth_id = $r->auth;
        if ($r->hasFile('img')) {
            if ($prd->img!='no-img.jpg') {
                unlink('backend/img/'.$prd->img);
            }
            $file = $r->img;
            $fileName = Str::slug($r->name, '-').'.'.$file->getClientOriginalExtension();
            $file->move('backend/img',$fileName);
            $prd->img = $fileName;
        }

        $prd->category_id = $r->category;
        $prd->save();
        $attr = Attribute::all();
        foreach ($attr as $value) {
         $nprd = Product_Attribute::where('product_id',$idPrd)->first();
         $nprd->name = $prd->name.' '.$value->size.' '.$value->color;;
         $nprd->save();
        }
        return redirect('/admin/product')->with('thongbao','Đã sửa thành công');
           } catch (\Throwable $e) {
            return redirect()->back()->withInput()->with('thongbao','Có lỗi xảy ra vui lòng thử lại!');
           }
       
    }

    function DelProduct($idPrd){
        Product::find($idPrd)->delete();
        return redirect('/admin/product')->with('thongbao','Đã xóa thành công');

    }
}
