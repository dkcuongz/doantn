<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    function getIndex() {
        $monthNow = Carbon::now()->format('m');
        $yearNow = Carbon::now()->format('Y');
        $data['month'] = $monthNow;
        $data['order'] = Order::where('state',1)->whereMonth('updated_at',$monthNow)->whereYear('updated_at',$yearNow);
        for($i = 1;$i <= $monthNow;$i++){
            $data['total']['Tháng '.$i] = Order::where('state',1)->whereMonth('updated_at',$i)->whereYear('updated_at',$yearNow)->sum('total');
        }
        return view('backend.index' ,$data);
       
    }
}
