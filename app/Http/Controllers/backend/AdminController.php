<?php

namespace App\Http\Controllers\backend;

use App\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\EditUserRequest;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    function getAdminUser() {
        $data['users']= Admin::paginate(4);
        return view('backend.user.listuser',$data);
    }

    function getAddAdminUser() {
        return view('backend.user.adduser');
    }

    function postAddAdminUser(AddUserRequest $r) {
        $user = new Admin;
        $user->email = $r->email;
        $user->password = bcrypt($r->password);
        $user->name = $r->name;
        $user->address = $r->address;
        $user->phone = $r->phone;
        $user->level = $r->level;
        $user->save();
        return redirect('/admin/adminuser')->with('thongbao','Đã thêm thành công');
    }

    function getEditAdminUser($idUser) {
        $data['user'] = Admin::find($idUser);
        return view('backend.user.edituser',$data);
    }

    function postEditAdminUser($idUser,EditUserRequest $r) {
       
        $user = Admin::find($idUser);
        $user->email = $r->email;
        $user->password = bcrypt($r->password);
        $user->name = $r->name;
        $user->address = $r->address;
        $user->phone = $r->phone;
        $user->level = $r->level;
        $user->save();
        return redirect('/admin/adminuser')->with('thongbao','Đã sửa thành công');

    }

    function DelAdminUser($idUser){
        Admin::find($idUser)->delete();
        return redirect()->back()->with('thongbao','Đã xóa thành công');
    }
}
