<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddAuthRequest;
use App\Http\Requests\EditAuthRequest;
use App\Models\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    function getAuth() {
        $data['auth'] = Auth::all();
        return view('backend.auth.auth',$data);
    }
    function postAddAuth( AddAuthRequest $r){
        $auth = new Auth;
        $auth->name = $r->name;
        $auth->save();
        return redirect()->back()->with('thongbao','Đã thêm thành công');
    }

    function getEditAuth($idAuth) {
        $data['authall'] = Auth::all();
        $data['auth'] = Auth::find($idAuth);
        return view('backend.auth.editauth',$data);
    }

    function postEditAuth( $idAuth ,EditAuthRequest $r){
        $auth = Auth::find($idAuth);
        $auth->name = $r->name;
        $auth->save();
        return redirect()->back()->with('thongbao','Đã sửa thành công');
    }

    function DelAuth($idCate){
        $cate = Auth::find($idCate);
        $cate->delete();
        return redirect('/admin/auth')->with('thongbao','Đã xóa thành công');
    }  
}
