<?php

namespace App\Http\Controllers\backend;

use App\Models\Subcribe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use App\Mail\NotiEmail;
class SubcribeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    function getSubcribeAdmin(){       
        return view('backend.subcribe.subcribe');
    }
    function sendMail(Request $r){       
        $Sub = Subcribe::all();
        foreach ($Sub as $value) {
           Mail::to($value->email)->send(new NotiEmail($r->data,$r->url));
        }
        return redirect()->back()->with('thongbao','Đã gửi thành công');
    }
}
