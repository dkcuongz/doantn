<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Order;
use App\Models\Order_Detail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use PDF;
use App\Mail\InvoiceMail;
use App\Mail\RefuseEmail;
use App\Models\Product_Attribute;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    function getOrder() {
        $data['order'] = Order::where('state',0)->orderBy('updated_at','desc')->get();
        return view('backend.order.order',$data);
    }
    function getDetail($idOrder) {
        $data['order'] = Order::find($idOrder);      
        return view('backend.order.detailorder',$data);
    }
    function getProcessed() {
        $data['order'] = Order::where('state',1)->orderBy('updated_at','desc')->get();
        return view('backend.order.orderprocessed',$data);
    }
    function back() {
        return redirect()->back();
    }
    function getRefuseOrder() {
        $data['order'] = Order::where('state',2)->orderBy('updated_at','desc')->get();
        return view('backend.order.refuseOrder',$data);
    }
    function getSuccessOrder() {
        $data['order'] = Order::where('state',3)->orderBy('updated_at','desc')->get();
        return view('backend.order.successOrder',$data);
    }
    function getProcess($idOrder){
        $order = Order::find($idOrder);
        $order->state = 1;
        $order->admin_id = Auth::user()->id;
        $detail = Order_Detail::where('order_id',$order->id)->get();
        Mail::to($order->email)->send(new InvoiceMail($order,$detail));
        $order->save();
        return redirect('/admin/order');
    }
    function getRefuse($idOrder){
        $order = Order::find($idOrder);
        $order->state = 2;
        $order->admin_id = Auth::user()->id;
        $detail = Order_Detail::where('order_id',$order->id)->get();
        Mail::to($order->email)->send(new RefuseEmail($order,$detail));
        $order->save();
        $detail = Order_Detail::where('order_id',$idOrder)->get();
        foreach ($detail as $value) {
           $prd_att = Product_Attribute::where('id',$value->product_attribute_id)->first();
           Product::where('id',$prd_att->prd_attr_prd->id)->update(['quantity'=>$prd_att->prd_attr_prd->quantity + $value->qty,
           'quantity_sold'=>$prd_att->prd_attr_prd->quantity_sold - $value->qty]);
           $prd_att->quantity = $prd_att->quantity + $value->quantity;
           $prd_att->save();
        }
        return redirect('/admin/order');
    }
    public function getPdf($idOrder)
    {   $order = Order::find($idOrder);	
        $detail = Order_Detail::where('order_id',$idOrder)->get();
    	$pdf = PDF::loadView('emails.user.invoice',  compact('order','detail'));
    	return $pdf->download('Invoice'.$order->name.'.pdf');
    }
}
