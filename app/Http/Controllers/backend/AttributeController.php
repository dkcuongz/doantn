<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AttributeRequest;
use App\Models\Attribute;
use App\Models\Product;
use App\Models\Product_Attribute;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    function getAttribute() {
        $data['attribute'] = Attribute::paginate(4);
        return view('backend.attribute.attr',$data);
    }
    function getAddAttribute(){
        return view('backend.attribute.addattr');
    }
    function postAddAttribute( AttributeRequest $r){
            $getattr = Attribute::where('size',trim(strtoupper($r->size)))->where('color',trim(ucwords($r->color)))->first();
            if($getattr){
                return redirect()->back()->with('thongbao','Thuộc tính đã tồn tại');
            }
            else{
            $attr = new Attribute();
            $attr->size = trim(strtoupper($r->size));
            $attr->color = trim(ucwords($r->color));
            $attr->description = $r->description;
            $attr->save();
            $prd = Product::all();
            foreach ($prd as $value) {
             $nprd = new Product_Attribute();
             $nprd->product_id = $value->id;
             $nprd->attribute_id = $attr->id;
             $nprd->name = $value->name.' '.trim(strtoupper($r->size)).' '.trim(ucwords($r->color));
             $nprd->quantity = 0;
             $nprd->img = 'no-img.jpg';
             $nprd->save();
            }
            return redirect('/admin/attribute')->with('thongbao','Đã thêm thành công');
            }
    }

    function getEditAttribute($idAttr) {
        $data['attr'] = Attribute::find($idAttr);
        return view('backend.attribute.editattr',$data);
    }

    function postEditAttribute( $idAttr ,AttributeRequest $r){
        $getattr = Attribute::where('size',strtoupper($r->size))->where('color',ucwords($r->color))->first();
        if($getattr){
            return redirect()->back()->with('thongbao','Thuộc tính đã tồn tại');
        }
        else{
        $attr = Attribute::find($idAttr);
        $attr->size = trim(strtoupper($r->size));
        $attr->color = trim(ucwords($r->color));
        $attr->description = $r->description;
        $attr->save();
        $prd = Product::all();
        foreach ($prd as $value) {
         $nprd = Product_Attribute::where('attribute_id',$idAttr)->first();
         $nprd->name = $value->name.' '.trim(strtoupper($r->size)).' '.trim(ucwords($r->color));
         $nprd->save();
        }
        return redirect('/admin/attribute')->with('thongbao','Đã sửa thành công');
        }
    }

    function DelAttribute($idAttr){
        $attr = Attribute::find($idAttr);
        $attr->delete();
        return redirect('/admin/attribute')->with('thongbao','Đã xóa thành công');
    }  
}
