<?php

namespace App\Http\Controllers\backend;

use App\Models\Coupon;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddCouponRequest;
use App\Http\Requests\EditCouponRequest;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    function getCoupon() {
        $data['coupons']= Coupon::paginate(4);
        return view('backend.coupon.listcoupon',$data);
    }

    function getAddCoupon() {
        return view('backend.coupon.addcoupon');
    }

    function postAddCoupon(AddCouponRequest $r) {
       try {
        $Coupon = new Coupon();
        $Coupon->code = trim(strtoupper($r->code));
        $Coupon->name = $r->name;
        $Coupon->description = $r->description;
        $Coupon->available = $r->max_uses;
        $Coupon->max_uses = $r->max_uses;
        $Coupon->type = $r->type;
        $Coupon->discount_amount = $r->discount_amount;
        $Coupon->starts_at = $r->starts_at;
        $Coupon->expires_at = $r->expires_at;       
        $Coupon->save();
        return redirect('/admin/coupon')->with('thongbao','Đã thêm thành công');
       } catch (\Throwable $e) {
        return redirect()->back()->withInput();
       }
       
    }

    function getEditCoupon($idCoupon) {
        $data['Coupon'] = Coupon::find($idCoupon);
        //dd(  $data['Coupon'] );
        return view('backend.coupon.editcoupon',$data);
    }

    function postEditCoupon($idCoupon,EditCouponRequest $r) {
        $Coupon = Coupon::find($idCoupon);
        $Coupon->code = $r->code;
        $Coupon->name = $r->name;
        $Coupon->description = $r->description;
        $Coupon->uses = $r->uses;
        $Coupon->max_uses = $r->max_uses;
        $Coupon->type = $r->type;
        $Coupon->discount_amount = $r->discount_amount;
        $Coupon->starts_at = $r->starts_at;
        $Coupon->expires_at = $r->expires_at;       
        $Coupon->save();
        return redirect('/admin/coupon')->with('thongbao','Đã sửa thành công');

    }

    function DelCoupon($idCoupon){
        Coupon::find($idCoupon)->delete();
        return redirect()->back()->with('thongbao','Đã xóa thành công');
    }
}
