<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddProductAttributeRequest;
use App\Models\Attribute;
use App\Models\Product;
use App\Models\Product_Attribute;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class Prd_AttributeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    function getPrd_Attribute($idPrd) {
        $data['product'] = Product::find($idPrd);
        $data['prd_attr'] = Product_Attribute::where('product_id',$idPrd)->orderby('quantity','desc')->paginate(3);
        return view('backend.prd_attribute.listattr',$data);
    }
    function getAddPrd_Attribute($idPrd) {
        $data['product'] = Product::find($idPrd);
        return view('backend.prd_attribute.addprd_attribute',$data);
    }
    function postAddPrd_Attribute($idPrd,AddProductAttributeRequest $r) {
        $product = Product::find($idPrd);
        $attr_id = Attribute::where('color',$r->color)->where('size',$r->size)->first();
        if ($attr_id) {
            $prd = Product_Attribute::where('attribute_id',$attr_id->id)->where('product_id',$idPrd)->first();
            if ($prd) {
            $prd->quantity = $prd->quantity + $r->quantity;
            $prd->save();
            }
            else{
            $prd = new Product_Attribute();
            $prd->product_id = $idPrd;
            $prd->attribute_id = $attr_id->id;
            $prd->quantity = $r->quantity;
            if ($r->hasFile('img')) {
            $file = $r->img;
            $fileName = $product->slug.'-'.Str::slug($r->color, '-').Str::slug($r->size).'.'.$file->getClientOriginalExtension();
            $file->move('backend/img',$fileName);
            $prd->img = $fileName;
            }else{
            $prd->img = 'no-img.jpg';
             }
             $prd->save();
            }
            return redirect('/admin/prd_attribute/'.$idPrd.'')->with('thongbao','Đã thêm thành công');
        }
        else{
            $attr = new Attribute();
            $attr->size = trim(strtoupper($r->size));
            $attr->color = trim(ucwords($r->color));
            $attr->description = $r->description;
            $attr->save();
            $prd = new Product_Attribute();
            $prd->product_id = $idPrd;
            $prd->quantity = $r->quantity;
            $prd->attribute_id = $attr->id;
            if ($r->hasFile('img')) {
            $file = $r->img;
            $fileName = $product->slug.'-'.Str::slug($r->color, '-').Str::slug($r->size).'.'.$file->getClientOriginalExtension();
            $file->move('backend/img',$fileName);
            $prd->img = $fileName;

            }else{
            $prd->img = 'no-img.jpg';
            }
            $prd->save();
            return redirect('/admin/prd_attribute/'.$idPrd.'')->with('thongbao','Đã thêm thành công');
        }
    }
    function getEditPrd_Attribute($idPrd,$idPrd_Attr) {
        $data['product'] = Product::find($idPrd);
        $data['prd_attr'] = Product_Attribute::find($idPrd_Attr);
        return view('backend.prd_attribute.editprd_attribute',$data);
    }
    function postEditPrd_Attribute($idPrd,$idPrd_Attr, AddProductAttributeRequest $r){
        $product = Product::where('id',$idPrd)->get();
        $prd = Product_Attribute::find($idPrd_Attr);
        $prd->product_id = $idPrd;
        if ($r->hasFile('img')) {
            $file = $r->img;
            $fileName = $product->slug.'-'.Str::slug($r->color, '-').Str::slug($r->size).'.'.$file->getClientOriginalExtension();
            $file->move('backend/img',$fileName);
            $prd->img = $fileName;

        }else{
            $prd->img = 'no-img.jpg';
        }
        $attr_id = Attribute::where('color',$r->color)->where('size',$r->size)->first();
        if ($attr_id) {
            if ($attr_id->id != $idPrd_Attr) {
                $prd_s = Product_Attribute::where('attribute_id',$attr_id->id)->where('product_id',$idPrd)->first();
                if($prd_s){
                $prd_s->quantity = $prd_s->quantity + $r->quantity;
                $prd_s->save();
                $prd->delete();
                }
                else{
                $prd->quantity = $r->quantity;
                $prd->save();
                }
                return redirect('/admin/prd_attribute/'.$idPrd.'')->with('thongbao','Đã sửa thành công');
            }
            elseif($attr_id->id == $idPrd_Attr){
               $prd->quantity = $r->quantity;
               $prd->save();
               return redirect('/admin/prd_attribute/'.$idPrd.'')->with('thongbao','Đã sửa thành công');
            }
        }
        else{
            $attr = new Attribute();
            $attr->size = trim(strtoupper($r->size));
            $attr->color = trim(ucwords($r->color));
            $attr->description = $r->description;
            $attr->save();
            $prd->quantity = $r->quantity;
            $prd->attribute_id = $attr->id;
            $prd->save();
            return redirect('/admin/prd_attribute/'.$idPrd.'')->with('thongbao','Đã thêm thành công');
        }
}
        function DelPrd_Attribute($idPrd,$idPrd_Attr){
            Product_Attribute::find($idPrd_Attr)->delete();
            return redirect('/admin/prd_attribute/'.$idPrd.'')->with('thongbao','Đã xóa thành công');

        }
}