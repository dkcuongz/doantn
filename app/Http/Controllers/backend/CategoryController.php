<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddCategoryRequest;
use App\Http\Requests\EditCategoryRequest;
use App\Models\Category;
use Illuminate\Support\Str;
use Psy\Util\Str as PsyStr;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    function getCategory() {
        $data['categories'] = Category::all();
        return view('backend.category.category',$data);
    }
    function postAddCategory(AddCategoryRequest $r){

        $cate = new Category;
        $cate->name = $r->name;
        $cate->slug = Str::slug($r->name, '-');
        $cate->category_parent = $r->parent;
        $cate->save();
        return redirect()->back()->with('thongbao','Đã thêm thành công');


    }

    function getEditCategory($idCate) {
        $data['categories'] = Category::all();
        $data['cate'] = Category::find($idCate);
        $data['cate_parent'] = Category::find($data['cate']->category_parent);
        return view('backend.category.editcategory',$data);
    }

    function postEditCategory($idCate,EditCategoryRequest $r){
        $cate = Category::find($idCate);
        $cate->name = $r->name;
        $cate->slug = PsyStr::slug($r->name, '-');
        $cate->category_parent = $r->parent;
        $cate->save();
        return redirect()->back()->with('thongbao','Đã sửa thành công');
    }

    function DelCategory($idCate){
        $cate = Category::find($idCate);
        // if(!Category::where('category_parent',$idCate)){
        Category::where('category_parent',$idCate)->update(['category_parent'=>$cate->category_parent]);
        $cate->delete();
        return redirect('/admin/category')->with('thongbao','Đã xóa thành công');
    }  
}
