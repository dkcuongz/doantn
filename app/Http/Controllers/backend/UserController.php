<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddCustomerRequest;
use App\Http\Requests\EditCustomerRequest;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    function getUser() {
        $data['customers']= User::paginate(4);
        return view('backend.customer.listcustomer',$data);
    }

    function getAddUser() {
        return view('backend.customer.addcustomer');
    }

    function postAddUser(AddCustomerRequest $r) {
        $user = new User;
        $user->email = $r->email;
        $user->password = bcrypt($r->password);
        $user->name = $r->name;
        $user->address = $r->address;
        $user->phone = $r->phone;
        $user->quantity = 0;
        $user->save();
        return redirect('/admin/user')->with('thongbao','Đã thêm thành công');
    }

    function getEditUser($idUser) {
        $data['customers'] = User::find($idUser);
        return view('backend.customer.editcustomer',$data);
    }

    function postEditUser($idUser,EditCustomerRequest $r) {
       
        $user = User::find($idUser);
        $user->email = $r->email;
        $user->password = bcrypt($r->password);
        $user->name = $r->name;
        $user->address = $r->address;
        $user->phone = $r->phone;
        $user->quantity = $r->quantity;
        $user->save();
        return redirect('/admin/user')->with('thongbao','Đã sửa thành công');

    }

    function DelUser($idUser){
        User::find($idUser)->delete();
        return redirect()->back()->with('thongbao','Đã xóa thành công');
    }
}
