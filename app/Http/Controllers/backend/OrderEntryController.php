<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\Auth;
use App\Models\Order_Entry;
use App\Models\Order_Entry_Detail;
use App\Models\Product;
use App\Models\Product_Attribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as LoginAuth;

class OrderEntryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    function getOrder() {
        $data['order_entry'] = Order_Entry::orderBy('updated_at','desc')->get();
        return view('backend.orderentry.orderprocessed',$data);
    }
    function getDetail($idOrder_et) {
        $data['order'] = Order_Entry::where('id',$idOrder_et)->first();
        $data['order_entry'] = Order_Entry_Detail::where('order_entry_id',$idOrder_et)->get();
        return view('backend.orderentry.detailorderentry',$data);
    }
    function getAddOrder() {
        $data['attr'] = Attribute::all();
        $data['auth'] = Auth::all();
        $data['product'] = Product::all();
        return view('backend.orderentry.addorderentry',$data);
    }
    function postAddOrder(Request $r) {
        $order = new Order_Entry();
        $order->auth_id = $r->auth;
        $order->state = "1";
        $order->admin_id = LoginAuth::user()->id;
        $total = 0;
        $order->total = $total; 
        $order->save(); 
        foreach((array)$r->product as $key =>$item){
            $prd_attr = Product_Attribute::where('product_id',$item)->where('attribute_id',$r->attribute[$key])->first();
            $prd_attr->quantity = $r->quantity[$key];
            $prd_attr->save();
            $prd = Product::where('id',$r->product)->first();
            $prd->quantity += $r->quantity[$key];
            $prd->save();
            $total += $r->quantity[$key] *$r->price[$key];
            $order_dt = new Order_Entry_Detail(); 
            $order_dt->order_entry_id = $order->id;
            $order_dt->price = $r->price[$key];
            $order_dt->product_attribute_id = $prd_attr->id;
            $order_dt->quantity = $r->quantity[$key];
            if ($r->hasFile('img')) {
                $file = $r->img;
                $file->move('backend/img');
                $order_dt->img = $file;
            }else{
                $order_dt->img = 'no-img.jpg';
            }
            $order_dt->save();
        }
        $order->total = $total;
        $order->save(); 
        return redirect('/admin/order_et')->with('thongbao','Đã thêm thành công');
        
    }
    function getEditOrder($idOrder) {
        $data['attr'] = Attribute::all();
        $data['auth'] = Auth::all();
        $data['order'] = Order_Entry::where('id',$idOrder)->first();
        $data['order_dt'] = Order_Entry_Detail::where('order_entry_id',$data['order']->id)->get();
        $data['product'] = Product::all();
        return view('backend.orderentry.editorderentry',$data);
    }
    function postEditOrder(Request $r,$idOrder) {
        $order =  Order_Entry::where('id',$idOrder)->first();
        $order_dt = Order_Entry_Detail::where('id',$order->id)->get();
        foreach ($order_dt as  $value) {
        $prd_attr = Product_Attribute::where('id',$value->product_attribute_id)->first();
        $prd_attr->quantity = $prd_attr->quantity - $value->quantity ;
        $prd_attr->save();
        $prd = Product::where('id',$prd_attr->prd_attr_prd->id)->first();
        $total = $order->total - $value->quantity* $value->price ;
        $prd->quantity = $prd->quantity - $value->quantity ;
        $prd->save();
        $prd_attr_edit = Product_Attribute::where('product_id',$r->product[$value->id])->where('attribute_id',$r->attribute[$value->id])->first();
        $prd_attr_edit->quantity += $r->quantity[$value->id];
        $prd_attr_edit->save();
        $prd_edit = Product::where('id',$r->product[$value->id])->first();;
        $prd_edit->quantity += $r->quantity[$value->id];
        $prd_edit->save();
        $value->product_attribute_id = $prd_attr_edit->id;
        $value->price = $r->price[$value->id];
        $value->quantity = $r->quantity[$value->id];
        $total += $r->price[$value->id]*$r->quantity[$value->id];
        $order_dt->quantity = $r->quantity;
            if ($r->hasFile('img')) {
                $file = $r->img[$value->id];
                $file->move('backend/img');
                $value->img = $file;
            }else{
                $value->img = 'no-img.jpg';
            }
        $value->save();
        }
        $order->total = $total;
        $order->save(); 
        return redirect('/admin/order_et')->with('thongbao','Đã thêm thành công');
        
    }
    function DelOrder($idOrder_et){
        $order = Order_Entry::find($idOrder_et);
        $order_dt = $order->entry_dt;
        foreach ($order_dt as $value) {
        $prd_att = Product_Attribute::where('id',$value->product_attribute_id)->first();
        $prd_att->quantity-= $value->quantity;
        $prd = $prd_att->prd_attr_prd;
        $prd->quantity -= $value->quantity;
        $prd_att->save();
        $prd->save();
        }
        $order->delete();
        return redirect('/admin/order_et')->with('thongbao','Đã xoá thành công');
    }
}
