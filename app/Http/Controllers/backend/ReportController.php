<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Order_Entry;
use App\Models\Product;
use Illuminate\Http\Request;
use PDF;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    function getReport(){
        $data['month'] =  date('m');
        $data['year'] = date('Y');
        $data['order'] = Order::whereraw('MONTH(created_at)=?',$data['month'])->whereraw('YEAR(created_at)=?',$data['year'])->whereNotIn('state', [0,2])->get();
        $data['total_order'] = $data['order']->sum('total');
        $data['order_entry'] = Order_Entry::whereraw('MONTH(created_at)=?',$data['month'])->whereraw('YEAR(created_at)=?',$data['year'])->get();
        $data['total_order_entry'] = $data['order_entry']->sum('total');
        return view("backend.report.report",$data);
    }
    function postReport(Request $r){
        $data['month'] =  $r->month;
        $data['year'] = date('Y');
        if($r->day!=""){
        $data['day'] = $r->day;
        $data['order'] = Order::whereraw('DAY(created_at)=?',$data['day'])->whereraw('MONTH(created_at)=?',$data['month'])
        ->whereraw('YEAR(created_at)=?',$data['year'])->whereNotIn('state', [0,2])->get();
        $data['order_entry'] = Order_Entry::whereraw('DAY(created_at)=?',$data['day'])->whereraw('MONTH(created_at)=?',$data['month'])
        ->whereraw('YEAR(created_at)=?',$data['year'])->get();    
        }
        else{
            $data['order'] = Order::whereraw('MONTH(created_at)=?',$data['month'])
            ->whereraw('YEAR(created_at)=?',$data['year'])->whereNotIn('state', [0,2])->get();
            $data['order_entry'] = Order_Entry::whereraw('MONTH(created_at)=?',$data['month'])
            ->whereraw('YEAR(created_at)=?',$data['year'])->get();
        }
        $data['total_order_entry'] = $data['order_entry']->sum('total');
        $data['total_order'] = $data['order']->sum('total');
        return view("backend.report.report",$data);
    }
    public function getPdfReport1($month,$year)
    {  
        $order = Order::whereraw('MONTH(created_at)=?',$month)->whereraw('YEAR(created_at)=?',$year)->whereNotIn('state', [0,2])->get();
        $total_order = $order->sum('total');
        $order_entry = Order_Entry::whereraw('MONTH(created_at)=?',$month)->whereraw('YEAR(created_at)=?',$year)->get();
        $total_order_entry = $order_entry->sum('total');
    	$pdf = PDF::loadView('backend.report.reportPDF',  compact('order','total_order','order_entry','total_order_entry','month','year'));
    	return $pdf->download('Report'.$month.'-'.$year.'.pdf');
    }
    public function getPdfReport2($day,$month,$year)
    {   
        $order = Order::whereraw('DAY(created_at)=?',$day)->whereraw('MONTH(created_at)=?',$month)
        ->whereraw('YEAR(created_at)=?',$year)->whereNotIn('state', [0,2])->get();
        $order_entry = Order_Entry::whereraw('DAY(created_at)=?',$day)->whereraw('MONTH(created_at)=?',$month)
        ->whereraw('YEAR(created_at)=?',$year)->get();
        $total_order_entry = $order_entry->sum('total');
        $total_order = $order->sum('total');
    	$pdf = PDF::loadView('backend.report.reportPDF',  compact('order','total_order','order_entry','total_order_entry','month','day','year'));
    	return $pdf->download('Report'.$month.'-'.$year.'.pdf');
    }
    // function getReportProduct(){
    //     $data['month'] =  date('m');
    //     $data['year'] = date('Y');
    //     $data['prd'] = Product::all();
    //     //$data['total_order'] = $data['order']->sum('total');
    //     //dd($data['total_order']);
    //     // $data['order_entry'] = Order_Entry::whereraw('MONTH(created_at)=?',$data['month'])->whereraw('YEAR(created_at)=?',$data['year'])->get();
    //     //$data['total_order_entry'] = $data['order_entry']->sum('total');
    //     return view("backend.report.reportproduct",$data);
    // }
    // function postReportProduct(Request $r){
    //     $data['month'] =  $r->month;
    //     $data['year'] = date('Y');
    //     if($r->day!=""){
    //     $data['day'] = $r->day;
    //     $data['order'] = Order::whereraw('DAY(created_at)=?',$data['day'])->whereraw('MONTH(created_at)=?',$data['month'])
    //     ->whereraw('YEAR(created_at)=?',$data['year'])->whereNotIn('state', [0,2])->get();
    //     //dd($data['order']);
    //     $data['order_entry'] = Order_Entry::whereraw('DAY(created_at)=?',$data['day'])->whereraw('MONTH(created_at)=?',$data['month'])
    //     ->whereraw('YEAR(created_at)=?',$data['year'])->get();    
    //     }
    //     else{
    //         $data['order'] = Order::whereraw('MONTH(created_at)=?',$data['month'])
    //         ->whereraw('YEAR(created_at)=?',$data['year'])->whereNotIn('state', [0,2])->get();
    //         //dd($data['total_order']);
    //         $data['order_entry'] = Order_Entry::whereraw('MONTH(created_at)=?',$data['month'])
    //         ->whereraw('YEAR(created_at)=?',$data['year'])->get();
    //     }
    //     $data['total_order_entry'] = $data['order_entry']->sum('total');
    //     $data['total_order'] = $data['order']->sum('total');
    //     return view("backend.report.reportproduct",$data);
    // }
}
