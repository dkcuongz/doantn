<?php
 namespace App\Http\ViewComposers;

use App\Models\Category;
use Illuminate\View\View;

 class CategoryComposer
 {
     public $cateList = [];
     /**
      * Create a cate composer.
      *
      * @return void
      */
     public function __construct()
     {
         $this->cateList = Category::all();
     }

     /**
      * Bind data to the view.
      *
      * @param  View  $view
      * @return void
      */
     public function compose(View $view)
     {
         $view->with('cate', end($this->cateList));
     }
 }