<?php

function showErrors($errors,$name){
    if ($errors->has($name)){
        echo '<div class="alert alert-danger" role="alert">';
        echo '<strong>'.$errors->first($name).'</strong>';
        echo '</div>';
    }
}

function getCate($cate,$parent,$tab,$idSelect){

	foreach($cate as $value){
		if($value['category_parent']==$parent){
            if ($value['id']==$idSelect) {
                echo '<option selected value="'.$value['id'].'">'.$tab.$value['name'].'</option>';
            }else{
                echo '<option value="'.$value['id'].'">'.$tab.$value['name'].'</option>';
            }

		    getCate($cate,$value['id'],$tab.'--|',$idSelect);
		}
	}
}


function showCate($cate,$parent,$tab){

	foreach($cate as $value){
		if($value['category_parent']==$parent){
            echo '<div class="item-menu"><span>'.$tab.$value['name'].'</span>
                <div class="category-fix">
                <a class="btn-category btn-primary" href="/admin/category/edit/'.$value['id'].'"><i class="fa fa-edit"></i></a>
                <a onclick="return del()" class="btn-category btn-danger" href="/admin/category/del/'.$value['id'].'"><i class="fas fa-times"></i></i></a>
                </div>
                </div>';


		    showCate($cate,$value['id'],$tab.'--|');
		}
	}
}
function showCate_2($cate,$parent,$tab){
	foreach($cate as $value){
		if($value['category_parent']==$parent){
            echo '<li><a href="/product/'.$value['slug'].'.html">'.$tab.$value['name'].'</a></li>';
		    showCate_2($cate,$value['id'],$tab.'--|');
		}
	}
}
function showMenu($cate,$parent,$tab,$endtab){

	foreach($cate as $value){
		if($value['category_parent'] == $parent){
            echo '
                <li><a href="/product/'.$value['slug'].'.html">'.$value['name'];
                if($value->getParentCategory!=null){
                  echo '<span class = "caret"></span>
                  ';}
                  echo '</a>';
                if($value->getParentCategory){
                echo '
                <ul class="dropdown-menu">';}
                showMenu($cate,$value['id'],$tab,$endtab);        
                if($value->getParentCategory){
                  echo '
                  </ul>';}
              echo'</li>';                            
    }
	}
}