<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
//FRONTEND
Auth::routes(['verify'=> true]);
Route::post('/test', 'frontend\HomeController@test');

Route::get('subcribe', 'frontend\HomeController@getSubcribe')->name('getSubcribe');
Route::get('', 'frontend\HomeController@getIndex');
Route::get('contact', 'frontend\HomeController@getContact');
Route::get('about', 'frontend\HomeController@getAbout');
Route::get('account','frontend\LoginController@getLogin');
Route::get('autocomplete', 'frontend\HomeController@search');
Route::get('/search', 'frontend\HomeController@searchFullText')->name('search');
//cart
Route::group(['prefix' => 'cart'], function () {
    Route::get('', 'frontend\CartController@getCart')->name('getcart');
    Route::get('add', 'frontend\CartController@addCart')->name('addcart');
    Route::get('add/{id}/{addlink}', 'frontend\CartController@addCart');
    Route::get('del/{rowId}', 'frontend\CartController@delCart');
    Route::get('edit/{rowId}/{qty}', 'frontend\CartController@updateCart');
    //coupon
    Route::post('coupon', 'frontend\CartController@postCoupon')->name('postCoupon');
  
});
Route::group(['prefix' => 'user'], function () {
    Route::get('/profile/{idUser}', 'frontend\UserController@getProfile')->name('getProfile');
    Route::post('/post/{idUser}', 'frontend\UserController@postProfile')->name('postProfile');
    Route::get('/order/{idUser}', 'frontend\UserController@getUserorder')->name('getUserorder');
    Route::get('{idUser}/order/refuse/{idOrder}', 'frontend\UserController@refuseOrder')->name('refuseOrder');
    Route::get('{idUser}/order/detail/{idOrder}', 'frontend\UserController@getOrderDetail')->name('getOrderDetail');
    Route::get('{idUser}/order/pdf/{idOrder}','frontend\UserController@getOrderPdf')->name('getOrderPdf');
  
});
//checkout
Route::group(['prefix' => 'checkout'], function () {
    Route::get('', 'frontend\CheckoutController@getCheckout')->name('getcheckout');
    Route::post('', 'frontend\CheckoutController@postCheckout')->name('checkout');
    Route::get('complete/{idOrder}', 'frontend\CheckoutController@getComplete');
});
//product
Route::group(['prefix' => 'product'], function () {
    Route::get('{slug_cate}.html','frontend\ProductController@getPrdCate' )->name('getPrdbyCate');
    Route::get('/detail/{slug_prd}', 'frontend\ProductController@getDetail');
    Route::get('/detail/{slug_prd}/{idPrd}', 'frontend\ProductController@getDetailPrdAttr')->name('prdDetail');
    Route::get('shop','frontend\ProductController@getShop' )->name('getShop');
    Route::post('dataModal','frontend\ProductController@getDataModal' )->name('getDataModal');
    Route::post('getDataAddcart','frontend\ProductController@getDataAddcart' )->name('getDataAddcart');
    //Route::get('/filter', 'frontend\ProductController@getFilterGiang')->name('filter');
    //Route::post('/filter/price', 'frontend\ProductController@getFilter')->name('filter');
    //Route::get('/filter/price/{slug_cate}', 'frontend\ProductController@getFilterShopCate')->name('filterCate');
});
//------------------------
//BACKEND
//login
Route::get('adminlogin', 'Auth\LoginAdminController@showLoginForm')->name('admin.login')->middleware('CheckLogout');
Route::post('adminlogin', 'Auth\LoginAdminController@login')->name('admin.login.submit');
Route::get('adminlogout', 'backend\LoginController@getLogout');

Route::group(['prefix' => 'admin','middleware'=>'CheckLogin'], function () {
    //admin
    Route::get('', 'backend\HomeController@getIndex')->name('admin.dashboard');
    //category
    Route::group(['prefix' => 'category'], function () {
        Route::get('', 'backend\CategoryController@getCategory');
        Route::post('', 'backend\CategoryController@postAddCategory');
        Route::get('edit/{idCate}','backend\CategoryController@getEditCategory');
        Route::post('edit/{idCate}','backend\CategoryController@postEditCategory');
        Route::get('del/{idCate}','backend\CategoryController@DelCategory');
    });
    //auth
    Route::group(['prefix' => 'auth'], function () {
        Route::get('', 'backend\AuthController@getAuth');
        Route::post('', 'backend\AuthController@postAddAuth');
        Route::get('edit/{idAuth}','backend\AuthController@getEditAuth');
        Route::post('edit/{idAuth}','backend\AuthController@postEditAuth');
        Route::get('del/{idAuth}','backend\AuthController@DelAuth');
    });
    //attribute
    Route::group(['prefix' => 'attribute'], function () {
        Route::get('', 'backend\AttributeController@getAttribute');
        Route::get('add', 'backend\AttributeController@getAddAttribute');
        Route::post('add', 'backend\AttributeController@postAddAttribute');
        Route::get('edit/{idAttr}','backend\AttributeController@getEditAttribute');
        Route::post('edit/{idAttr}','backend\AttributeController@postEditAttribute');
        Route::get('del/{idAttr}','backend\AttributeController@DelAttribute');
    });
    //order
    Route::group(['prefix' => 'order'], function () {
        Route::get('', 'backend\OrderController@getOrder');
        Route::get('detail/{idOrder}', 'backend\OrderController@getDetail');
        Route::get('processed', 'backend\OrderController@getProcessed');
        Route::get('success', 'backend\OrderController@getSuccessOrder');
        Route::get('refuse', 'backend\OrderController@getRefuseOrder');
        Route::get('back', 'backend\OrderController@back')->name('back');
        Route::get('process/{idOrder}', 'backend\OrderController@getProcess');
        Route::get('refuse/{idOrder}', 'backend\OrderController@getRefuse');
        Route::get('pdf/{idOrder}','backend\OrderController@getPdf')->name('getPdf');
    });
    //order entry
    Route::group(['prefix' => 'order_et'], function () {
        Route::get('', 'backend\OrderEntryController@getOrder');
        Route::get('detail/{idOrder_et}', 'backend\OrderEntryController@getDetail');
        Route::get('add', 'backend\OrderEntryController@getAddOrder');
        Route::post('add', 'backend\OrderEntryController@postAddOrder');
        Route::get('{idOrder}/edit','backend\OrderEntryController@getEditOrder');
        Route::post('{idOrder}/edit','backend\OrderEntryController@postEditOrder');
        //Route::get('processed', 'backend\OrderEntryController@getProcessed');
        Route::get('del/{idOrder_et}', 'backend\OrderEntryController@DelOrder');

    });
    //product
    Route::group(['prefix' => 'product'], function () {
        Route::get('', 'backend\ProductController@getProduct');
        Route::get('add', 'backend\ProductController@getAddProduct');
        Route::post('add', 'backend\ProductController@postAddProduct');
        Route::get('edit/{idPrd}','backend\ProductController@getEditProduct' );
        Route::post('edit/{idPrd}','backend\ProductController@postEditProduct' );
        Route::get('del/{idPrd}','backend\ProductController@DelProduct' );
    });
    //report
    Route::group(['prefix' => 'report'], function () {
        Route::get('', 'backend\ReportController@getReport');
        Route::post('', 'backend\ReportController@postReport')->name('postReport');
        Route::get('/product', 'backend\ReportController@getReportProduct');
        Route::post('/product', 'backend\ReportController@postReportProduct')->name('postReportProduct');
        Route::get('pdf/{month}/{year}','backend\ReportController@getPdfReport1')->name('getPdfReport1');
        Route::get('pdf/{day}/{month}/{year}','backend\ReportController@getPdfReport2')->name('getPdfReport2');
    });
    //product_attribute
    Route::group(['prefix' => 'prd_attribute'], function () {
        Route::get('{idPrd}', 'backend\Prd_AttributeController@getPrd_Attribute');
        Route::get('{idPrd}/add', 'backend\Prd_AttributeController@getAddPrd_Attribute');
        Route::post('{idPrd}/add', 'backend\Prd_AttributeController@postAddPrd_Attribute');
        Route::get('{idPrd}/edit/{idPrd_Attr}','backend\Prd_AttributeController@getEditPrd_Attribute' );
        Route::post('{idPrd}/edit/{idPrd_Attr}','backend\Prd_AttributeController@postEditPrd_Attribute' );
        Route::get('{idPrd}/del/{idPrd_Attr}','backend\Prd_AttributeController@DelPrd_Attribute' );
    });

    //user
    Route::group(['prefix' => 'user'], function () {
        Route::get('', 'backend\UserController@getUser');
        Route::get('add','backend\UserController@getAddUser' );
        Route::post('add','backend\UserController@postAddUser' );
        Route::get('edit/{idUser}', 'backend\UserController@getEditUser');
        Route::post('edit/{idUser}', 'backend\UserController@postEditUser');
        Route::get('del/{idUser}', 'backend\UserController@delUser');
    });
    //adminuser
    Route::group(['prefix' => 'adminuser'], function () {
        Route::get('', 'backend\AdminController@getAdminUser');
        Route::get('add','backend\AdminController@getAddAdminUser' );
        Route::post('add','backend\AdminController@postAddAdminUser' );
        Route::get('edit/{idAdminUser}', 'backend\AdminController@getEditAdminUser');
        Route::post('edit/{idAdminUser}', 'backend\AdminController@postEditAdminUser');
        Route::get('del/{idAdminUser}', 'backend\AdminController@DelAdminUser');
    });
  //coupon
  Route::group(['prefix' => 'coupon'], function () {
    Route::get('', 'backend\CouponController@getCoupon');
    Route::get('add','backend\CouponController@getAddCoupon' );
    Route::post('add','backend\CouponController@postAddCoupon' );
    Route::get('edit/{idCoupon}', 'backend\CouponController@getEditCoupon');
    Route::post('edit/{idCoupon}', 'backend\CouponController@postEditCoupon');
    Route::get('del/{idCoupon}', 'backend\CouponController@delCoupon');
});
    //subcribe
    Route::get('subcribe', 'backend\SubcribeController@getSubcribeAdmin')->name('getSubcribeAdmin');
    //email
    Route::post('send','backend\SubcribeController@sendMail')->name('sendNotiMail');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
